<?php
return [
    'adminEmail' => 'info@tukuvi.net',
    'supportEmail' => 'info@tukuvi.net',
    'senderEmail' => 'info@tukuvi.net',
    'senderName' => 'ESRF',
    'user.passwordResetTokenExpire' => 3600,
    'user.passwordMinLength' => 8,

    'yvideoAdminmail' => 'info@tukuvi.net',
    'urlServer' => 'http://35.83.179.128/yesrf',
];

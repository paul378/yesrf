<?php
namespace common\models;

use app\models\Application;
use app\models\ApplicationReceiver;
use app\models\AuthAssignment;
use app\models\Notification;
use app\models\NotificationTarget;
use Yii;
use yii\base\Model;
use yii\db\Expression;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use yii\web\NotFoundHttpException;

use yii\helpers\ArrayHelper;


/**
 * Login form
 */
class Esrf extends Model
{

    public static function datedifferent( $datetime )
    {

        $datetime1 = new \DateTime( $datetime );
        $datetime2 = new \DateTIme('Now');

        $different = $datetime1->diff($datetime2);

        return [
            'close' => ((isset( $different->invert ) && ( $different->invert > 0 ))? 200 : 100 ),
            'days'  => ((isset( $different->days ))? $different->days : 0 ),
        ];

    }

    public static function applicationStatusStatistic()
    {
        try {

            /*
             *  HERE we check if the method was used by API
             *  and is TRUE, then API will pass user referral id
             *  else Use referral id from session of user.
             * */

            return (object) (new \yii\db\Query())

                ->select([

                    "SUM(IF(application._status = 10, 1, 0)) AS saved",
                    "SUM(IF(application._status = 20, 1, 0)) AS cancelled",
                    "SUM(IF(application._status = 30, 1, 0)) AS submitted",
                    "SUM(IF(application._status = 40, 1, 0)) AS approved",
                    "SUM(IF(application._status = 50, 1, 0)) AS rejected"

                ])

                ->from('application')

                ->where([
                    'application._deleted' => 20,
                    'user_id' => ((isset( Yii::$app->user->identity->id ))? Yii::$app->user->identity->id : null ),
                ])

                ->one();

        } catch (\Exception $e) {

            return null ;

        }

    }

    public static function userSearch($input)
    {
        try {

            /*
             *  HERE we check if the method was used by API
             *  and is TRUE, then API will pass user referral id
             *  else Use referral id from session of user.
             * */

            return (object) (new \yii\db\Query())

                ->select([

                    "`user`.id",
                    "CONCAT_WS(' ', first_name, last_name, '<', email, '>', mobile) as full_data"

                ])

                ->from('user')

                ->where([
                    '_deleted' => 20,
                    '_status'  => 10,
                ])

                ->andWhere("CONCAT_WS(' ', first_name, last_name, '<', email, '>', mobile) LIKE '%$input%'")

                ->limit(4)

                ->all();

        } catch (\yii\db\Exception $e) {

            return null ;

        }

    }

    public static function applicationAdminStatistic()
    {
        try {
            
            /*
             *  HERE we check if the method was used by API
             *  and is TRUE, then API will pass user referral id
             *  else Use referral id from session of user.
             * */
            
            return (object) (new \yii\db\Query())
            
            ->select([

                "SUM(IF(application._status = 10, 1, 0)) AS saved", 
                "SUM(IF(application._status = 20, 1, 0)) AS cancelled", 
                "SUM(IF(application._status = 30, 1, 0)) AS submitted", 
                "SUM(IF(application._status = 40, 1, 0)) AS approved", 
                "SUM(IF(application._status = 50, 1, 0)) AS rejected"
            
            ])
            
            ->from('application')
            
            ->where('application._deleted = 20')
            
            ->one();
            
        } catch (\Exception $e) {
            
            return null ;
            
        }
        
    }

    public static function applicantCounter()
    {
        try {
            
            /*
             *  HERE we count all user who have applied for any call
             * */
            
            return (object) (new \yii\db\Query())
            
            ->select([

                "COUNT(DISTINCT application.user_id) AS userCounter"
            
            ])
            
            ->from('application')
            
            ->where('application._deleted = 20 AND application._status IN (30, 40, 50)')
            
            ->one();

        } catch (\Exception $e) {
            
            return null ;
            
        }
        
    }

    public static function applicantCounterByGender()
    {
        try {
            
            /*
             *  HERE we count all user who have applied for any call
             * */

            return (object) (new \yii\db\Query())

            ->select([

                "COUNT(DISTINCT application.user_id) AS userCounter",
                "user.sex"
            
            ])

            ->from('application')
            ->join('INNER JOIN', 'user', 'application.user_id = user.id')

            ->where([
                'application._deleted' => 20,
                'application._status'  => [ 30,40,50 ]
            ])

            ->groupBy([
                '`user`.sex'
            ])

            ->all();

        } catch (\Exception $e) {
            
            return null ;
            
        }
        
    }

    public static function applicantCounterByRegion()
    {
        try {
            
            /*
             *  HERE we count all user who have applied for any call
             * */

            return (object) (new \yii\db\Query())

            ->select([

                "COUNT(DISTINCT application.user_id) AS userCounter",
                "user.region_id",
                "region._name",
            
            ])

            ->from('application')
            ->join('INNER JOIN', 'user', 'application.user_id = user.id')
            ->join('INNER JOIN', 'region', 'user.region_id = region.id')

            ->where([
                'application._deleted' => 20,
                'application._status'  => [ 30,40,50 ]
            ])

            ->groupBy([
                "user.region_id",
                "region._name",
            ])

            ->all();

        } catch (\Exception $e) {
            
            return null ;
            
        }
        
    }

    public static function applicationCounter()
    {
        try {
            
            /*
             *  HERE we count all user who have applied for any call
             * */
            
            return (object) (new \yii\db\Query())
            
            ->select([

                "COUNT(application.id) AS idcounter"
            
            ])
            
            ->from('application')
            
            ->where('application._deleted = 20 AND application._status IN (30, 40, 50)')
            
            ->one();

        } catch (\Exception $e) {
            
            return null ;
            
        }
        
    }

    public static function callCounter()
    {
        try {

            /*
             *  HERE we count all user who have applied for any call
             * */

            return (object) (new \yii\db\Query())

                ->select([

                    "COUNT(proposal_call.id) AS idcounter"

                ])

                ->from('proposal_call')

                ->where('proposal_call._deleted = 20 AND proposal_call._status IN (30, 40, 50)')

                ->one();

        } catch (\Exception $e) {

            return null ;

        }

    }

    public static function changeApplicationStatus($id, $status)
    {
        try {

            /*
             *  HERE we count all user who have applied for any call
             * */

            $apply = Application::findOne([
                'id' => $id,
            ]);

            //var_dump($apply);
            //die();

            if (isset( $apply->id ))
            {

                $apply->_status = $status;

                if ($apply->validate())
                {
                    if ($apply->save())
                    {
                        return true;
                    }
                }

            }

            return false ;

        } catch (\yii\db\Exception $e) {

            return false ;

        }

    }

    public static function createPrivateNotification($user_id, $message, $sender = 'ESRF Team')
    {
        try {

            /*
             *  HERE we count all user who have applied for any call
             * */

            $notice = new Notification();
            $notice->load([
                'Notification' => [
                    'for_public' => 20,
                    'message' => $message,
                    'sender' => $sender,
                ]
            ]);

            if ($notice->validate()) {
                if ($notice->save()) {

                    /*
                     *  HERE we count all user who have applied for any call
                     * */

                    $noticetarget = new NotificationTarget();
                    $noticetarget->load([
                        'NotificationTarget' => [
                            'notification_id' => $notice->id,
                            'target_id' => $user_id,
                        ]
                    ]);

                    if ($noticetarget->validate()) {
                        if ($noticetarget->save()) {

                        }}

                }}

            return $notice ;

        } catch (\yii\db\Exception $e) {

            return $e ;

        }

    }

    public static function PrivateNotification($users, $message, $sender = 'ESRF Team')
    {
        try {

            /*
             *  HERE we count all user who have applied for any call
             * */

            $notice = new Notification();
            $notice->load([
                'Notification' => [
                    'for_public' => 20,
                    'message' => $message,
                    'sender' => $sender,
                ]
            ]);

            if ($notice->validate()) {
                if ($notice->save()) {

                    /*
                     *  HERE we count all user who have applied for any call
                     * */

                    $target = array();

                    if (is_array( $users ))
                    {

                        foreach ( $users as $user )
                        {
                            $target[] = [
                                $notice->id, $user['id']
                            ];
                        }

                    }

                    \Yii::$app->db->createCommand()
                    ->batchInsert(
                        'notification_target', [ 'notification_id', 'target_id' ], $target
                    )->execute();

                }}

            return $notice ;

        } catch (\yii\db\Exception $e) {

            return $e ;

        }

    }

    public static function applicationReceiverEmail()
    {
        return User::find()->select([ 'user.id', 'first_name', 'email', 'mobile' ])->innerJoin('application_receiver', 'application_receiver.receiver_id = user.id')->asArray()->all();
    }

    public static function userReceiverEmail()
    {
        return User::find()->select([ 'user.id', 'first_name', 'email', 'mobile' ])->innerJoin('auth_assignment', 'auth_assignment.user_id = user.id')->where([ 'auth_assignment.item_name' => 'User', 'user.user_activated' => 100, 'user._deleted' => 20, 'user._status' => 10 ])->asArray()->all();
    }

    public static function userReceiverEmailWithName()
    {
        return User::find()->select([ 'user.id', 'first_name', 'email', 'mobile' ])->innerJoin('auth_assignment', 'auth_assignment.user_id = user.id')->where([ 'auth_assignment.item_name' => 'User', 'user.user_activated' => 100, 'user._deleted' => 20, 'user._status' => 10 ])->asArray()->all();
    }

    public static function invitedReceiverEmail($id)
    {
        return User::find()->select([ 'user.id', 'first_name', 'email', 'mobile' ])->innerJoin('auth_assignment', 'auth_assignment.user_id = user.id')->innerJoin('proposal_calls_invitation', 'proposal_calls_invitation.user_id = user.id')->where([ 'auth_assignment.item_name' => 'User', 'user.user_activated' => 100, 'user._deleted' => 20, 'user._status' => 10, 'proposal_calls_invitation.proposal_calls_id' => $id, ])->asArray()->all();
    }

    public static function lastYearApplicationReceived()
    {
        try {
            
            /*
             *  HERE we count all user who have applied for any call
             * */

            return (object) (new \yii\db\Query())

            ->select([

                "EXTRACT( YEAR FROM application.created_at ) AS _year", 
                "EXTRACT( MONTH FROM application.created_at ) AS _month", 
                "DATE_FORMAT(application.created_at, \"%M\") AS _mwezi",
                "COUNT(application.id) AS applicationCounter",
            
            ])

            ->from('application')

            //  ->where("EXTRACT( YEAR FROM application.created_at ) = (EXTRACT( YEAR FROM CURRENT_DATE() ) - 1)")
            ->where("EXTRACT( YEAR FROM application.created_at ) = 2021")
            ->andWhere([
                'application._deleted' => 20,
                'application._status'  => [ 30,40,50 ],
            ])

            ->groupBy([
                "_year", "_month", "_mwezi",
            ])

            ->orderBy("_month")

            ->all();

        } catch (\Exception $e) {
            
            return null ;
            
        }
        
    }

    public static function adminStatusStatistic()
    {
        try {

            /*
             *  HERE we count all user who have applied for any call
             * */

            $applicant = self::applicantCounter();
            $application = self::applicationCounter();
            $calls = self::callCounter();
            $applicationstatistic = self::applicationAdminStatistic();

            $applicantCounterByGender = self::applicantCounterByGender();
            $applicantCounterByRegion = self::applicantCounterByRegion();

            $lastYearApplicationReceived = self::lastYearApplicationReceived();

            $pergender = [];
            foreach ( $applicantCounterByGender as $gender ) 
            {

                if ( $gender['sex'] == 'F' )
                {
                    $pergender['female'] = $gender;
                }

                if ( $gender['sex'] == 'M' )
                {
                    $pergender['male'] = $gender;
                }

            }

            $ucount = [];
            $umonth = [];

            foreach ( $lastYearApplicationReceived as $mc ) {

                if ( isset( $mc['_month'] ) && (( $mc['_month'] > 0 ) && ( $mc['_month'] < 13 )) ) {
                    $ucount[] = $mc['applicationcounter'];
                    $umonth[] = $mc['_mwezi'];
                }

            }

            return [
                'applicant' => $applicant,
                'application' => $application,
                'calls' => $calls,
                'applicationstatistic' => $applicationstatistic,
                'total_application' => ((isset( $applicationstatistic->approved, $applicationstatistic->submitted, $applicationstatistic->rejected ))? ( ((float) $applicationstatistic->approved) + ((float) $applicationstatistic->submitted) + ((float) $applicationstatistic->rejected) ) : 0 ),
                'applicantCounterByGender' => $applicantCounterByGender,
                'pergender' => $pergender,
                'applicantCounterByRegion' => $applicantCounterByRegion,
                'lastYearApplicationReceived' => $lastYearApplicationReceived,
                'lastyrapp' => [
                    'month' => $umonth,
                    'total' => $ucount,
                ],
            ];

        } catch (\Exception $e) {
            
            return null ;
            
        }

    }

    public static function updateRole($model)
    {

        if (isset( $model->id, $model->role ))
        {

            $roles = AuthAssignment::findOne([
                'item_name' => $model->role,
                'user_id'   => $model->id,
            ]);

            if (!isset( $roles->user_id ))
            {

                //    $users = new AuthAssignment();
                //    $users->load([
                //        'AuthAssignment' => [
                //            'item_name' => $model->role,
                //            'user_id'   => $model->id,
                //        ]
                //    ]);

                $users = AuthAssignment::findOne([
                    'user_id'   => $model->id,
                ]);

                if (isset( $users->user_id )) {

                    $users->item_name = $model->role;

                    if ($users->validate()) {
                        $users->save();
                    }
                }


            }

        }

    }

    public static function createNotification($model)
    {
    }

}

<?php
namespace common\models;

use Yii;
use yii\base\Model;
use yii\db\Expression;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;



/**
 * Login form
 */
class Common extends Model
{

    public static function replace($text, $rule = "removespaces", $str = "")
    {

        /*
            bellow is list of rule's string
        */
        $rules = [
            "purenumerous" => "/\D/",
            "alphanumeric" => "/[^a-zA-Z0-9\s]/",
            "space_comars" => "/[\s,]+/",
            "removespaces" => "/\s+/",
            "comar"        => "/[\,]+/",
        ];

        /*
            remove whitespace in the rule's string
        */
        $rule = preg_replace($rules['removespaces'], "", $rule);

        /*
            use the rule's string to split the text
        */
        $strg = preg_replace($rules[ $rule ], $str, $text);

        return $strg;

    }
    
    
    public static function YiiiEmail( $views, $model )
    {
        
        try {
            
            if ( Yii::$app->mailer->compose()
                ->setFrom( Yii::$app->params['yvideoAdminmail'] )->setTo($model['receiver'])->setSubject($model['subject'])
                ->setHtmlBody( Yii::$app->controller->renderPartial( $views, [
                    'model' => $model,
                ]) )->send())
            {
                
                return TRUE;
                
            }
            
        } catch (\Exception $e) {
            
            return FALSE;
            
        }
        
    }
    
    
    public static function iEmail( $model )
    {
        
        try {

            if ( Yii::$app->mailer->compose()
                ->setFrom( Yii::$app->params['yvideoAdminmail'] )->setTo($model['receiver'])->setSubject($model['subject'])
                ->setHtmlBody( Yii::$app->controller->renderPartial( $model['viewtemplate'], [
                    'model' => $model,
                ]) )->send())
            {
                
                return TRUE;
                
            }
            
        } catch (\Exception $e) {
            
            return FALSE;
            
        }
        
    }

    public static function TheEmail($views, $model)
    {


        $mail = new PHPMailer(true);

        try {

            //  Server settings
            //  $mail->SMTPDebug = SMTP::DEBUG_SERVER;
            $mail->SMTPDebug = false;
            //  $mail->Timeout = 20;
            $mail->ContentType = 'text/html; charset=utf-8';
            $mail->isSMTP();
            $mail->Host       = 'smtp.gmail.com';
            $mail->SMTPAuth   = true;
            $mail->Username   = 'tangayetu@esrftz.org';
            $mail->Password   = 'Kajiba@Africa2015';
            $mail->SMTPSecure = 'tls';
            $mail->Port       = 587;
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            // Sender
            $mail->setFrom('tangayetu@esrftz.org', 'ESRF Platform');

            // Reply To
            $mail->addReplyTo('tangayetu@esrftz.org', 'ESRF Platform');

            if (isset( $model['receiver'] ))
            {

                if (is_array( $model['receiver'] ))
                {

                    foreach ( $model['receiver'] as $item )
                    {
                        $mail->addAddress($item['email'], $item['first_name']);
                    }

                } else {

                    // Recipients
                    $mail->addAddress($model['receiver']);

                }

            }

            // Content
            $mail->isHTML(true);

            // Set Subject
            $mail->Subject = $model['subject'];

            // Generate Body
            $mail->AltBody = 'Hello you are getting this email because you have subscribe to mail list as applicant';
            $mail->Body = Yii::$app->controller->renderPartial(
                            $views,
                            [
                                'model' => $model,
                            ]);

            if ($mail->send())
            {
                return true;
            }

            return false;

        } catch (Exception $e) {

            return false;
        }


    }


    public static function generateString( $option = [] )
    {
        // code...
        $str = self::replace( Yii::$app->security->generateRandomString( (isset($option['len']))? $option['len'] : 15 ), "alphanumeric" );

        switch ( (isset($option['case']))? $option['case'] : null ) {
            case 'ucase':
                return strtoupper($str);
                break;
            case 'lcase':
                return strtolower($str);
                break;
        }

        return $str;
    }

    
    public static function generateNumber( $option = [] )
    {

        $chars = array_merge( range(0,9) );
        shuffle($chars );

        $codes = implode( array_slice( $chars, 0, ((isset($option['len']))? $option['len'] : 15 ) ) );

        return $codes ;

    }


    public static function generate_hash_string($password)
    {

        try {
            return Yii::$app->getSecurity()->generatePasswordHash($password);
        } catch (\Exception $e) {
            return null;
        }

    }


    public static function verify_hash_string($real_password, $hash_password)
    {

        try {

            if (Yii::$app->getSecurity()->validatePassword($real_password, $hash_password)) {
                return true;
            } else {
                return false;
            }

        } catch (\Exception $e) {
            return false;
        }

    }


    public static function timeAgo( $time_ago )
    {

        date_default_timezone_set("Africa/Nairobi");
        $time_ago        = $time_ago;
        $current_time    = time();
        $time_difference = $current_time - $time_ago;
        $seconds         = $time_difference;

        $minutes = round($seconds / 60); // value 60 is seconds
        $hours   = round($seconds / 3600); //value 3600 is 60 minutes * 60 sec
        $days    = round($seconds / 86400); //86400 = 24 * 60 * 60;
        $weeks   = round($seconds / 604800); // 7*24*60*60;
        $months  = round($seconds / 2629440); //((365+365+365+365+366)/5/12)*24*60*60
        $years   = round($seconds / 31553280); //(365+365+365+365+366)/5 * 24 * 60 * 60

        if ($seconds <= 60)
        {

            return "Just Now";

        } else if ($minutes <= 60) {

            if ($minutes == 1){

                return "one minute ago";

            } else {

                return "$minutes minutes ago";

            }

        } else if ($hours <= 24) {

            if ($hours == 1){

                return "an hour ago";

            } else {

                return "$hours hrs ago";

            }

        } else if ($days <= 7){

            if ($days == 1){

                return "yesterday";

            } else {

                return "$days days ago";

            }

        } else if ($weeks <= 4.3){

            if ($weeks == 1){

                return "a week ago";

            } else {

                return "$weeks weeks ago";

            }

        } else if ($months <= 12){

            if ($months == 1){

                return "a month ago";

            } else {

                return "$months months ago";

            }

        } else {

            if ($years == 1){

                return "one year ago";

            } else {

                return "$years years ago";

            }
        }
    }


    public static function number_format_short( $n, $precision = 1 )
    {
        if ($n < 900) {
            // 0 - 900
            $n_format = number_format($n, $precision);
            $suffix = '';
        } else if ($n < 900000) {
            // 0.9k-850k
            $n_format = number_format($n / 1000, $precision);
            $suffix = 'K';
        } else if ($n < 900000000) {
            // 0.9m-850m
            $n_format = number_format($n / 1000000, $precision);
            $suffix = 'M';
        } else if ($n < 900000000000) {
            // 0.9b-850b
            $n_format = number_format($n / 1000000000, $precision);
            $suffix = 'B';
        } else {
            // 0.9t+
            $n_format = number_format($n / 1000000000000, $precision);
            $suffix = 'T';
        }

        // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
        // Intentionally does not affect partials, eg "1.50" -> "1.50"
        if ( $precision > 0 ) {
            $dotzero = '.' . str_repeat( '0', $precision );
            $n_format = str_replace( $dotzero, '', $n_format );
        }

        return $n_format . $suffix;
    }


    public static function fileSize( $n, $precision = 1 ) {
        if ($n < 900) {
            // 0 - 900
            $n_format = number_format($n, $precision);
            $suffix = '';
        } else if ($n < 900000) {
            // 0.9k-850k
            $n_format = number_format($n / 1000, $precision);
            $suffix = 'K';
        } else if ($n < 900000000) {
            // 0.9m-850m
            $n_format = number_format($n / 1000000, $precision);
            $suffix = 'M';
        } else if ($n < 900000000000) {
            // 0.9b-850b
            $n_format = number_format($n / 1000000000, $precision);
            $suffix = 'B';
        } else {
            // 0.9t+
            $n_format = number_format($n / 1000000000000, $precision);
            $suffix = 'T';
        }

        // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
        // Intentionally does not affect partials, eg "1.50" -> "1.50"
        if ( $precision > 0 ) {
            $dotzero = '.' . str_repeat( '0', $precision );
            $n_format = str_replace( $dotzero, '', $n_format );
        }

        return $n_format . $suffix;
    }
    
    
    public static function getCountryByIp()
    {
        
        try {
            
            /*Get user ip address*/
            $ip_address = $_SERVER['REMOTE_ADDR'];
            
            /*Get user ip address details with geoplugin.net*/
            $geopluginURL='http://www.geoplugin.net/php.gp?ip='.$ip_address;
            
            $addrDetailsArr = unserialize(file_get_contents($geopluginURL));
            
            /*Get City name by return array*/
            $city = $addrDetailsArr['geoplugin_city'];
            
            /*Get Country name by return array*/
            $country = $addrDetailsArr['geoplugin_countryName'];
            
            if(!$city){
                $city = 'Unknown' ;
            }
            
            if(!$country){
                $country = 'Unknown';
            }
            
            return [
                'country' => $country,
                'city'    => $city, 
            ];
            
        } catch (\Exception $e) {
            return [];
        }
        
    }
    
    
    public static function getJustCountryByIp( $ip_address = null, $returnOption = 'geoplugin_countryName' )
    {
        
        try {

            /* Get user ip address */
            $ip_address = ((isset( $ip_address ))? $ip_address : $_SERVER['REMOTE_ADDR'] );
            
            /* Get user ip address details with geoplugin.net */
            $geopluginURL = 'http://www.geoplugin.net/php.gp?ip='. $ip_address ;
            
            $addrDetailsArr = unserialize(file_get_contents($geopluginURL));
            
            /*Get Country name by return array*/
            $country = $addrDetailsArr[$returnOption];

            return $country ;
            
        } catch (\Exception $e) {

            return null ;

        }
        
    }
    
    public static function url_exists($url)
    {
        
        try {

            // Open file
            $handle = @fopen($url, 'r');
            
            // Check if file exists
            if(!$handle){
                return FALSE;
            }
            
            return TRUE;

        } catch (\Exception $e) {

            return FALSE;

        }
        
    }
    
    public static function GetAge($date)
    {
        
        $from = new \DateTime( (string) $date );
        $to   = new \DateTime('today');

        return $from->diff($to)->y;

    }    
    
    
}

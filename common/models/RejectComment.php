<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class RejectComment extends Model
{
    public $proposal_id;
    public $comment;
    public $title;

    private $_user;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['proposal_id', 'comment'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'comment' => 'Comment',
            'proposal_id' => 'Proposal',
            'title' => 'Title',
        ];
    }

}

<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class SignUp extends Model
{
    public $first_name;
    public $last_name;
    public $email;
    public $mobile;
    public $sex;
    public $occupation;
    public $regions_id;
    public $district;
    public $password;
    public $repassword;

    public $rememberMe = true;

    private $_user;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // username and password are both required
            [[ 'first_name', 'last_name', 'sex', 'occupation', 'email', 'mobile', 'regions_id', 'district', 'password', 'repassword' ], 'required'],
            ['password', 'string', 'min' => 6],
            ['repassword', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
            // rememberMe must be a boolean value

        ];
    }

}

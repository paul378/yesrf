<?php
namespace common\models;

use app\models\AuthAssignment;
use app\models\ProposalCall;
use app\models\ProposalCallsInvitation;
use Yii;
use yii\web\NotFoundHttpException;
use yii\base\Model;

use app\models\User;


/**
 * Login form
 */
class InvitedUser extends Model
{
    public $proposal_calls_id;
    public $email;
    public $user_id;
    public $sends;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['proposal_calls_id', 'email'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'proposal_calls_id' => 'Proposal Calls ID',
            'user_id' => 'User ID',
            'email' => 'Email',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getApplicant($email)
    {

        return User::findOne([
            'email' => $email,
            '_status' => 10
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function inviteApplicant()
    {

        #   here is the container that hold all invitation action
        #   response
        $output = array();

        #   separate all email and keep them in a single array
        #   for the further process
        $useremail = explode(",", $this->email );

        if (is_array( $useremail ) && (count( $useremail ) > 0))
        {

            #   loop through all email to check if users with email provided are exist
            #   if yes register them into invitation book
            foreach ($useremail as $email)
            {

                $user = User::findOne([
                    'email' => $email, '_deleted' => 20, '_status' => 10
                ]);

                #   --------------------------------------------------------------
                #   --------------------------------------------------------------
                #   IF invited user doesn't exist create invitation and send email
                if (!isset( $user->id ))
                {
                    $user = $this->IUser($email);
                }
                #   --------------------------------------------------------------
                #   --------------------------------------------------------------

                if (isset( $user->id ))
                {

                    #   register user into
                    #   invitation book
                    $invitation = new ProposalCallsInvitation();
                    $invitation->load([
                        'ProposalCallsInvitation' => [
                            'proposal_calls_id' => (int) $this->proposal_calls_id,
                            'user_id' => $user->id,
                        ]
                    ]);

                    $userinvited = ProposalCallsInvitation::findOne([
                        'proposal_calls_id' => $invitation->proposal_calls_id,
                        'user_id'  => $invitation->user_id,
                        '_deleted' => 20,
                        '_status'  => 10,
                    ]);


                    if (!isset( $userinvited->user_id ))
                    {

                        if ($invitation->validate()) {

                            try {

                                $saving = $invitation->save();

                                if ($saving)
                                {
                                    $output[] = [
                                        'error' => null,
                                        'user'  => $email
                                    ];
                                } else {
                                    $output[] = [
                                        'error' => 100,
                                        'user'  => $email
                                    ];
                                }

                            } catch (\yii\db\Exception $e) {

                                $output[] = [
                                    'error' => 101,
                                    'user'  => $email
                                ];

                            }

                        } else {
                            $output[] = [
                                'error' => 200,
                                'user'  => $email
                            ];
                        }

                    } else {
                        $output[] = [
                            'error' => null,
                            'user'  => $email
                        ];
                    }

                } else {
                    $output[] = [
                        'error' => 300,
                        'user'  => $email
                    ];
                }

            }
        }

        return $output;

    }


    /**
     * {@inheritdoc}
     */
    public function IUser($email)
    {

        $model = new User();

        //  $model->scenario = 'invitation';

        if ($model->load([

            'User' => [
                'first_name' => 'Invited',
                'last_name' => 'User',
                'password' => 'none',
                'email' => $email,
                'user_activated' => 100,
                '_deleted' => 20,
                '_status'    => 10,
            ]
        ]))
        {

            if ($model->validate()) {

                try {

                    if ($model->save()) {

                        if (isset( $model->id, $model->email ))
                        {

                            $authass = new AuthAssignment();
                            $authass->load([
                                'AuthAssignment' => [
                                    'item_name' => 'User',
                                    'user_id'   => $model->id,
                                ]
                            ]);

                            if ($authass->validate()) {
                                $authass->save();
                            }

                            $proposal = ProposalCall::findOne([
                                'id' => $this->proposal_calls_id
                            ]);

                            //  SEND email
                            $this->sends = Common::TheEmail(
                                '//application/email/initial-sign-up',
                                [
                                    'receiver' => $model->email,
                                    'subject'  => 'Re: welcome to ESRF platform',
                                    'model'    => $proposal,
                                    'links'    => Yii::$app->urlManager->createAbsoluteUrl([ '/invited-user/create-account', 'id' => $model->id ]),
                                ]
                            );

                        }

                    }

                } catch (yii\db\Exception $e) {

                    $model = User::findOne([
                        'email' => $model->email
                    ]);

                }

            } else {

                $model = User::findOne([
                    'email' => $model->email
                ]);

            }

        }

        return $model;

    }

}

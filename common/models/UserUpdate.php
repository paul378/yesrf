<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class UserUpdate extends Model
{
    public $new_password;
    public $old_password;

    private $_user;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['old_password', 'new_password'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'old_password' => 'Old Password',
            'new_password' => 'New Password',
        ];
    }

}

/*
    HERE we capture the main server
*/
const url_servers = `${window.location.protocol}//${window.location.host.split(":")[0]}/yesrf`;


/*
 HERE we are going upload report
 */
window.addEventListener("load", function ()
{

    let anydoc = PaulUploadButton({
        pickfiles: 'pickfiles-any',
        container: 'container-any',
        url: '$endPointUrl',
        chunk_size: '2mb',
        filters: {
            max_file_size : '2mb',
            mime_types: [
                {title : "Document files", extensions : "pdf,doc,docx,xls,xlsx"},
            ]
        },
        filelist: 'filelist-any',
        progress_counter: 'logs_counter-any',
        multipart_params: {
            "ProposalCallsReport[proposal_calls_id]": `${url_servers}/uploader/call-report`,
            "ProposalCallsReport[category]": "CALL",
        },
        callback: function toa_matokeo(output) {

            fetch(`${url_servers}/uploader/call-report-update`, {
                method: 'post',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    filename: document.getElementById('file-name-input').value,
                    id: output.info.proposal.id.toString()
                })
            })
                .then(res => res.json())
                .then(res => {

                    // here we can refrash the page after update
                    // the document information
                    window.location.reload();

                });

        },
    });

}, false);

var url = window.location.protocol + "//" + location.host.split(":")[0] +"/yesrf";
console.log(url);

var base_url = window.location.origin;
console.log(base_url);

var host = window.location.host;
console.log(host);

var pathArray = window.location.pathname.split( '/' );
console.log(pathArray);


/*
	HERE we upload the profile avatar
*/
function PaulUpload(safar)
{

	/*
	
		let safar = {
			pickfiles: 'pickfiles',
			container: 'container',
			url: '/uploader/user-avatar-upload',
			chunk_size: '2mb',
			filters: {
				max_file_size : '2mb',
				mime_types: [
					{title : "Video files", extensions : "jpg,jpeg,gif,png"},
				]
			},
			filelist: 'filelist',
			progress_counter: 'pickavatar_counter',
		}

	 */

    let path = "/plupload/js/`";
    let uploader = new plupload.Uploader({

        runtimes: 'html5,flash,silverlight,html4',
        flash_swf_url: path + 'Moxie.swf',
        silverlight_xap_url: path + '/Moxie.xap',

        browse_button: safar.pickfiles,
        container: document.getElementById(safar.container),
        url: safar.url,
        chunk_size: safar.chunk_size,
        max_retries: 2,

        filters : safar.filters,
        multipart_params : safar.multipart_params,

        init: {

            PostInit: function() {
                document.getElementById(safar.filelist).innerHTML = '';
            },

            BeforeUpload: function(up, file) {
                up.settings.multipart_params = {}
            },

            FilesAdded: function(up, files) {

                up.refresh();

				// // if you want to show all list of file uploaded
                // plupload.each(files, function(file)
                // {
				// 	document.getElementById(safar.filelist).innerHTML += `<div id="${file.id}">${file.name} (${plupload.formatSize(file.size)}) <b></b></div>`;
                // });

				// if you want to show only the last file uploaded
				if (files.length > 0) {
					document.getElementById(safar.filelist).innerHTML = `<div id="${ files[ files.length - 1 ].id }">${ files[ files.length - 1 ].name } (${ plupload.formatSize( files[ files.length - 1 ].size ) }) <b></b></div>`;
				}

                uploader.start();
            },

            UploadProgress: function(up, file) {
                document.getElementById(safar.progress_counter).innerHTML = `<span> - ${file.percent} % </span>`;
				console.log(`${file.percent} %`);
            },

            FileUploaded: function(up, file, info) {

		        try {
		
	                const output = JSON.parse(info.response);
					const jscall = safar.callback ;

	                console.log(output);

		            if (typeof jscall === 'function') 
		            {
		            	jscall(output);
		            }
		
		        } catch(err) {
		        	console.log(err);
		        } finally {
		        	console.log("File uploaded");
		        }

            },

            Error: function(up, err) {
                console.log(`Error # ${ err.code } : ${ err.message }`);
            }
        }

    });

    uploader.init();

}


/*
	HERE we upload
*/


/*
	HERE we upload the profile avatar
*/
function PaulUploadButton(safar)
{

	/*
	
		let safar = {
			pickfiles: 'pickfiles',
			container: 'container',
			url: '/uploader/user-avatar-upload',
			chunk_size: '2mb',
			filters: {
				max_file_size : '2mb',
				mime_types: [
					{title : "Video files", extensions : "jpg,jpeg,gif,png"},
				]
			},
			filelist: 'filelist',
			progress_counter: 'pickavatar_counter',
            multipart_params: {
                "age": 26
            }
		}

	 */

    let path = "/plupload/js/`";
    let uploader = new plupload.Uploader({

        runtimes: 'html5,flash,silverlight,html4',
        flash_swf_url: path + 'Moxie.swf',
        silverlight_xap_url: path + '/Moxie.xap',

        browse_button: safar.pickfiles,
        container: document.getElementById(safar.container),
        url: safar.url,
        chunk_size: safar.chunk_size,
        max_retries: 2,

        filters : safar.filters,
        multipart_params : safar.multipart_params,

        init: {

            PostInit: function() {
                document.getElementById(safar.filelist).innerHTML = '';

                document.getElementById('any-btn-upload').addEventListener('click', function() {

                    uploader.start();
                    return false;

                }, false);

            },

            BeforeUpload: function(up, file) {
                up.settings.multipart_params = {}
            },

            FilesAdded: function(up, files) {

                up.refresh();

				// if you want to show only the last file uploaded
				if (files.length > 0) {
					document.getElementById(safar.filelist).innerHTML = `<div id="${ files[ files.length - 1 ].id }">${ files[ files.length - 1 ].name } (${ plupload.formatSize( files[ files.length - 1 ].size ) }) <b></b></div>`;
				}

            },

            UploadProgress: function(up, file) {
                document.getElementById(safar.progress_counter).innerHTML = `<span> - ${file.percent} % </span>`;
				console.log(`${file.percent} %`);
            },

            FileUploaded: function(up, file, info) {

		        try {
		
	                const output = JSON.parse(info.response);
					const jscall = safar.callback ;

	                console.log(output);

		            if (typeof jscall === 'function') 
		            {
		            	jscall(output);
		            }
		
		        } catch(err) {
		        	console.log(err);
		        } finally {
		        	console.log("File uploaded");
		        }

            },

            Error: function(up, err) {
                console.log(`Error # ${ err.code } : ${ err.message }`);
            }
        }

    });

    uploader.init();

}


/*
	HERE we upload
*/



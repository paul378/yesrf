<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'esrf-web-portal',
    'name'=>'esrf',

    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',

    'modules' => [

        'api' => [
            'class' => 'app\modules\api\v1',
        ],

        'audit' => [
            'class' => 'bedezign\yii2\audit\Audit',
            // the layout that should be applied for views within this module
            'layout' => 'main',
            // Name of the component to use for database access
            'db' => 'db', 
            // List of actions to track. '*' is allowed as the last character to use as wildcard
            'trackActions' => ['*'], 
            // Actions to ignore. '*' is allowed as the last character to use as wildcard (eg 'debug/*')
            // 'ignoreActions' => ['audit/*', 'debug/*'],
            'ignoreActions' => ['*'],
            // Maximum age (in days) of the audit entries before they are truncated
            'maxAge' => 'debug',
            // IP address or list of IP addresses with access to the viewer, null for everyone (if the IP matches)
            'accessIps' => ['127.0.0.1', '192.168.*'], 
            // Role or list of roles with access to the viewer, null for everyone (if the user matches)
            'accessRoles' => ['admin'],
            // User ID or list of user IDs with access to the viewer, null for everyone (if the role matches)
            'accessUsers' => [1, 2],
            // Compress extra data generated or just keep in text? For people who don't like binary data in the DB
            'compressData' => true,
            // The callback to use to convert a user id into an identifier (username, email, ...). Can also be html.
            'userIdentifierCallback' => ['app\models\User', 'userIdentifierCallback'],
            // If the value is a simple string, it is the identifier of an internal to activate (with default settings)
            // If the entry is a '<key>' => '<string>|<array>' it is a new panel. It can optionally override a core panel or add a new one.
            'panels' => [
                'audit/request',
                'audit/error',
                'audit/trail',
                'app/views' => [
                    'class' => 'app\panels\ViewsPanel',
                    // ...
                ],
            ],
            'panelsMerge' => [
               // ... merge data (see below)
            ]
        ],

    ],

    'components' => [

        'view' => [
             'theme' => [
                 'pathMap' => [
                    // '@app/views' => '@vendor/hail812/yii2-adminlte3/src/views'
                    '@app/views' => '@app/themes/yii2-adminlte3/src/views'
                 ],
             ],
        ],

        'request' => [
            'csrfParam' => '_csrf-frontend',
            'class' => 'common\components\Request',
            'web' => '/frontend/web',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity-esrf-frontend', 
                'httpOnly' => true
            ],
            'authTimeout'     => 600,
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'class' => '\bedezign\yii2\audit\components\web\ErrorHandler',
            'errorAction' => 'site/error',
        ],

        'urlManager' => [

            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            //'suffix'=>'.html',
            'enablePrettyUrl' => true,
            'rules' => array(
                [
                    'class' => 'common\helpers\UrlRule'
                ],

                '<controller:\w+>/<id:\d+>'     => '<controller>/<action>',
                '<controller:\w+>/<id:\d+>/<action:\w+>'    => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ],
        'authManager'=>[
            'class'=>'yii\rbac\DbManager',
        ],
    ],
    'params' => $params,
];

<?php
namespace frontend\controllers;

use app\models\Company;
use app\models\CompanyCategory;
use Yii;
use yii\base\BaseObject;
use yii\base\InvalidArgumentException;
use yii\data\ArrayDataProvider;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use frontend\models\ResetPasswordForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

use Openpesa\SDK\Pesa;
use Openpesa\SDK\Tests\Fixture;

use common\models\LoginForm;
use common\models\SignUp;
use common\models\Esrf;
use common\models\UserUpdate;
use common\models\Common;


use app\models\ProposalCall;
use app\models\ProposalCallSearch;
use app\models\Application;
use app\models\ApplicationSearch;
use app\models\ApplicationDocument;
use app\models\Region;
use app\models\District;
use app\models\Category;
use app\models\User;

use app\models\AuthAssignment;
use app\models\AuthAssignmentSearch;

use app\models\Faq;
use app\models\FaqSearch;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actionNoIdentity()
    {

        return $this->renderPartial(
            '//error/access-permission',
            [
            ]
        );

    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        if (Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        }

        /*
            for admin and staff
        */
        if (Yii::$app->user->can('admin-dashboard-site')) {
            return $this->redirect([
                'site/admin-dashboard'
            ]);
        }

        /*
            for applicant
        */
        if (Yii::$app->user->can('applicant-dashboard-site')) {
            return $this->redirect([
                'site/applicant-dashboard'
            ]);
        }

        /*
            for anyone
        */
        return $this->render('index');

    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionAdminDashboard()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        }


//        return json_encode([
//            'data' => Esrf::adminStatusStatistic(),
//        ]);

        return $this->render(
            'admin-dashboard',
            [
                'esrfdata' => json_decode(json_encode( Esrf::adminStatusStatistic() )),
            ]
        );
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionApplicantDashboard()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        }

        $searchModelApp = new ApplicationSearch();
        $searchModelCal = new ProposalCallSearch();

        $dataProviderCal = $searchModelCal->search(Yii::$app->request->queryParams);
        $dataProviderCal->query
            ->andFilterWhere([
                'types_id' => 10,
                '_deleted' => 20,
                '_status'  => [30],
            ])
            ->limit(5);


        ///////////////////////////

        $sqlModel = new ProposalCallSearch();
        $sqlProvider = $sqlModel->search(Yii::$app->request->queryParams);

        $sqlProvider->query
            ->innerJoin('proposal_calls_invitation', 'proposal_calls_invitation.proposal_calls_id = proposal_call.id')
            ->where([
                'proposal_calls_invitation.user_id' => ((isset( Yii::$app->user->identity->id ))? Yii::$app->user->identity->id : null ),
                'proposal_calls_invitation._deleted' => 20,
                'proposal_call.types_id' => 20,
                'proposal_call._deleted' => 20,
                'proposal_call._status'  => [
                    30
                ],
            ])
            ->limit(5)
            ->all();

        //  get combine the two query
        $combine = array_merge($dataProviderCal->getModels(), $sqlProvider->getModels());

        //  use Data Provide to Restructure the Data
        $dataProviderCal = new ArrayDataProvider([
            'allModels' => $combine
        ]);

        ///////////////////////////


        $dataProviderApp = $searchModelApp->search(Yii::$app->request->queryParams);
        $dataProviderApp->query
            ->andFilterWhere([
                '_deleted' => 20,
                'user_id' => ((isset( Yii::$app->user->identity->id ))? Yii::$app->user->identity->id : null ),
            ])
            ->limit(10);

        return $this->render(
            'applicant-dashboard',
            [
                'applicationStatusStatistic' => Esrf::applicationStatusStatistic(),

                'dataProviderApp' => $dataProviderApp,
                'searchModelApp'  => $searchModelApp,

                'dataProviderCal' => $dataProviderCal,
                'searchModelCal'  => $searchModelCal,
            ]
        );
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionSignUp()
    {

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'main-register';

        return $this->render('signup', [
            'model' => null,
        ]);

    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionSignUpIndividual()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'main-register';

        $model = new SignUp();

        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate())
            {

                $user = new User();
                $user->load([
                    'User' => [
                        'first_name' => $model->first_name,
                        'last_name' => $model->last_name,
                        'sex' => $model->sex,
                        'email' => $model->email,
                        'mobile' => $model->mobile,
                        'region_id' => $model->regions_id,
                        'district_id' => $model->district,
                        'password' => Common::generate_hash_string( $model->password ),
                        '_status' => 10,
                    ]
                ]);

                if ($user->validate())
                {

                    if ($user->save())
                    {

                        if (isset( $user->email, $user->id )){

                            $imail = Common::TheEmail(
                                    '//site/email/register',
                                    [
                                        'account'  => $user,
                                        'receiver' => $user->email,
                                        'urlink'   => Yii::$app->urlManager->createAbsoluteUrl([ '/site/user-activate-account', 'id' => $user->id ]),
                                        'subject'  => 'ESRF Account Activation',
                                    ]
                                );

                            $authass = new AuthAssignment();
                            $authass->load([
                                'AuthAssignment' => [
                                    'item_name' => 'User',
                                    'user_id'   => $user->id,
                                ]
                            ]);

                            if ($authass->validate()) {
                                if ($authass->save()) {

                                    return $this->redirect([
                                        '/site/registered-notice', 'id' => $user->id, 'status' => 'success'
                                    ]);
                                }
                            }

                        }

                        return $this->redirect([
                            '/site/registered-notice', 'id' => $user->id, 'status' => 'failed'
                        ]);

                    }

                }

                return json_encode([
                    'model' => $model, 'user' => $user->password
                ]);

            }

        } else {

            $model->repassword = '';
            $model->password   = '';

        }

        return $this->render('register', [
            'model' => $model,
            'regions' => Region::find()->where([ 'status' => 10 ])->all(),
        ]);

    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionSignUpInstitution()
    {

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'main-register';

        $company = new Company();
        $model = new SignUp();

        if ($company->load(Yii::$app->request->post()) && $model->load(Yii::$app->request->post())) {

            if ($model->validate())
            {

                $user = new User();
                $user->load([
                    'User' => [
                        'first_name' => $model->first_name,
                        'last_name' => $model->last_name,
                        'sex' => $model->sex,
                        'email' => $model->email,
                        'mobile' => $model->mobile,
                        'region_id' => $model->regions_id,
                        'district_id' => $model->district,
                        'password' => Common::generate_hash_string( $model->password ),
                        '_status' => 10,
                    ]
                ]);

                $com = Company::findOne([
                    'email' => $company->email,
                ]);

                if (!isset( $com->id )) {
                    if ($company->validate()) {
                        if ($company->save()) {
                            $user->company_id = $company->id;
                        }
                    }} else {

                    //  if the company exist just take its details
                    $company = $com;
                    $user->company_id = $company->id;
                }

                //  This ensure that the response output are returned in form of JSON
                //  Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                //  return [ $company, $user];

                if ($user->validate())
                {

                    if ($user->save())
                    {

                        if (isset( $user->email, $user->id )){

                            $imail = Common::TheEmail(
                                    '//site/email/register',
                                    [
                                        'account'  => $user,
                                        'receiver' => $user->email,
                                        'urlink'   => Yii::$app->urlManager->createAbsoluteUrl([ '/site/user-activate-account', 'id' => $user->id ]),
                                        'subject'  => 'ESRF Account Activation',
                                    ]
                                );

                            $authass = new AuthAssignment();
                            $authass->load([
                                'AuthAssignment' => [
                                    'item_name' => 'User',
                                    'user_id'   => $user->id,
                                ]
                            ]);

                            if ($authass->validate()) {
                                if ($authass->save()) {

                                    return $this->redirect([
                                        '/site/registered-notice', 'id' => $user->id, 'status' => 'success'
                                    ]);
                                }
                            }

                        }

                        return $this->redirect([
                            '/site/registered-notice', 'id' => $user->id, 'status' => 'failed'
                        ]);

                    }

                }

                //  This ensure that the response output are returned in form of JSON
                //  Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                //  return [ $company, $user, $user->getErrors() ];

            }

        } else {

            $model->repassword = '';
            $model->password   = '';

        }

        //  return 100;
        return $this->render('register-institution', [
            'model' => $model,
            'category' => CompanyCategory::find()->where([ '_status' => 10 ])->all(),
            'regions'  => Region::find()->where([ 'status' => 10 ])->all(),
            'company' => $company,
        ]);

    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionFaq()
    {

        if (Yii::$app->user->isGuest) {
            $this->layout = 'main-register';
        }

        $searchModel = new FaqSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('//faq/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'whois' => 'applicant'
        ]);

    }

    public function actionRegisteredNotice($id)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = User::findOne($id);

        $this->layout = 'main-register';

        return $this->render('//site/registered-notice', [
            'model' => $model,
        ]);
    }

    public function actionPasswordResetNotice($id)
    {

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = User::findOne($id);

        $this->layout = 'main-register';

        return $this->render('//site/password-reset-notice', [
            'model' => $model,
        ]);

    }

    public function actionResendEmail($id)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = User::findOne($id);

        if (isset( $model->email, $model->id )) {

            $imail = Common::TheEmail(
                '//site/email/register',
                [
                    'account' => $model,
                    'receiver' => $model->email,
                    'urlink' => Yii::$app->urlManager->createAbsoluteUrl(['/site/user-activate-account', 'id' => $model->id]),
                    'subject' => 'ESRF Account Activation',
                ]
            );

            return $this->redirect([
                '/site/registered-notice', 'id' => $model->id, 'status' => 'success'
            ]);

        }

        return $this->redirect([
            '/site/registered-notice', 'id' => $model->id, 'status' => 'success'
        ]);

    }

    public function actionResendPwdresetEmail($id)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = User::findOne($id);

        if (isset( $model->email, $model->id )) {

            $sendEmail = Common::TheEmail(
                '//site/email/reset-password',
                [
                    'account' => $model,
                    'receiver' => [ [ 'email' => $model->email, 'first_name' => $model->first_name ] ],
                    'urlink' => Yii::$app->urlManager->createAbsoluteUrl(['/site/user-password-resetting', 'token' => $model->password_reset_token ]),
                    'subject' => 'ESRF Account Password Resetting',
                ]);

            return $this->redirect([
                '/site/password-reset-notice', 'id' => $model->id, 'status' => 'success'
            ]);

        }

        return $this->redirect([
            '/site/password-reset-notice', 'id' => $model->id, 'status' => 'success'
        ]);

    }

    public function actionUserPasswordResetting($token)
    {

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = User::findOne([
            'password_reset_token' => $token,
        ]);

        if (isset( $model->id )) {

            $password = Common::generateString();

            $model->password = Common::generate_hash_string( $password );

            if ($model->validate()) {
                if ($model->save()) {
                    //  - some code

                    $sendEmail = Common::TheEmail(
                        '//site/email/new-password',
                        [
                            'account' => $model,
                            'receiver' => [ [ 'email' => $model->email, 'first_name' => $model->first_name ] ],
                            'password' => $password,
                            'subject' => 'ESRF Account Password Resetting',
                        ]);

                }}

        }

        return $this->redirect([
            '/site/index'
        ]);

    }

    public function actionUserActivateAccount($id)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = User::findOne($id);

        if (isset( $model->id, $model->user_activated ))
        {
            $model->user_activated = 100;
            if ($model->validate())
            {
                $model->save();
            }
        }

        $this->layout = 'main-register';

        return $this->render('//site/user-activate-account', [
            'model' => null,
        ]);
    }

    /**
     * User sign up.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'main-login';

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * User sign up.
     *
     * @return mixed
     */
    public function actionForgetPassword()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'main-login';

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            return json_encode([
                'model' => $model,
            ]);
        } else {
            $model->password = '';

            return $this->render('forget-password', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSiignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {

        $this->layout = 'main-login';

        $model = new PasswordResetRequestForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {

            /* @var $user User */
            $user = User::findOne([
                'email'   => $model->email, '_status' => 10,
            ]);

            $user->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();

            if ($user->validate()) {
                if ($user->save()) {

                    $sendEmail = Common::TheEmail(
                            '//site/email/reset-password',
                            [
                                'account' => $user,
                                'receiver' => [ [ 'email' => $user->email, 'first_name' => $user->first_name ] ],
                                'urlink' => Yii::$app->urlManager->createAbsoluteUrl(['/site/user-password-resetting', 'token' => $user->password_reset_token ]),
                                'subject' => 'ESRF Account Password Resetting',
                            ]);

                    return $this->redirect([
                        '/site/password-reset-notice', 'id' => $user->id, 'status' => 'success'
                    ]);


                }}

        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);

    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {

        $this->layout = 'main-register';

        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }


    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }


    public function actionDistrictSearch()
    {

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $out = [];
        if(isset($_POST['depdrop_parents'])) {

            $parents = $_POST['depdrop_parents'];

            if(!empty($parents))
            {

                $company_id = (!empty( $parents[0] )) ? $parents[0] : null ;


                if( $company_id !== null ){

                    $departments = District::find()->where([
                        'region_id' => $company_id
                    ])->all();

                    foreach ($departments as $department)
                    {
                        $out[] = [
                            'id' => $department['id'], 'name' => $department['_name'],
                        ];
                    }

                }

                return [
                    'output' => $out, 'selected' => ''
                ];

            }

        }

        return [
            'output' => '', 'selected' => ''
        ];
    }

    /**
     * c2b m-pesa method
     *
     * @return mixed
     */
    public function actionMpesaC2b()
    {

        $publicKey = 'MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEArv9yxA69XQKBo24BaF/D+fvlqmGdYjqLQ5WtNBb5tquqGvAvG3WMFETVUSow/LizQalxj2ElMVrUmzu5mGGkxK08bWEXF7a1DEvtVJs6nppIlFJc2SnrU14AOrIrB28ogm58JjAl5BOQawOXD5dfSk7MaAA82pVHoIqEu0FxA8BOKU+RGTihRU+ptw1j4bsAJYiPbSX6i71gfPvwHPYamM0bfI4CmlsUUR3KvCG24rB6FNPcRBhM3jDuv8ae2kC33w9hEq8qNB55uw51vK7hyXoAa+U7IqP1y6nBdlN25gkxEA8yrsl1678cspeXr+3ciRyqoRgj9RD/ONbJhhxFvt1cLBh+qwK2eqISfBb06eRnNeC71oBokDm3zyCnkOtMDGl7IvnMfZfEPFCfg5QgJVk1msPpRvQxmEsrX9MQRyFVzgy2CWNIb7c+jPapyrNwoUbANlN8adU1m6yOuoX7F49x+OjiG2se0EJ6nafeKUXw/+hiJZvELUYgzKUtMAZVTNZfT8jjb58j8GVtuS+6TM2AutbejaCV84ZK58E2CRJqhmjQibEUO6KPdD7oTlEkFy52Y1uOOBXgYpqMzufNPmfdqqqSM4dU70PO8ogyKGiLAIxCetMjjm6FCMEA3Kc8K0Ig7/XtFm9By6VxTJK1Mg36TlHaZKP6VzVLXMtesJECAwEAAQ==';
        $apiKey = '6bc4157dbowkdd409118e0978dc6991a';

        $pesa = new Pesa([
            'api_key' => $apiKey,
            'public_key' => $publicKey,
        ]);


        $data = [
            'input_Amount' => 5000,
            'input_CustomerMSISDN' => '000000000001',
            'input_Country' => 'TZN',
            'input_Currency' => 'TZS',
            'input_ServiceProviderCode' => '000000',
            'input_TransactionReference' => 'T12344Z',
            'input_ThirdPartyConversationID' => '1e9b774d1da34af78412a498cbc28f5d',
            'input_PurchasedItemsDesc' => 'Test Three Item'
        ];

        try {
            return json_encode([
                'c2b' => $pesa->c2b($data)
            ]);
        } catch (Throwable $th) {
            return json_encode([
                'c2b' => $th->getMessage()
            ]);
        }

        return json_encode([
            'c2b' => $data
        ]);
    }


}

<?php

namespace frontend\controllers;

use app\models\UserSearch;
use common\models\Esrf;
use Yii;
use app\models\ApplicationReceiver;
use app\models\ApplicationReceiverSearch;
use yii\base\BaseObject;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ApplicationReceiverController implements the CRUD actions for ApplicationReceiver model.
 */
class ApplicationReceiverController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Displays a single ApplicationReceiver model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ApplicationReceiver model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionIndex()
    {
        // return json_encode([Esrf::applicationReceiverEmail()]);
        $model = new ApplicationReceiver();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $userModel = new UserSearch();
        $userProvider = $userModel->search(Yii::$app->request->queryParams);
        $userProvider->query->innerJoin('auth_assignment', 'auth_assignment.user_id = user.id');
        $userProvider->query->where([
            'auth_assignment.item_name' => [
                'Super Admin',
                'Admin',
                'Staff',
                'Reviewer'
            ]
        ]);

        $receiverModel = new ApplicationReceiverSearch();
        $receiverProvider = $receiverModel->search(Yii::$app->request->queryParams);

        return $this->render('create', [
            'model' => $model,
            'userProvider' => $userProvider,
            'userModel' => $userModel,

            'receiverProvider' => $receiverProvider,
            'receiverModel' => $receiverModel,
        ]);
    }

    /**
     * Creates a new ApplicationReceiver model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAdduser($id)
    {
        $model = new ApplicationReceiver();

        if ($model->load([
            'ApplicationReceiver' => [
                'receiver_id' => $id,
            ]
        ])) {
            if ($model->validate()) {
                if ($model->save()) {
                    return $this->redirect([
                        'index',
                    ]);
                }
            }
        }

        return $this->redirect([
            'index',
        ]);

    }

    /**
     * Updates an existing ApplicationReceiver model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ApplicationReceiver model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ApplicationReceiver model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ApplicationReceiver the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ApplicationReceiver::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

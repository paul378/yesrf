<?php

namespace frontend\controllers;

use app\models\AuthItem;
use app\models\CompanyCategory;
use common\models\Esrf;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\User;
use app\models\UserSearch;
use app\models\Region;

use common\models\UserUpdate;
use common\models\Common;


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;



/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->where([
            '_status' => [ 10, 20 ]
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'regions' => Region::find()->where([ 'status' => 10 ])->all(),
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);

    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post())) {

            $password = ((isset( $model->password ))? $model->password : null );

            $model->password = Common::generate_hash_string( $model->password );
            $model->role = 'Admin';

            if ($model->validate()) {
                if ($model->save()) {
                    return $this->redirect([
                        'view', 'id' => $model->id
                    ]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'roles' => AuthItem::find()
                ->where([
                    'type' => 2
                ])->all(),
            'regions' => Region::find()
                ->where([
                    'status' => 10
                ])->all(),
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);

        $model->scenario = 'update';

        if ($model->load(Yii::$app->request->post()))
        {

            if (isset( $model->company->full_name )) {

                $model->company->load(Yii::$app->request->post());

                if ($model->company->validate()) {
                    if ($model->company->save()) {

                    }}

            }

            if ($model->validate()) {

                if ($model->save())
                {

                    $roleupdate = Esrf::updateRole($model);

                    return $this->redirect([
                        'view', 'id' => $model->id
                    ]);
                }

            } else {

                //  return \GuzzleHttp\json_encode($model->getErrors());

            }

        }

        return $this->render( ((isset( $model->company->id ))? 'update-institution' : 'update' ), [
            'model' => $model,
            'roles' => AuthItem::find()
                ->where([
                    'type' => 2
                ])
                ->andFilterWhere([
                    'NOT IN', 'name', [ 'Super Admin' ]
                ])
                ->all(),

            'category' => CompanyCategory::find()->where([ '_status' => 10 ])->all(),
            'regions'  => Region::find()->where([ 'status' => 10 ])->all(),
            //  'company'  => $model->company,
        ]);

    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionChangePassword($id)
    {

        $model = $this->findModel($id);

        if (isset( $model->id ))
        {

            $common = new UserUpdate();
            $common->load(Yii::$app->request->post());
            if ( $common->validate() )
            {

                if (Common::verify_hash_string( $common->old_password, $model->password ))
                {

                    $model->password = Common::generate_hash_string( $common->new_password );

                    if ($model->validate()) {
                        $model->save();
                    }

                }

            }

        }

        return ((isset( $model->id ))? $this->redirect([ 'view', 'id' => $model->id ]) : $this->redirect([ 'view', 'id' => $id ]) );
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        return ((isset( $model->id ))? $this->redirect([ 'view', 'id' => $model->id ]) : $this->redirect([ 'view', 'id' => $id ]) );
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionProfile()
    {
        return $this->render('_profile', [
            'model' => $this->findModel( ((isset( Yii::$app->user->identity->id ))? Yii::$app->user->identity->id : null ) ),
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;


use app\models\AuthItem;
use app\models\AuthItemSearch;
use app\models\AuthItemChild;
use app\models\AuthItemChildSearch;
use app\models\AuthAssignment;
use app\models\AuthAssignmentSearch;

use app\models\Region;
use app\models\District;
use app\models\Category;
use app\models\User;

/**
 * AuthItemController implements the CRUD actions for AuthItem model.
 */
class AuthItemController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AuthItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Users not Available in Auth Assignment.
     * @return mixed
     */
    protected function getNOAssigned($id)
    {

        $authUsers = ArrayHelper::getColumn( AuthAssignment::find()->select(['user_id'])->where([ 'item_name' => $id ])->asArray()->all(), 'user_id' );
        return User::find()
        ->where([
            'NOT IN', 'id', $authUsers
        ])
        ->all();

    }

    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    public function actionAuthPermission($id)
    {

        $auths = new AuthAssignment();

        if ($auths->load(Yii::$app->request->post()) && $auths->save()) {
            return $this->redirect(['view', 'item_name' => $auths->item_name, 'user_id' => $auths->user_id]);
        }

        $model = AuthItem::find()
                ->where([
                    'name' => $id,
                    'type' => 2,
                ])->one();

        $childsearch = new AuthItemChildSearch();
        $child  = $childsearch->search(Yii::$app->request->queryParams);

        $child->query->where([
            'parent' => $id,
        ]);

        $usersearch = new AuthAssignmentSearch();
        $users  = $usersearch->search(Yii::$app->request->queryParams);

        $users->query->where([
            'item_name' => $id,
        ]);

        return $this->render('auth-permission', [
            'model' => $model,
            'authassign' => $this->getNOAssigned($id),
            'auths' => $auths,

            'usersearch' => $usersearch,
            'users' => $users,

            'childsearch' => $childsearch,
            'child' => $child,
        ]);

    }

    /**
     * Displays a single AuthItem model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AuthItem();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->name]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->name]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AuthItem::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

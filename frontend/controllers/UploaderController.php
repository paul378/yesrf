<?php
namespace frontend\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use app\models\Application;
use app\models\ApplicationDocument;

use app\models\ProposalCall;
use app\models\ProposalCallsInvitation;
use app\models\ProposalCallsReport;
use app\models\ProposalCallsTypes;

use app\models\User;


/**
 * Site controller
 */
class UploaderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {

        Yii::$app->request->enableCsrfValidation = false;
        return parent::beforeAction($action);

    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        return null ;

    }

    // RESPONSE FUNCTION
    function verbose($ok = 1, $info = "")
    {

        // THROW A 400 ERROR ON FAILURE
        if ($ok == 0) {
            http_response_code(400);
        }

        return [
            "ok" => $ok, "info" => $info
        ];

    }

    public function actionUploadDocument()
    {

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        //  output container
        $output = array();

        // INVALID UPLOAD
        if (empty($_FILES) || $_FILES['file']['error'])
        {
            return $this->verbose(0, "Failed to move uploaded file.");
        }

        // THE UPLOAD DESITINATION - CHANGE THIS TO YOUR OWN
        $filePath = __DIR__ .'/..'. DIRECTORY_SEPARATOR . "web/public/documents";
        if (!file_exists($filePath))
        {
            if (!mkdir($filePath, 0777, true)) {
                return $this->verbose(0, "Failed to create $filePath");
            }
        }

        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : $_FILES["file"]["name"];

        //  get ext
        $output['type'] = pathinfo( $fileName, PATHINFO_EXTENSION );
        $output['real_name'] = pathinfo( $fileName, PATHINFO_FILENAME );
        $output['name_time'] = md5( $fileName .''. time() .''. ((isset( \Yii::$app->user->identity->id ))? \Yii::$app->user->identity->id : rand() )) ;
        $output['name'] = $output['name_time'] .'.'. $output['type'] ;
        $output['path'] = $filePath . DIRECTORY_SEPARATOR . $output['name'] ;
        $output['src'] = '/public/documents/'. $output['name'];

        $output['href'] = Yii::$app->request->baseUrl . '/public/application/'. $output['name'];

        $filePath = $filePath . DIRECTORY_SEPARATOR . $fileName;

        // DEAL WITH CHUNKS
        $chunk  = isset($_REQUEST["chunk"]) ?  intval($_REQUEST["chunk"])  : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
        if ($out)
        {

            $in = @fopen($_FILES['file']['tmp_name'], "rb");
            if ($in) {
                while ($buff = fread($in, 4096)) { fwrite($out, $buff); }
            } else {
                return $this->verbose(0, "Failed to open input stream");
            }
            @fclose($in);
            @fclose($out);
            @unlink($_FILES['file']['tmp_name']);

        } else {
            return $this->verbose(0, "Failed to open output stream");
        }

        // CHECK IF FILE HAS BEEN UPLOADED
        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $output['path']);
        }

        return $this->verbose(1, $output );

    }

    public function actionApplicationDocument()
    {

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        //  output container
        $output = array();

        // INVALID UPLOAD
        if (empty($_FILES) || $_FILES['file']['error'])
        {
            return $this->verbose(0, "Failed to move uploaded file.");
        }

        // THE UPLOAD DESITINATION - CHANGE THIS TO YOUR OWN
        $filePath = __DIR__ .'/..'. DIRECTORY_SEPARATOR . "web/public/application";
        if (!file_exists($filePath))
        {
            if (!mkdir($filePath, 0777, true)) {
                return $this->verbose(0, "Failed to create $filePath");
            }
        }

        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : $_FILES["file"]["name"];

        //  get ext
        $output['type'] = pathinfo( $fileName, PATHINFO_EXTENSION );
        $output['real_name'] = pathinfo( $fileName, PATHINFO_FILENAME );
        $output['name_time'] = md5( $fileName .''. time() .''. ((isset( \Yii::$app->user->identity->id ))? \Yii::$app->user->identity->id : rand() )) ;
        $output['name'] = $output['name_time'] .'.'. $output['type'] ;
        $output['path'] = $filePath . DIRECTORY_SEPARATOR . $output['name'] ;
        $output['src'] = '/public/application/'. $output['name'];

        $output['href'] = Yii::$app->request->baseUrl . '/public/application/'. $output['name'];


        /*
            here we create instant of ApplicationDocument
            for uploading to be saved to databases
        */
        $model = new ApplicationDocument();
        $model->load(\Yii::$app->request->post());

        $model->filename = $output['real_name'];
        $model->filepath = $output['src'];


        $exist = ApplicationDocument::find()
                 ->where([
                    'application_id' => ((isset( $model->application_id ))? $model->application_id : null ),
                    'technicalfinacial' => ((isset( $model->technicalfinacial ))? $model->technicalfinacial : null ),
                ])->one();

        if (( $model->technicalfinacial == 'TECH') || ( $model->technicalfinacial == 'FINA'))
        {

            if ((isset( $exist->technicalfinacial )) && ($exist->technicalfinacial == $model->technicalfinacial))
            {

                $exist->filename = $output['real_name'];
                $exist->filepath = $output['src'];

                if ($exist->validate())
                {
                    if ($exist->save())
                    {
                        $output['application'] = $exist;
                    }
                }

            } else {

                if ($model->validate())
                {
                    if ($model->save())
                    {
                        $output['application'] = $model;
                    } else {
                        $output['application'] = 100;
                    }
                } else {
                        $output['application'] = 200;
                    }
        
            }

        } else {

            if ($model->validate())
            {
                if ($model->save())
                {
                    $output['application'] = $model;
                } else {
                        $output['application'] = 300;
                    }
            } else {
                        $output['application'] = [
                            400, $model->getErrors()
                        ];
                    }
    
        }


        $filePath = $filePath . DIRECTORY_SEPARATOR . $fileName;

        // DEAL WITH CHUNKS
        $chunk  = isset($_REQUEST["chunk"]) ?  intval($_REQUEST["chunk"])  : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
        if ($out)
        {

            $in = @fopen($_FILES['file']['tmp_name'], "rb");
            if ($in) {
                while ($buff = fread($in, 4096)) { fwrite($out, $buff); }
            } else {
                return $this->verbose(0, "Failed to open input stream");
            }
            @fclose($in);
            @fclose($out);
            @unlink($_FILES['file']['tmp_name']);

        } else {
            return $this->verbose(0, "Failed to open output stream");
        }

        // CHECK IF FILE HAS BEEN UPLOADED
        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $output['path']);
        }

        return $this->verbose(1, $output );

    }


    public function actionCallReport()
    {

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        //  output container
        $output = array();

        // INVALID UPLOAD
        if (empty($_FILES) || $_FILES['file']['error'])
        {
            return $this->verbose(0, "Failed to move uploaded file.");
        }

        // THE UPLOAD DESITINATION - CHANGE THIS TO YOUR OWN
        $filePath = __DIR__ .'/..'. DIRECTORY_SEPARATOR . "web/public/proposal";
        if (!file_exists($filePath))
        {
            if (!mkdir($filePath, 0777, true)) {
                return $this->verbose(0, "Failed to create $filePath");
            }
        }

        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : $_FILES["file"]["name"];

        //  get ext
        $output['type'] = pathinfo( $fileName, PATHINFO_EXTENSION );
        $output['real_name'] = pathinfo( $fileName, PATHINFO_FILENAME );
        $output['name_time'] = md5( $fileName .''. time() .''. ((isset( \Yii::$app->user->identity->id ))? \Yii::$app->user->identity->id : rand() )) ;
        $output['name'] = $output['name_time'] .'.'. $output['type'] ;
        $output['path'] = $filePath . DIRECTORY_SEPARATOR . $output['name'] ;
        $output['src'] = '/public/proposal/'. $output['name'];

        $output['href'] = Yii::$app->request->baseUrl . '/public/proposal/'. $output['name'];


        /*
            here we create instant of proposalDocument
            for uploading to be saved to databases
        */
        $model = new ProposalCallsReport();
        $model->load(\Yii::$app->request->post());

        $model->filename = $output['real_name'];
        $model->filepath = $output['src'];


        $exist = ProposalCallsReport::find()
                 ->where([
                    'proposal_calls_id' => ((isset( $model->proposal_calls_id ))? (int) $model->proposal_calls_id : null ),
                    'category' => ((isset( $model->category ))? $model->category : null ),
                ])->one();

        if (( $model->category == 'TECH') || ( $model->category == 'FINA'))
        {

            if ((isset( $exist->category )) && ($exist->category == $model->category))
            {

                $exist->filename = $output['real_name'];
                $exist->filepath = $output['src'];

                if ($exist->validate())
                {
                    if ($exist->save())
                    {
                        $output['proposal'] = $exist;
                    }
                }

            } else {

                if ($model->validate())
                {
                    if ($model->save())
                    {
                        $output['proposal'] = $model;
                    } else {
                        $output['proposal'] = 100;
                    }
                } else {
                        $output['proposal'] = 200;
                    }

            }

        } else {

            if ($model->validate())
            {
                if ($model->save())
                {
                    $output['proposal'] = $model;
                } else {
                        $output['proposal'] = 300;
                    }
            } else {
                $output['proposal'] = [
                    400, $model->getErrors()
                ];
            }

        }


        $filePath = $filePath . DIRECTORY_SEPARATOR . $fileName;

        // DEAL WITH CHUNKS
        $chunk  = isset($_REQUEST["chunk"]) ?  intval($_REQUEST["chunk"])  : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
        if ($out)
        {

            $in = @fopen($_FILES['file']['tmp_name'], "rb");
            if ($in) {
                while ($buff = fread($in, 4096)) { fwrite($out, $buff); }
            } else {
                return $this->verbose(0, "Failed to open input stream");
            }
            @fclose($in);
            @fclose($out);
            @unlink($_FILES['file']['tmp_name']);

        } else {
            return $this->verbose(0, "Failed to open output stream");
        }

        // CHECK IF FILE HAS BEEN UPLOADED
        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $output['path']);
        }

        return $this->verbose(1, $output );

    }


    public function actionFileUpdate()
    {

        $request =  json_decode( file_get_contents('php://input') );

        if (isset( $request->id ))
        {

            $model = ApplicationDocument::findOne( $request->id );

            if (isset( $model->id )) {

                $model->filename = ((isset( $request->filename ))? $request->filename : $model->filename );

                if ($model->validate())
                {
                    $model->save();
                }

                return json_encode([
                    'model' => $model->filename
                ]);
            } else {
                return json_encode($model);
            }

        } else {
            return json_encode($request);
        }

    }


    public function actionCallReportUpdate()
    {

        $request =  json_decode( file_get_contents('php://input') );

        if (isset( $request->id ))
        {

            $model = ProposalCallsReport::findOne( $request->id );

            if (isset( $model->id )) {

                $model->filename = ((isset( $request->filename ))? $request->filename : $model->filename );

                if ($model->validate())
                {
                    $model->save();
                }

                return json_encode([
                    'model' => $model->filename
                ]);
            } else {
                return json_encode($model);
            }

        } else {
            return json_encode($request);
        }

    }

}


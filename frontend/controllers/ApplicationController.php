<?php

namespace frontend\controllers;

use app\models\ApplicationDocumentSearch;
use app\models\NotificationTarget;
use app\models\ProposalCallsReportSearch;
use common\models\Common;
use common\models\Esrf;
use common\models\RejectComment;
use Yii;
use yii\base\BaseObject;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\Application;
use app\models\ApplicationDocument;
use app\models\ProposalCall;
use app\models\User;

use app\models\ApplicationSearch;

/**
 * ApplicationController implements the CRUD actions for Application model.
 */
class ApplicationController extends Controller
{

    public function behaviors()
    {

        // $behaviors=[];

        // $behaviors['verbs'] = [
        //     'class' => VerbFilter::className(),
        //     'actions' => [
        //         'delete' => ['post'],
        //         'delete-multiple' => ['post'],
        //     ],
        // ];


        // $behaviors['access'] = [

        //     'class' => \yii\filters\AccessControl::className(),

        //     'ruleConfig' => [
        //         'class' => \frontend\commands\rbac\rules\AccessRule::className(),
        //     ],

        //     'rules' => [

        //         [
        //             'allow' => true,
        //             'roles' => ['@'],
        //             'matchCallback' => function ($rule, $action) {

        //                 $module             = Yii::$app->controller->module->id;
        //                 $action             = Yii::$app->controller->action->id;
        //                 $controller         = Yii::$app->controller->id;

        //                 $route              = (( $module == "payment" )? $action ."-". \frontend\components\Inflect::singularize($controller) : $action ."-". \frontend\components\Inflect::singularize($controller) );

        //                 // $post = Yii::$app->request->post();

        //                 if (\Yii::$app->user->can($route)) {
        //                     return true;
        //                 }

        //             }
        //         ],

        //     ],

        // ];


        // return $behaviors;
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];

    }

    /**
     * Lists all Application models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ApplicationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->andFilterWhere([
            '_deleted' => 20,
            'user_id' => ((isset( Yii::$app->user->identity->id ))? Yii::$app->user->identity->id : null ),
            '_status'  => Yii::$app->request->get('appstatus', null),
        ]);

        return $this->render('//application/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Application models.
     * @return mixed
     */
    public function actionStaffIndex()
    {
        $searchModel = new ApplicationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->andFilterWhere([
            '_deleted' => 20,
            '_status'  => [
                30, 40, 50, 60, 70, 80, 90, 100
            ],
        ]);

        return $this->render('//application/staff-index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    /**
     * Displays a single Application model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found staff
     */
    public function actionView($id)
    {

        $model = $this->findModel($id);

        #   call report
        $searchReport = new ApplicationDocumentSearch();
        $reportProvider = $searchReport->search(Yii::$app->request->queryParams);
        $reportProvider->query->andFilterWhere([
            'application_id' => ((isset( $model->id ))? $model->id : '-1'),
            '_deleted' => 20,
            'technicalfinacial' => 'REPO',
            '_status'  => 10,
        ]);

        return $this->render(
            'view', [
            'reportProvider'  => $reportProvider,
            'model' => $model,
        ]);

    }

    /**
     * Displays a single Application model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found staff
     */
    public function actionStaffView($id)
    {
        return $this->render('staff-view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Application model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Application();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Application model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) 
        {
            if ($model->validate())
            {
                $model->save();
            }
        }

        if (isset($model->_status))
        {
            switch ($model->_status)
            {
                case 70:
                case 60:
                case 50:
                case 40:
                case 30:
                case 20:
                    return $this->redirect([
                        'view', 'id' => $model->id
                    ]);
                    break;
            }
        }

        return $this->render('update', [
            'model' => $model,
            'techdoc' => ApplicationDocument::find()
                        ->where([
                            'technicalfinacial' => 'TECH',
                            'application_id' => $id,
                        ])->one(),
            'finadoc' => ApplicationDocument::find()
                        ->where([
                            'technicalfinacial' => 'FINA',
                            'application_id' => $id,
                        ])->one(),
            'anydoc' => ApplicationDocument::find()
                        ->where([
                            'technicalfinacial' => 'ANY',
                            'application_id' => $id,
                        ])->all(),
        ]);
    }

    /**
     * Updates an existing Application model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAnyFile($pr)
    {

        return $this->renderPartial('//application/any-file-list', [

            'anydoc' => ApplicationDocument::find()
                        ->where([
                            'technicalfinacial' => 'ANY',
                            'application_id' => $pr,
                        ])->all(),

        ]);

    }

    /**
     * Deletes an existing ProposalCall model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPublish($id)
    {
        $model = $this->findModel($id);

        if (isset( $model->_status ))
        {
            $model->_status = 30;

            if ($model->save())
            {
                if ( $model->_status == 30 )
                {
                    $notice = Esrf::createPrivateNotification($model->user->id, $model->title .' Application Submitted' );
                    $receiver = Esrf::applicationReceiverEmail();

                    $imail = Common::TheEmail(
                        '//application/email/submit',
                        [
                            'account'  => $model->user,
                            'receiver' => $model->user->email,
                            'subject'  => 'Re: '. $model->title .' Application Submitted',
                            'apply'  => $model,
                        ]
                    );

                    $notice = Esrf::PrivateNotification( $receiver, $model->title .' Application Submitted' );
                    $imail = Common::TheEmail(
                        '//application/email/submit-admin',
                        [
                            'account'  => $model->user,
                            'receiver' => $receiver,
                            'subject'  => 'Re: '. $model->title .' Application Submitted',
                            'apply'  => $model,
                        ]
                    );
                }

            }
        }

        return $this->redirect([
            'view', 'id' => $model->id
        ]);
    }

    /**
     * Deletes an existing Application model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
     public function actionDelete($id)
     {
         $this->findModel($id)->delete();
 
         return $this->redirect(['index']);
     }

    /**
     * Deletes an existing Application model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeleteFiles($id, $ap)
    {

        $model = ApplicationDocument::find()
            ->where([
                'application_id' => $ap,
                'id' => $id,
            ])->one();

        if (isset( $model->filepath ))
        {
            // here we locate the file we want to delete
            if (unlink( __DIR__ . '/../web' . $model->filepath ))
            {
                $model->delete();
            }
        }

        return $this->redirect([
            '/application/update',
            'id' => $ap,
        ]);

    }

    /**
     * Deletes an existing Application model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionReviewApplication($id)
    {


        if ( Esrf::changeApplicationStatus($id, 60) )
        {

            $apply = $this->findModel($id);

            if (isset( $apply->id, $apply->title, $apply->user->email )) {

                $notice = Esrf::createPrivateNotification($apply->user->id, $apply->title .' Application On Reviews Status' );

                $imail = Common::TheEmail(
                    '//application/email/review',
                    [
                        'account'  => $apply->user,
                        'receiver' => $apply->user->email,
                        'subject'  => 'Re: '. $apply->title .' Application On Reviews Status',
                        'apply'  => $apply,
                    ]
                );

            }

            return $this->redirect([
                '/application/staff-index',
            ]);

        }

        return $this->redirect([
            '/application/staff-index',
        ]);

    }

    /**
     * Deletes an existing Application model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionApproveApplication($id)
    {


        if ( Esrf::changeApplicationStatus($id, 40) )
        {

            $apply = $this->findModel($id);

            if (isset( $apply->id, $apply->title, $apply->user->email )) {

                $notice = Esrf::createPrivateNotification($apply->user->id, $apply->title .' Application Approved' );

                $imail = Common::TheEmail(
                    '//application/email/approve',
                    [
                        'account'  => $apply->user,
                        'receiver' => $apply->user->email,
                        'subject'  => 'Re: '. $apply->title .' Application Approved',
                        'apply'  => $apply,
                    ]
                );

            }

            return $this->redirect([
                '/application/staff-index',
            ]);

        }

        return $this->redirect([
            '/application/staff-index',
        ]);

    }

    /**
     * Deletes an existing Application model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionRejectApplication($id)
    {


        if ( Esrf::changeApplicationStatus($id, 50) )
        {

            $apply = $this->findModel($id);

            if (isset( $apply->id, $apply->title, $apply->user->email )) {

                $notice = Esrf::createPrivateNotification($apply->user->id, $apply->title .' Application Rejected' );

                $imail = Common::TheEmail(
                    '//application/email/reject',
                    [
                        'account'  => $apply->user,
                        'receiver' => $apply->user->email,
                        'subject'  => 'Re: '. $apply->title .' Application Rejected',
                        'comment'  => ((isset( $comment ))? $comment : null ),
                        'apply'  => $apply,
                    ]
                );

            }

            return $this->redirect([
                '/application/staff-index',
            ]);

        }

        return $this->redirect([
            '/application/staff-index',
        ]);

    }
 
    /**
     * Deletes an existing Application model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionRejectWithComment()
    {

        $model = new RejectComment();
        $model->load(Yii::$app->request->post());

        //  return $model->comment;

        if ($model->validate())
        {

            if ( Esrf::changeApplicationStatus($model->proposal_id, 50) )
            {

                $apply = $this->findModel($model->proposal_id);

                if (isset( $apply->id, $apply->title, $apply->user->email ))
                {

                    $notice = Esrf::createPrivateNotification($apply->user->id, $apply->title .' Application Rejected' );

                    $imail = Common::TheEmail(
                        '//application/email/reject',
                        [
                            'account'  => $apply->user,
                            'receiver' => $apply->user->email,
                            'subject'  => 'Re: '. $apply->title .' Application Rejected',
                            'comment'  => $model->comment,
                            'apply'  => $apply,
                        ]
                    );

                }

                return $this->redirect([
                    '/application/staff-view',
                    'id' => $model->proposal_id
                ]);

            }

        }

        return $this->redirect([
            '/application/staff-index',
        ]);

    }

      /**
     * Finds the Application model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Application the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Application::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

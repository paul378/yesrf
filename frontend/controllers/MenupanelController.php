<?php

namespace frontend\controllers;

use Yii;
use app\models\MenuPanel;
use app\models\MenuPanelSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MenupanelController implements the CRUD actions for MenuPanel model.
 */
class MenupanelController extends Controller
{

    public function behaviors()
    {

        // $behaviors=[];

        // $behaviors['verbs'] = [
        //     'class' => VerbFilter::className(),
        //     'actions' => [
        //         'delete' => ['post'],
        //         'delete-multiple' => ['post'],
        //     ],
        // ];


        // $behaviors['access'] = [

        //     'class' => \yii\filters\AccessControl::className(),

        //     'ruleConfig' => [
        //         'class' => \frontend\commands\rbac\rules\AccessRule::className(),
        //     ],

        //     'rules' => [

        //         [
        //             'allow' => true,
        //             'roles' => ['@'],
        //             'matchCallback' => function ($rule, $action) {

        //                 $module             = Yii::$app->controller->module->id;
        //                 $action             = Yii::$app->controller->action->id;
        //                 $controller         = Yii::$app->controller->id;

        //                 $route              = (( $module == "payment" )? $action ."-". \frontend\components\Inflect::singularize($controller) : $action ."-". \frontend\components\Inflect::singularize($controller) );

        //                 // $post = Yii::$app->request->post();

        //                 if (\Yii::$app->user->can($route)) {
        //                     return true;
        //                 }

        //             }
        //         ],

        //     ],

        // ];


        // return $behaviors;
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];

    }

    /**
     * Lists all MenuPanel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MenuPanelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MenuPanel model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MenuPanel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MenuPanel();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MenuPanel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        try {
            
            if ($model->load(Yii::$app->request->post())) {

                $model->parent_id = ((isset( $model->parent_id ))? $model->parent_id : 0 );

                if ($model->validate()) {
                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }
            }

        } catch (\yii\db\Exception $e) {
            return json_encode($e);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MenuPanel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MenuPanel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MenuPanel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MenuPanel::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

<?php

namespace frontend\controllers;

use app\models\ApplicationSearch;
use app\models\ProposalCallsInvitationSearch;
use app\models\ProposalCallsReportSearch;
use Yii;
use yii\base\BaseObject;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;

use common\models\Esrf;
use common\models\InvitedUser;
use common\models\Common;

use app\models\ProposalCall;
use app\models\ProposalCallSearch;
use app\models\ProposalCallsTypes;
use app\models\ProposalCallsInvitation;
use app\models\ProposalCallsReport;
use app\models\Application;
use app\models\Category;

/**
 * ProposalCallController implements the CRUD actions for ProposalCall model.
 */
class ProposalCallController extends Controller
{

    public function behaviors()
    {

        // $behaviors=[];

        // $behaviors['verbs'] = [
        //     'class' => VerbFilter::className(),
        //     'actions' => [
        //         'delete' => ['post'],
        //         'delete-multiple' => ['post'],
        //     ],
        // ];


        // $behaviors['access'] = [

        //     'class' => \yii\filters\AccessControl::className(),

        //     'ruleConfig' => [
        //         'class' => \frontend\commands\rbac\rules\AccessRule::className(),
        //     ],

        //     'rules' => [

        //         [
        //             'allow' => true,
        //             'roles' => ['@'],
        //             'matchCallback' => function ($rule, $action) {

        //                 $module             = Yii::$app->controller->module->id;
        //                 $action             = Yii::$app->controller->action->id;
        //                 $controller         = Yii::$app->controller->id;

        //                 $route              = (( $module == "payment" )? $action ."-". \frontend\components\Inflect::singularize($controller) : $action ."-". \frontend\components\Inflect::singularize($controller) );

        //                 // $post = Yii::$app->request->post();

        //                 if (\Yii::$app->user->can($route)) {
        //                     return true;
        //                 }

        //             }
        //         ],

        //     ],

        // ];


        // return $behaviors;
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];

    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Lists all ProposalCall models.
     * @return mixed
     */
    public function actionIndex()
    {
        /*
            for applicant
        */
        if (!Yii::$app->user->can('index-proposal-call')) {
            return $this->redirect([
                '/proposal-call/index-applicant'
            ]);
        }

        $searchModel = new ProposalCallSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->andFilterWhere([
            '_deleted' => 20,
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all ProposalCall models.
     * @return mixed
     */
    public function actionIndexApplicant()
    {
        $searchModel = new ProposalCallSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->andFilterWhere([
            'types_id' => 10,
            '_deleted' => 20,
            '_status'  => [
                30
            ],
        ]);

        $sqlModel = new ProposalCallSearch();
        $sqlProvider = $sqlModel->search(Yii::$app->request->queryParams);

        $sqlProvider->query
            ->innerJoin('proposal_calls_invitation', 'proposal_calls_invitation.proposal_calls_id = proposal_call.id')
            ->where([
                'proposal_calls_invitation.user_id' => ((isset( Yii::$app->user->identity->id ))? Yii::$app->user->identity->id : null ),
                'proposal_calls_invitation._deleted' => 20,
                'proposal_call.types_id' => 20,
                'proposal_call._deleted' => 20,
                'proposal_call._status'  => [
                    30
                ],
            ])
            ->all();

        //  get combine the two query
        $combine = array_merge($dataProvider->getModels(), $sqlProvider->getModels());

        //  use Data Provide to Restructure the Data
        $comProvider = new ArrayDataProvider([
            'allModels' => $combine
        ]);


        return $this->render('index-applicant', [
            'searchModel' => $searchModel,
            'dataProvider' => $comProvider,
        ]);

    }

    /**
     * Lists all ProposalCall models.
     * @return mixed
     */
    public function actionIndexSql()
    {

        $searchModel = new ProposalCallSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->andFilterWhere([
            '_deleted' => 20,
            '_status'  => [
                30
            ],
        ]);

        $sqlModel = new ProposalCallSearch();
        $sqlProvider = $sqlModel->search(Yii::$app->request->queryParams);

        $sqlProvider->query->andFilterWhere([
            '_deleted' => 20,
            '_status'  => [
                40
            ],
        ]);

        //  get combine the two query
        $combine = array_merge($dataProvider->getModels(), $sqlProvider->getModels());

        //  use Data Provide to Restructure the Data
        $comProvider = new ArrayDataProvider([
            'allModels' => $combine
        ]);

        return json_encode([
            'combine' => $comProvider->getModels(),
            'one' => $dataProvider->getModels(),
            'two' => $sqlProvider->getModels(),
        ]);
    }

    /**
     * Displays a single ProposalCall model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $model = $this->findModel($id);

        $tarehe = Esrf::datedifferent( ((isset( $model->closed_date ))? $model->closed_date : '2000-12-31 00:00:00' ) );

        $usersearch = new InvitedUser();
        $output     = null ;
        $errorFound = null ;

        if ($usersearch->load(Yii::$app->request->post())) {

            if ($usersearch->validate()) {

                #   \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $output = $usersearch->inviteApplicant();
                //  return json_encode($output);

                foreach( $output as $out ) {
                    if (isset( $out['error'] )) {
                        $errorFound = true ;
                    }
                }

            }

        }

        #   clear user search
        $usersearch->email = '';

        #   get users invited
        $searchModel = new ProposalCallsInvitationSearch();
        $invitedUser = $searchModel->search(Yii::$app->request->queryParams);
        $invitedUser->query->andFilterWhere([
            'proposal_calls_id' => ((isset( $model->id ))? $model->id : '-1'),
            '_deleted' => 20,
            '_status'  => [
                10
            ],
        ]);

        #   call report
        $searchReport = new ProposalCallsReportSearch();
        $reportProvider = $searchReport->search(Yii::$app->request->queryParams);
        $reportProvider->query->andFilterWhere([
            'proposal_calls_id' => ((isset( $model->id ))? $model->id : '-1'),
            '_deleted' => 20,
            '_status'  => [
                10
            ],
        ]);

        #   list of application
        $applyModel = new ApplicationSearch();
        $applyProvider = $applyModel->search(Yii::$app->request->queryParams);
        $applyProvider->query->andFilterWhere([
            'proposal_id' => ((isset( $model->id ))? $model->id : '-1'),
            '_deleted' => 20,
            '_status'  => [
                30, 40, 50, 60, 70, 80, 90, 100
            ],
        ]);

        #   render view
        return $this->render(
            'view', [
                'model' => $model,
                'close' => ((isset( $tarehe['close'] ))? $tarehe['close'] : 100 ),
                'days'  => ((isset( $tarehe['days'] ))?  $tarehe['days']  : 0 ),
                'usersearch'  => $usersearch,
                'invitedUser'  => $invitedUser,
                'reportProvider'  => $reportProvider,
                'applyProvider'   => $applyProvider,
                'errorFound'  => $errorFound,
                'output'      => $output,
            ]
        );

    }

    /**
     * Deletes an existing ProposalCallsReport model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeleteReport($id)
    {
        $model = ProposalCallsReport::findOne($id);

        $copier = $model;
        $model->delete();

        return $this->redirect([
            'view',
            'id' => ((isset( $copier->proposal_calls_id ))? $copier->proposal_calls_id : null )
        ]);
    }

    /**
     * Deletes an existing ProposalCallsReport model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeleteInvitation($id)
    {
        $model = ProposalCallsInvitation::findOne($id);

        $copier = $model;
        $model->delete();

        return $this->redirect([
            'view',
            'id' => ((isset( $copier->proposal_calls_id ))? $copier->proposal_calls_id : null )
        ]);
    }

    /**
     * Creates a new ProposalCall model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        }

        $model = new ProposalCall();

        if ($model->load(Yii::$app->request->post())) {

            $model->creator_id = ((isset( Yii::$app->user->identity->id ))? Yii::$app->user->identity->id : null );
            $model->_status  = 10;

            if ($model->validate()) {

                try {

                    if ($model->save()) {

                        if ($model->_status == 30)
                        {

                            $userEmail = Esrf::userReceiverEmail();
                            $imail = Common::TheEmail(
                                '//application/email/calls-advert',
                                [
                                    'receiver' => $userEmail,
                                    'subject'  => 'Re: '. $model->title .' Application Submitted',
                                    'model'   => $model,
                                ]
                            );

                        }

                        return $this->redirect([
                            'view', 'id' => $model->id
                        ]);

                    }

                } catch (\yii\db\Exception $e) {

                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return [
                        'model' => $model,
                        'error' => $e,
                    ];

                }

            }

        }

        $calltype = ProposalCallsTypes::find()->where([ '_status' => 10, '_deleted' => 20 ])->all();
        $category = Category::find()->where([ '_status' => 10, '_deleted' => 20 ])->all();

        return $this->render('create', [
            'model' => $model,
            'calltype' => $calltype,
            'category' => $category,
        ]);
    }

    /**
     * Updates an existing ProposalCall model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->updator_id = ((isset( Yii::$app->user->identity->id ))? Yii::$app->user->identity->id : null );

            if ($model->validate()) {
                if ($model->save()) {

                    return $this->redirect([
                        'view', 'id' => $model->id
                    ]);

                }
            }

        }

        $calltype = ProposalCallsTypes::find()->where([ '_status' => 10, '_deleted' => 20 ])->all();
        $category = Category::find()->where([ '_status' => 10, '_deleted' => 20 ])->all();

        if ((isset($model->_status)) && ($model->_status != 10))
        {
            return $this->redirect([
                'view', 'id' => $model->id
            ]);
        }

        return $this->render('update', [
            'model' => $model,
            'calltype' => $calltype,
            'category' => $category,
        ]);
    }

    /**
     * Deletes an existing ProposalCall model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (isset( $model->_deleted ) && ($model->_deleted != 10))
        {
            $model->_deleted = 10;
            $model->save();
        }

        return $this->redirect([
            'index'
        ]);
    }

    /**
     * Deletes an existing ProposalCall model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPublish($id)
    {

        //  This ensure that the response output are returned in form of JSON
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = $this->findModel($id);

        //  GET all user who should receive notification via E-MAIL
        $userEmail = Esrf::userReceiverEmailWithName();

        if ( $model->types_id == 20 )
        {
            //  GET all user who should receive AN INVITED notification via E-MAIL
            $userEmail = Esrf::invitedReceiverEmail( $model->id );
        }

        //  return [
        //      $userEmail, $model
        //  ];

        if (isset( $model->_status ) && ( $model->_status == 10 ))
        {
            $model->_status = 30;

            if ($model->save())
            {

                if ($model->_status == 30)
                {

                    //  WRITE notification to USER
                    $notice = Esrf::PrivateNotification( $userEmail, 'There is a new call for '. $model->title .'' );
                    //  return \GuzzleHttp\json_encode($userEmail);

                    $imail = Common::TheEmail(
                        '//application/email/calls-advert',
                        [
                            'receiver' => $userEmail,
                            'subject'  => 'Re: '. $model->title .' - Call Advert',
                            'model'    => $model,
                        ]
                    );

                    //  return \GuzzleHttp\json_encode($userEmail);

                }

            }
        }

        return $this->redirect([
            'view', 'id' => $model->id
        ]);
    }

    /**
     * Deletes an existing ProposalCall model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSubmitApplication($id)
    {

        $model = $this->findModel($id);

        $apply = new Application();

        $apply->load([
            'Application' => [
                'title' => 'title',
                'description' => 'description',
                'proposal_id' => ((isset( $model->id ))? $model->id : null ),
                'user_id' => ((isset( Yii::$app->user->identity->id ))? Yii::$app->user->identity->id : null ),
            ]
        ]);

        if ($apply->validate())
        {

            $exist = Application::find()
                ->where([
                    'proposal_id' => $apply->proposal_id,
                    'user_id'     => $apply->user_id,
                ])->one();
            
            if (isset( $exist->id ))
            {
                return $this->redirect([
                    '/application/update', 'id' => $exist->id
                ]);
            }

            if ($apply->save())
            {
                return $this->redirect([
                    '/application/update', 'id' => $apply->id
                ]);
            }
        }

        return $this->redirect([
            'view', 'id' => $model->id
        ]);
    }

    /**
     * Finds the ProposalCall model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProposalCall the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProposalCall::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}




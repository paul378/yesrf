<?php
use yii\helpers\Html;
?>

<aside class="main-sidebar sidebar-dark-primary elevation-4" style="background-color: #dfe2e4;">
    <!-- Brand Logo -->
    <!-- <a href="index3.html" class="brand-link">
        <img src="< ?=$assetDir?>/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">ESRF</span>
    </a> -->
    <?= Html::a(Html::img('@web/logos.png', [ 'alt' => 'AdminLTE Logo', 'class' => 'brand-image', 'style' => 'opacity: .8;' ]) .' <span class="brand-text font-weight-bold">ESRF</span>', [ 'site/index' ], [ 'class' => 'brand-link', 'style' => 'color: rgb(25 24 24 / 80%);' ]); ?>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar user panel (optional) ->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="<?=$assetDir?>/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block" style="color: #474b52;">Alexander Pierce</a>
            </div>
        </div -->


        <!-- Sidebar user panel (optional) -->
        <!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="< ?=$assetDir?>/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Alexander Pierce</a>
            </div>
        </div> -->

        <!-- Sidebar Menu -->
        <nav class="mt-5">





            <?php
                echo frontend\components\NavWidget::widget([
                    'encodeLabels' => false,
                    'options' => [
                        'class' => 'nav nav-pills nav-sidebar flex-column',
                        'data-widget' => 'treeview',
                        'role' => 'menu',
                        'data-accordion' => 'false',
                    ],
                    'labelTemplate' => '<a href="#">{icon}{label}</a>',
                    'linkTemplate' => '<a {lenga} href="{url}">{icon}{label}</a>',
                    'submenuTemplate' => "\n<ul class=\"nav-item\">\n{items}\n</ul>\n",
                    'activateParents' => true,
                    'items' => app\models\MenuPanel::getMenu()
                ]);
            ?>



            <?= Html::beginForm(['/site/logout'], 'post') . Html::submitButton(
                '<i class="fas fa-sign-out-alt mr-2"></i>logout',
                [
                    'class' => 'btn btn-link logout mt-5 ml-2',
                    'style' => 'color: #484a4e!important;'
                ]
            ) . Html::endForm(); ?>


        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>


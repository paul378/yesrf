<?php
use yii\helpers\Html;
?>

<footer class="main-footer">
    <strong>Copyright &copy; 2021 <?= Html::a('ESRF', [ '/site/index' ], [ 'class' => '' ]); ?>.</strong>
    <div class="float-right d-none d-sm-inline-block">
    </div>
</footer>
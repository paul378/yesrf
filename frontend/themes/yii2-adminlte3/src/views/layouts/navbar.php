<?php

use yii\helpers\Html;

$notice = \app\models\NotificationTarget::find()
    ->where([
        'target_id' => ((isset( Yii::$app->user->identity->id ))? Yii::$app->user->identity->id : null ),
        'has_read' => 20,
    ])
    ->all();

?>
<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">

        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="Javascript: void(0);" class="nav-link">
                Login as : <strong><?= ((isset( Yii::$app->user->identity->role ))? Yii::$app->user->identity->role : 'Applicant' ); ?> ( <?= ((isset( Yii::$app->user->identity->first_name ))? Yii::$app->user->identity->first_name : '' ); ?> <?= ((isset( Yii::$app->user->identity->last_name ))? Yii::$app->user->identity->last_name : '' ); ?> ) </strong>
            </a>
        </li>
        <!-- li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">Contact</a>
        </li> -->

    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- Navbar Search -->
        <li class="nav-item">
            <a class="nav-link" data-widget="navbar-search" href="#" role="button">
                <i class="fas fa-search"></i>
            </a>
            <div class="navbar-search-block">
                <form class="form-inline">
                    <div class="input-group input-group-sm">
                        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                        <div class="input-group-append">
                            <button class="btn btn-navbar" type="submit">
                                <i class="fas fa-search"></i>
                            </button>
                            <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </li>

        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-bell"></i>
                <span class="badge badge-warning navbar-badge"><?= ((is_array( $notice ))? count($notice) : 0 ) ?></span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-header"><?= ((is_array( $notice ))? count($notice) : 0 ) ?> Notifications</span>
                <div class="dropdown-divider"></div>

                <?php foreach ($notice as $note) { ?>
                    <?= Html::a(
                            ((isset( $note->notification->message ))? '<i class="fas fa-envelope mr-2"></i>' . substr( $note->notification->message, 0, 36 )  : null ),
                            [
                                '//notification/view',
                                'id' => $note->id
                            ],
                            [
                                'class' => 'dropdown-item', 'style' => 'font-size: 12px;',
                            ]
                    ) ?>
                <?php } ?>

                <div class="dropdown-divider"></div>

            </div>
        </li>
        <li class="nav-item">
            <?= Html::a(Html::img( "$assetDir/img/AdminLTELogo.png", [ 'alt' => 'some', 'class' => 'brand-image img-circle elevation-3', 'style' => 'opacity: .8; width: 36px; margin: 0px;' ]), [ 'user/profile' ], [ 'class' => '', 'style' => 'margin-left: 10px;' ]); ?>
        </li>
    </ul>
</nav>
<!-- /.navbar -->
<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

\hail812\adminlte3\assets\AdminLteAsset::register($this);
\bedezign\yii2\audit\web\JSLoggingAsset::register($this);
$this->registerCssFile('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700');
$this->registerCssFile('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css');
\hail812\adminlte3\assets\PluginAsset::register($this)->add(['fontawesome', 'icheck-bootstrap']);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="hold-transition" style="background-color: #e9ecef; flex-direction: column; height: 100vh; padding-top: 100px;" >
<?php  $this->beginBody() ?>

<div class="login-box" style="margin: 5px auto;">
    <div class="login-logo">
        <div style="width: 120px; margin: 5px auto;">
            <?= Html::a(Html::img('@web/logo.png', [ 'alt' => 'some', 'class' => '', 'style' => 'width: 100%;' ]), [ 'site/index' ]); ?>
        </div>
    </div>
    <!-- /.login-logo -->

    <?= $content ?>
</div>
<!-- /.login-box -->

<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplicationSearch */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'ESRF | Login';
?>

<div class="card" style="margin-top: 1.5rem;">
    <div class="card-body login-card-body">

        <h1 style="text-align: center;">Sign In</h1>
        <p class="login-box-msg">Please sign in to proceed</p>

        <?php $form = \yii\bootstrap4\ActiveForm::begin(['id' => 'login-form']) ?>

            <?= $form->field($model,'username', [
                'options' => ['class' => 'form-group has-feedback'],
                'wrapperOptions' => ['class' => 'input-group mb-3']
            ])
                ->label(false)
                ->textInput(['placeholder' => $model->getAttributeLabel('E-mail')]) ?>

            <?= $form->field($model, 'password', [
                'options' => ['class' => 'form-group has-feedback'],
                'wrapperOptions' => ['class' => 'input-group mb-3']
            ])
                ->label(false)
                ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

            <div class="row">
                <div class="col-12" style="padding-top: 0.6rem; padding-bottom: 2.0rem;" >
                    <?= Html::submitButton( 'Sign In', [ 'class' => 'btn btn-success btn-block', 'style' => 'font-weight: bold;' ]) ?>
                </div>
            </div>

        <?php \yii\bootstrap4\ActiveForm::end(); ?>

        <p class="mb-1" style="padding-bottom: 1.0rem;">
            <?= Html::a('Forgot your password?', [ 'site/request-password-reset' ]) ?>
        </p>
        <p class="mb-0" class="text-center" style="text-align: center; padding: 1.0rem;" >
            You don't have an account? <?= Html::a('Sign up', [ '/site/sign-up' ], [ 'class' => '' ]); ?>
        </p>
    </div>
    <!-- /.login-card-body -->
</div>



<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\ApplicationSearch */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'ESRF | Sign Up';
?>

<div class="card" style="margin-top: 1.5rem; padding-top: 1.0rem;">
    <div class="card-body login-card-body">

        <h1 style="text-align: center;">Sign Up</h1>
        <p class="login-box-msg">Please sign in to proceed</p>

        <?php $form = \yii\bootstrap4\ActiveForm::begin(['id' => 'login-form']) ?>

            <div class="col-md-12">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= $form->field($model, 'first_name')->textInput(['placeholder' => "First name"])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= $form->field($model, 'last_name')->textInput(['placeholder' => "Last name"])->label(false) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= $form->field($model, 'email')->label(false)->textInput(['placeholder' => "E-mail"]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= $form->field($model, 'mobile')->widget(\yii\widgets\MaskedInput::class, [
                                'mask' => '+255 999 999 999',
                            ])->textInput(['placeholder' => "Phone number"])->label(false) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= $form->field($model, 'sex')->dropDownList(ArrayHelper::map(array(['id'=>'F','name'=>'Female'],['id'=>'M','name'=>'Male']),'id','name'), [ 'prompt' => 'Select Gender' ])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= $form->field($model, 'occupation')->textInput(['placeholder' => "Occupation"])->label(false) ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">


                            <select class="custom-select rounded-0" name="SignUp[regions_id]" id="regions_id">

                                <option>Select Region</option>
                                <?php
                                if (isset( $regions ) && is_array( $regions )) {
                                    foreach( $regions as $region) {
                                        if (isset( $region->_name, $region->id )) {
                                        ?>
                                        <option value="<?= $region->id ; ?>"><?= $region->_name ; ?></option>
                                        <?php
                                }}}
                                ?>

                            </select>



                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">

                            <?= $form->field($model, 'district')->widget(DepDrop::className(), [
                                'options' =>[ 'id' => 'district_id', 'prompt'=>'Select District' ],
                                'pluginOptions'=>[
                                    'depends'=>['regions_id'],
                                    'placeholder'=>'Select District',
                                    'url' => \Yii::$app->request->baseUrl . '/site/district-search'
                                ]
                            ])->label(false) ?>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= $form->field($model, 'password')->passwordInput(['placeholder' => "Password"])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= $form->field($model, 'repassword')->passwordInput(['placeholder' => "Confirm Password"])->label(false) ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group text-center" style="padding-top: 1.9rem;">
                            <div style="width: 260px; margin: 1px auto;">
                                <?= Html::submitButton( 'Sign Up', [ 'class' => 'btn btn-success btn-block', 'style' => 'font-weight: bold;' ]) ?>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        <?php \yii\bootstrap4\ActiveForm::end(); ?>

        <p class="mb-0" class="text-center" style="text-align: center; padding: 0.6rem;" >
            You have an account? <?= Html::a('Sign In', [ '/site/login' ], [ 'class' => '' ]); ?>
        </p>
        <br />

    </div>
    <!-- /.login-card-body -->
</div>
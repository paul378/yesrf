<?php

/* @var $this yii\web\View */

$this->title = '-';
?>
<div class="site-index mr-2 ml-2 mt-4">

    <div class="jumbotron pl-5">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'User - Sign Up';

\yii\web\YiiAsset::register($this);

?>
<style>
    .btn-lg {
        text-align: left;
        width: 100%;
    }
</style>
<div class="row">
    <div class="col-md-6">

        <?= Html::a(
                '<span class="fas fa-user-plus pull-left" style="font-size: 60px; margin:5px;"></span> For Individual<br><small>Sign Up</small>',
                [ '/site/sign-up-individual' ],
                ['class' => 'btn btn-outline-success btn-lg mt-5']
        ) ?>

    </div>
    <div class="col-md-6">

        <?= Html::a(
            '<span class="fas fa-industry pull-left" style="font-size: 60px; margin:5px;"></span> For Institution<br><small>Sign Up</small>',
            [ '/site/sign-up-institution' ],
            ['class' => 'btn btn-outline-warning btn-lg mt-5']
        ) ?>

    </div>
</div>

<?php
use yii\helpers\Html;
?>

<div class="card" style="margin-top: 1.5rem;">
    <div class="card-body login-card-body">

        <h5 style="text-align: left;">
            Reset Password
        </h5>
        <p class="">
            Please enter your email and press Reset Password
        </p>

        <?php $form = \yii\bootstrap4\ActiveForm::begin(['id' => 'login-form']) ?>

            <?= $form->field($model,'username', [
                'options' => ['class' => 'form-group has-feedback'],
                'wrapperOptions' => ['class' => 'input-group mb-3']
            ])
                ->label(false)
                ->textInput(['placeholder' => $model->getAttributeLabel('Enter email')]) ?>

            <div class="row">
                <div class="col-12" style="padding-top: 0.6rem; padding-bottom: 2.0rem;" >
                    <?= Html::submitButton( 'Reset Password', [ 'class' => 'btn btn-success btn-block', 'style' => 'font-weight: bold;' ]) ?>
                </div>
            </div>

        <?php \yii\bootstrap4\ActiveForm::end(); ?>

        <p class="mb-0" class="text-center" style="text-align: center; padding: 0.5rem;" >
            I remember password? <?= Html::a( 'Sign In', [ '/site/login' ], [ 'class' => '' ]); ?>
        </p>
    </div>
    <!-- /.login-card-body 0785 935969 - Jackob Mswima -->
</div>

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proposal_call".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $closed_date
 * @property string $docs_path
 * @property int $creator_id
 * @property int|null $updator_id
 * @property int $category_id
 * @property int $types_id
 * @property int $project_duration
 * @property int $_deleted 20. Not Deleted 10. Deleted
 * @property int $_status 30. Published 20. Not Active 10. Active
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Application[] $applications
 * @property User $creator
 * @property Category $category
 * @property User $updator
 * @property ProposalCallsTypes $types
 * @property ProposalCallsInvitation[] $proposalCallsInvitations
 * @property ProposalCallsReport[] $proposalCallsReports
 */
class ProposalCall extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proposal_call';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'closed_date', 'docs_path', 'creator_id', 'category_id', 'project_duration'], 'required'],
            [['description'], 'string'],
            [['closed_date', 'created_at', 'updated_at'], 'safe'],
            [['creator_id', 'updator_id', 'category_id', 'types_id', 'project_duration', '_deleted', '_status'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['docs_path'], 'string', 'max' => 200],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['updator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updator_id' => 'id']],
            [['types_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProposalCallsTypes::className(), 'targetAttribute' => ['types_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'closed_date' => 'Closed Date',
            'docs_path' => 'Docs Path',
            'creator_id' => 'Creator ID',
            'updator_id' => 'Updator ID',
            'category_id' => 'Category ID',
            'types_id' => 'Types ID',
            'project_duration' => 'Project Duration',
            '_deleted' => 'Deleted',
            '_status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Applications]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(Application::className(), ['proposal_id' => 'id']);
    }

    /**
     * Gets query for [[Creator]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * Gets query for [[Updator]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdator()
    {
        return $this->hasOne(User::className(), ['id' => 'updator_id']);
    }

    /**
     * Gets query for [[Types]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTypes()
    {
        return $this->hasOne(ProposalCallsTypes::className(), ['id' => 'types_id']);
    }

    /**
     * Gets query for [[ProposalCallsInvitations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProposalCallsInvitations()
    {
        return $this->hasMany(ProposalCallsInvitation::className(), ['proposal_calls_id' => 'id']);
    }

    /**
     * Gets query for [[ProposalCallsReports]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProposalCallsReports()
    {
        return $this->hasMany(ProposalCallsReport::className(), ['proposal_calls_id' => 'id']);
    }
}

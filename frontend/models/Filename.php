<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "filename".
 *
 * @property int $id
 * @property string $_name
 * @property string|null $extension
 * @property int $_deleted
 * @property int $_status
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property ApplicationDocument[] $applicationDocuments
 */
class Filename extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'filename';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', '_name'], 'required'],
            [['id', '_deleted', '_status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['_name', 'extension'], 'string', 'max' => 50],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            '_name' => 'Name',
            'extension' => 'Extension',
            '_deleted' => 'Deleted',
            '_status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[ApplicationDocuments]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApplicationDocuments()
    {
        return $this->hasMany(ApplicationDocument::className(), ['filename_id' => 'id']);
    }
}

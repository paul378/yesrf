<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proposal_calls_report".
 *
 * @property int $id
 * @property int $proposal_calls_id
 * @property string|null $filename
 * @property string $filepath
 * @property string $category TECH - Technical FINA - Finacial ANY - Any file of which is option
 * @property int $_deleted
 * @property int $_status
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property ProposalCall $proposalCalls
 */
class ProposalCallsReport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proposal_calls_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['proposal_calls_id', 'filepath', 'category'], 'required'],
            [['proposal_calls_id', '_deleted', '_status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['filename', 'filepath'], 'string', 'max' => 200],
            [['category'], 'string', 'max' => 4],
            [['proposal_calls_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProposalCall::className(), 'targetAttribute' => ['proposal_calls_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'proposal_calls_id' => 'Proposal Calls ID',
            'filename' => 'Filename',
            'filepath' => 'Filepath',
            'category' => 'Category',
            '_deleted' => 'Deleted',
            '_status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[ProposalCalls]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProposalCalls()
    {
        return $this->hasOne(ProposalCall::className(), ['id' => 'proposal_calls_id']);
    }
}

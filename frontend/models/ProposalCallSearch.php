<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProposalCall;

/**
 * ProposalCallSearch represents the model behind the search form of `app\models\ProposalCall`.
 */
class ProposalCallSearch extends ProposalCall
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'creator_id', 'updator_id', 'category_id', 'project_duration', '_deleted', '_status'], 'integer'],
            [['title', 'description', 'closed_date', 'docs_path', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProposalCall::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'closed_date' => $this->closed_date,
            'creator_id' => $this->creator_id,
            'updator_id' => $this->updator_id,
            'category_id' => $this->category_id,
            'project_duration' => $this->project_duration,
            '_deleted' => $this->_deleted,
            '_status' => $this->_status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'docs_path', $this->docs_path]);

        return $dataProvider;
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "application_document".
 *
 * @property int $id
 * @property int $application_id
 * @property string|null $filename
 * @property string $filepath
 * @property string $technicalfinacial TECH - Technical FINA - Finacial ANY - Any file of which is option
 * @property int $_deleted
 * @property int $_status
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Application $application
 */
class ApplicationDocument extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'application_document';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['application_id', 'filepath', 'technicalfinacial'], 'required'],
            [['application_id', '_deleted', '_status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['filename'], 'string', 'max' => 100],
            [['filepath'], 'string', 'max' => 200],
            [['technicalfinacial'], 'string', 'max' => 4],
            [['application_id'], 'exist', 'skipOnError' => true, 'targetClass' => Application::className(), 'targetAttribute' => ['application_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'application_id' => 'Application ID',
            'filename' => 'Filename',
            'filepath' => 'Filepath',
            'technicalfinacial' => 'Technicalfinacial',
            '_deleted' => 'Deleted',
            '_status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Application]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApplication()
    {
        return $this->hasOne(Application::className(), ['id' => 'application_id']);
    }
}

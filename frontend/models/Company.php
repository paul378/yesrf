<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $full_name
 * @property int $category_id
 * @property string $email
 * @property string $mobile
 * @property int $_deleted
 * @property int $_status
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property CompanyCategory $category
 * @property User[] $users
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['full_name', 'category_id', 'email', 'mobile'], 'required'],
            [['category_id', '_deleted', '_status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['full_name'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 200],
            [['mobile'], 'string', 'max' => 20],
            [['email'], 'unique'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'Full Name',
            'category_id' => 'Category ID',
            'email' => 'Email',
            'mobile' => 'Mobile',
            '_deleted' => 'Deleted',
            '_status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(CompanyCategory::className(), ['id' => 'category_id']);
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['company_id' => 'id']);
    }
}

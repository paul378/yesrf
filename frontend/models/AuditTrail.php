<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "audit_trail".
 *
 * @property int $id
 * @property int|null $entry_id
 * @property int|null $user_id
 * @property string $action
 * @property string $model
 * @property string $model_id
 * @property string|null $field
 * @property string|null $old_value
 * @property string|null $new_value
 * @property string $created
 */
class AuditTrail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'audit_trail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entry_id', 'user_id'], 'integer'],
            [['action', 'model', 'model_id', 'created'], 'required'],
            [['old_value', 'new_value'], 'string'],
            [['created'], 'safe'],
            [['action', 'model', 'model_id', 'field'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entry_id' => 'Entry ID',
            'user_id' => 'User ID',
            'action' => 'Action',
            'model' => 'Model',
            'model_id' => 'Model ID',
            'field' => 'Field',
            'old_value' => 'Old Value',
            'new_value' => 'New Value',
            'created' => 'Created',
        ];
    }
}

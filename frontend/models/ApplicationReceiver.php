<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "application_receiver".
 *
 * @property int $id
 * @property int $receiver_id
 * @property int $_deleted
 * @property int $_status
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property User $receiver
 */
class ApplicationReceiver extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'application_receiver';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['receiver_id'], 'required'],
            [['receiver_id', '_deleted', '_status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['receiver_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['receiver_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'receiver_id' => 'Receiver ID',
            '_deleted' => 'Deleted',
            '_status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Receiver]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReceiver()
    {
        return $this->hasOne(User::className(), ['id' => 'receiver_id']);
    }
}

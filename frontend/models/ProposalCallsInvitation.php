<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proposal_calls_invitation".
 *
 * @property int $id
 * @property int $proposal_calls_id
 * @property int|null $user_id
 * @property int $_deleted
 * @property int $_status
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property ProposalCall $proposalCalls
 * @property User $user
 */
class ProposalCallsInvitation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proposal_calls_invitation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['proposal_calls_id'], 'required'],
            [['proposal_calls_id', 'user_id', '_deleted', '_status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['proposal_calls_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProposalCall::className(), 'targetAttribute' => ['proposal_calls_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'proposal_calls_id' => 'Proposal Calls ID',
            'user_id' => 'User ID',
            '_deleted' => 'Deleted',
            '_status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[ProposalCalls]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProposalCalls()
    {
        return $this->hasOne(ProposalCall::className(), ['id' => 'proposal_calls_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

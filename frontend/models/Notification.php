<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property int $id
 * @property int $for_public
 * @property string $message
 * @property string|null $sender
 * @property int $_deleted
 * @property int $_status
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property NotificationTarget[] $notificationTargets
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['for_public', '_deleted', '_status'], 'integer'],
            [['message'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['message'], 'string', 'max' => 200],
            [['sender'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'for_public' => 'For Public',
            'message' => 'Message',
            'sender' => 'Sender',
            '_deleted' => 'Deleted',
            '_status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[NotificationTargets]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationTargets()
    {
        return $this->hasMany(NotificationTarget::className(), ['notification_id' => 'id']);
    }
}

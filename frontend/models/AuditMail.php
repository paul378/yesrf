<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "audit_mail".
 *
 * @property int $id
 * @property int $entry_id
 * @property string $created
 * @property int $successful
 * @property string|null $from
 * @property string|null $to
 * @property string|null $reply
 * @property string|null $cc
 * @property string|null $bcc
 * @property string|null $subject
 * @property resource|null $text
 * @property resource|null $html
 * @property resource|null $data
 */
class AuditMail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'audit_mail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entry_id', 'created', 'successful'], 'required'],
            [['entry_id', 'successful'], 'integer'],
            [['created'], 'safe'],
            [['text', 'html', 'data'], 'string'],
            [['from', 'to', 'reply', 'cc', 'bcc', 'subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entry_id' => 'Entry ID',
            'created' => 'Created',
            'successful' => 'Successful',
            'from' => 'From',
            'to' => 'To',
            'reply' => 'Reply',
            'cc' => 'Cc',
            'bcc' => 'Bcc',
            'subject' => 'Subject',
            'text' => 'Text',
            'html' => 'Html',
            'data' => 'Data',
        ];
    }
}

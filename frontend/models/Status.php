<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "status".
 *
 * @property int $id
 * @property string $_name
 * @property string $controller
 * @property string|null $tag_color
 * @property int $_deleted
 * @property int $_status
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Application[] $applications
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', '_name', 'controller'], 'required'],
            [['id', '_deleted', '_status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['_name', 'controller', 'tag_color'], 'string', 'max' => 50],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            '_name' => 'Name',
            'controller' => 'Controller',
            'tag_color' => 'Tag Color',
            '_deleted' => 'Deleted',
            '_status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Applications]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(Application::className(), ['_status' => 'id']);
    }
}

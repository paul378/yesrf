<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "application".
 *
 * @property int $id
 * @property int|null $proposal_id
 * @property int $user_id
 * @property string $title
 * @property string $description
 * @property int $_deleted
 * @property int $_status
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property ProposalCall $proposal
 * @property User $user
 * @property Status $status
 * @property ApplicationDocument[] $applicationDocuments
 */
class Application extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'application';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['proposal_id', 'user_id', '_deleted', '_status'], 'integer'],
            [['user_id', 'title', 'description'], 'required'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 150],
            [['proposal_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProposalCall::className(), 'targetAttribute' => ['proposal_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['_status'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['_status' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'proposal_id' => 'Proposal ID',
            'user_id' => 'User ID',
            'title' => 'Title',
            'description' => 'Description',
            '_deleted' => 'Deleted',
            '_status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Proposal]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProposal()
    {
        return $this->hasOne(ProposalCall::className(), ['id' => 'proposal_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[Status]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => '_status']);
    }

    /**
     * Gets query for [[ApplicationDocuments]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApplicationDocuments()
    {
        return $this->hasMany(ApplicationDocument::className(), ['application_id' => 'id']);
    }
}

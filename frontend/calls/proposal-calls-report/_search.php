<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProposalCallsReportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proposal-calls-report-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'proposal_calls_id') ?>

    <?= $form->field($model, 'filename') ?>

    <?= $form->field($model, 'filepath') ?>

    <?= $form->field($model, 'category') ?>

    <?php // echo $form->field($model, '_deleted') ?>

    <?php // echo $form->field($model, '_status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

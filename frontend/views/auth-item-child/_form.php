<?php

    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use app\models\AuthItem;
    use yii\helpers\ArrayHelper;
    use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\AuthItemChild */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auth-item-child-form">

    <?php $form = ActiveForm::begin(); ?>



    <div class="form-group">
        
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <?=$form->field($model, 'parent')->dropDownList(ArrayHelper::map(AuthItem::find()->where('type=:t',[':t'=>2])->all(),'name','name'), [ 'prompt' => 'Please choose role' ]) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <?=$form->field($model, 'child')->dropDownList(ArrayHelper::map(AuthItem::find()->where('type=1')->all(),'name','name'), [ 'prompt' => 'Please choose permission' ]) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

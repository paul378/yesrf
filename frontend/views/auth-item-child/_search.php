<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AuthItemChildSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auth-item-child-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        
        <div class="col-md-3">
            <?= $form->field($model, 'parent')->textInput([ 'placeholder' => "Search By Role" ])->label(false); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'child')->textInput([ 'placeholder' => "Search By Permission Route" ])->label(false); ?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('Search', [ 'class' => 'btn btn-primary' ]) ?>
                <?= Html::a('Binding Role & Permission', [ 'create' ], [ 'class' => 'btn btn-success' ]) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AuthItemChild */

$this->title = 'Role & Permission';
$this->params['breadcrumbs'][] = ['label' => 'Role & Permission', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Binding Role & Permission';
?>
<div class="auth-item-child-create mr-2 ml-2 mt-4">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

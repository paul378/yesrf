<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AuthItemChild */

$this->title = $model->parent . '\'s Role Detail' ;
$this->params['breadcrumbs'][] = ['label' => 'Role & Permission', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->parent, 'url' => ['view', 'parent' => $model->parent, 'child' => $model->child]];
$this->params['breadcrumbs'][] = $model->parent . ' Update Form';
?>
<div class="auth-item-child-update mr-2 ml-2 mt-4">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Dashboard';
$this->params['breadcrumbs'] = [[ 'label' => $this->title ]];
?>
<div class="container-fluid">


    <div class="row">

        <div class="col-md-3 col-sm-6 col-12">
            <div id="w2" class="small-box bg-primary">
                <div class="inner"><h3 style="visibility: hidden">0</h3><p>TOTAL NUMBER<br/>OF APPLICATION</p></div>
                <div class="icon"><i class="fa fa" style="color: white;"><?= ((isset( $applicationStatusStatistic->approved, $applicationStatusStatistic->submitted, $applicationStatusStatistic->rejected ))? ( ((float) $applicationStatusStatistic->approved) + ((float) $applicationStatusStatistic->submitted) + ((float) $applicationStatusStatistic->rejected) ) : 0 ); ?></i></div>

            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-12">
            <div id="w3" class="small-box bg-success">
                <div class="inner"><h3 style="visibility: hidden">0</h3><p>TOTAL<br>APPLICATION</p></div>
                <div class="icon"><i class="fa fa" style="color: white;"><?= ((isset( $applicationStatusStatistic->approved ))? $applicationStatusStatistic->approved : 0 ); ?></i></div>

            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-12">
            <div id="w2" class="small-box bg-warning">
                <div class="inner"><h3 style="visibility: hidden">0</h3><p>PENDING<br>APPLICATION</p></div>
                <div class="icon"><i class="fa fa" style="color: white;"><?= ((isset( $applicationStatusStatistic->submitted ))? $applicationStatusStatistic->submitted : 0 ); ?></i></div>

            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-12">
            <div id="w3" class="small-box bg-danger">
                <div class="inner"><h3 style="visibility: hidden">0</h3><p>REJECTED<br>APPLICATION</p></div>
                <div class="icon"><i class="fa fa" style="color: white;"><?= ((isset( $applicationStatusStatistic->rejected ))? $applicationStatusStatistic->rejected : 0 ); ?></i></div>

            </div>
        </div>

    </div>





<div class="row">

    <div class="col-md-8 col-sm-6 col-12">

        <div class="card">
            <div class="card-header" style="border-bottom: none;">
                <h3 class="card-title">
                    <span class="far fa-file" style="margin-right: 10px; font-weight: bold; color: green;"></span> My Application
                </h3>
                <div class="card-tools">
                    <!-- Buttons, labels, and many other things can be placed here! -->
                    <!-- Here is a label for example -->
                    <span class="badge badge-primary">Label</span>
                </div>
                <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">

                <?= GridView::widget([
                    'tableOptions' => [
                        'class' => 'table table-striped',
                    ],
                    'options' => [
                        'class' => 'table-responsive',
                    ],
                    'summary' => false,
                    'dataProvider' => $dataProviderApp,
                    'columns' => [

                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'created_at',
                            'label' => 'Date',
                            'format' => 'html',
                            'value' => function($model) {
                                return ((isset( $model->created_at ))? Yii::$app->formatter->asDate( $model->created_at, 'd-MM-Y' ) : null ) ;
                            }
                        ],
                        [
                            'attribute' => 'title',
                            'label' => 'Proposal Title',
                            'format' => 'html',
                            'value' => function($model) {
                                return Html::a( ((isset( $model->title ))? $model->title : null ), [ '/application/update', 'id' => $model->id ], [ 'class' => '' ]) ;
                            }
                        ],
                        [
                            'attribute' => 'title',
                            'label' => 'Proposal Call',
                            'format' => 'html',
                            'value' => function($model) {
                                return ((isset( $model->proposal->title ))? $model->proposal->title : null ) ;
                            }
                        ],
                        [
                            'attribute' => '_status',
                            'label' => 'Status',
                            'format' => 'html',
                            'value' => function($model) {
                                return ((isset( $model->status->_name ))? '<span class="badge badge-' . $model->status->tag_color . ' mr-2">' . $model->status->_name . '</span>' : null ) ;
                            }
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            // 'contentOptions' => [ 'style' => 'width: 8.7%' ],
                            'visible' => Yii::$app->user->isGuest ? false : true,
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return Html::a('<i class="fas fa-eye"></i>', [ '/application/view', 'id' => $model->id ], [ 'class' => '' ]);
                                },
                                'update' => function ($url, $model) {
                                    return null;
                                },
                                'delete' => function ($url, $model) {
                                    return null;
                                },
                            ],
                        ],

                    ],
                ]); ?>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
            </div>
            <!-- /.card-footer -->
        </div>
        <!-- /.card -->

    </div>
    <div class="col-md-4 col-sm-6 col-12">

        <div class="card">
            <div class="card-header" style="border-bottom: none;">
                <h3 class="card-title">
                    <span class="far fa-edit" style="margin-right: 10px; font-weight: bold; color: green;"></span> Recent call for proposal
                </h3>
                <div class="card-tools">
                    <!-- Buttons, labels, and many other things can be placed here! -->
                    <!-- Here is a label for example -->
                    <span class="badge badge-primary">Label</span>
                </div>
                <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">

                <?= GridView::widget([
                    'tableOptions' => [
                        'class' => 'table table-striped',
                    ],
                    'options' => [
                        'class' => 'table-responsive',
                    ],
                    'summary' => false,
                    'dataProvider' => $dataProviderCal,
                    'columns' => [

                        [
                            'attribute' => 'created_at',
                            'label' => 'Date',
                            'format' => 'html',
                            'value' => function($model) {
                                return ((isset( $model->created_at ))? Yii::$app->formatter->asDate( $model->created_at, 'd-MM-Y' ) : null ) ;
                            }
                        ],
                        [
                            'attribute' => 'title',
                            'label' => 'Call Title',
                            'format' => 'html',
                            'value' => function($model) {
                                return ((isset( $model->title ))? $model->title : null ) ;
                            }
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            // 'contentOptions' => [ 'style' => 'width: 8.7%' ],
                            'visible' => Yii::$app->user->isGuest ? false : true,
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return Html::a('<i class="fas fa-eye"></i>', [ '/proposal-call/view', 'id' => $model->id ], [ 'class' => '' ]);
                                },
                                'update' => function ($url, $model) {
                                    return null;
                                },
                                'delete' => function ($url, $model) {
                                    return null;
                                },
                            ],
                        ],

                    ],
                ]); ?>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
            </div>
            <!-- /.card-footer -->
        </div>
        <!-- /.card -->

    </div>

</div>

</div>


<?php

/* @var $this yii\web\View */

$this->title = 'Welcome Message';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Reset Password!</h1>

        <p class="lead">We have successfully send you ESRF Password Reset Link, now you have to open your email account and click the link to reset your password.</p>

        <p>
            <a class="btn btn-success" href="<?= Yii::$app->urlManager->createAbsoluteUrl([ '/site/login' ]); ?>">Login now</a>
            <a class="btn btn-warning" href="<?= Yii::$app->urlManager->createAbsoluteUrl([ '/site/resend-pwdreset-email', 'id' => ((isset( $model->id ))? $model->id : null ) ]); ?>">Resend E-mail</a>
        </p>
    </div>

</div>

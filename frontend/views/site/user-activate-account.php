<?php

/* @var $this yii\web\View */

$this->title = 'Welcome Message';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome!</h1>

        <p class="lead">Thanks for activating your ESRF Account. Your account is now active</p>

        <p><a class="btn btn-lg btn-success" href="https://tangayetu.esrf.or.tz/yesrf/site/login">Login now</a></p>
    </div>

</div>

<?php
use yii\helpers\Html;
use scotthuangzl\googlechart\GoogleChart;
use dosamigos\chartjs\ChartJs;

$this->title = 'Dashboard';
$this->params['breadcrumbs'] = [[ 'label' => $this->title ]];
?>
<div class="container-fluid">


    <?php // echo $this->registerCss(".highcharts-credits { display: none !important; }"); ?>


    <div class="row">
        <div class="col-md-3 col-sm-6 col-12">
            <?= \hail812\adminlte\widgets\SmallBox::widget([
                'title' => ((isset( $esrfdata->applicant->usercounter ))? $esrfdata->applicant->usercounter : 0 ),
                'text' => 'TOTAL NUMBER<br/>OF APPLICANT',
                'icon' => 'fa fa',
                'theme' => 'primary',
                'linkUrl' => Yii::$app->request->baseUrl . '/user/applicant-index',

                'titleStyle' => 'visibility: hidden',
                'iconStyles' => 'color: white;',
                'iconNumber' => ((isset( $esrfdata->applicant->usercounter ))? $esrfdata->applicant->usercounter : 0 ),
            ]) ?>
        </div>
        <div class="col-md-3 col-sm-6 col-12">
            <?= \hail812\adminlte\widgets\SmallBox::widget([
                'title' => ((isset( $esrfdata->application->idcounter ))? $esrfdata->application->idcounter : 0 ),
                'text' => 'TOTAL<br/>APPLICATION',
                'icon' => 'fa fa',
                'theme' => 'success',
                'linkUrl' => Yii::$app->request->baseUrl . '/application/staff-index',

                'titleStyle' => 'visibility: hidden',
                'iconStyles' => 'color: white;',
                'iconNumber' => ((isset( $esrfdata->application->idcounter ))? $esrfdata->application->idcounter : 0 ),
            ]) ?>
        </div>
        <div class="col-md-3 col-sm-6 col-12">
            <?= \hail812\adminlte\widgets\SmallBox::widget([
                'title' => ((isset( $esrfdata->calls->idcounter ))? $esrfdata->calls->idcounter : 0 ),
                'text' => 'TOTAL<br/>CALLS',
                'icon' => 'fa fa',
                'theme' => 'warning',
                'linkUrl' => Yii::$app->request->baseUrl . '/proposal-call/index',

                'titleStyle' => 'visibility: hidden',
                'iconStyles' => 'color: white;',
                'iconNumber' => ((isset( $esrfdata->calls->idcounter ))? $esrfdata->calls->idcounter : 0 ),
            ]) ?>
        </div>
        <div class="col-md-3 col-sm-6 col-12">
            <?= \hail812\adminlte\widgets\SmallBox::widget([
                'title' => ((isset( $esrfdata->staff->idcounter ))? $esrfdata->staff->idcounter : 0 ),
                'text' => 'TOTAL NUMBER<br/>OF STAFF',
                'icon' => 'fa fa',
                'theme' => 'danger',
                'linkUrl' => Yii::$app->request->baseUrl . '/user/index?userstatus=50',

                'titleStyle' => 'visibility: hidden',
                'iconStyles' => 'color: white;',
                'iconNumber' => ((isset( $esrfdata->staff->idcounter ))? $esrfdata->staff->idcounter : 0 ),
            ]) ?>
        </div>
    </div>







    <div class="row" style="min-height: 260px;">

        <div class="col-md-8 col-sm-6 col-12">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fa fa-chart-line" style="margin-right: 10px; font-weight: bold; color: green;"></i> Application receiced for the last 12 months
                    </h3>
                    <div class="card-tools">
                        <!-- Buttons, labels, and many other things can be placed here! -->
                        <!-- Here is a label for example -->
                        <span class="badge badge-primary">-</span>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">

                    <?= \dosamigos\highcharts\HighCharts::widget([
                        'clientOptions' => [
                            'chart' => [
                                'plotBackgroundColor' => null,
                                'plotBorderWidth' => null,
                                'plotShadow' => false,
                                'type' => 'line'
                            ],
                            'credits' => [
                                'enabled' => false
                            ],
                            'exporting' => [
                                'enabled' => false
                            ],
                            'title' => [
                                'text' => ' '
                            ],
                            'xAxis' => [
                                'categories' => ((isset( $esrfdata->lastyrapp->month ) && is_array( $esrfdata->lastyrapp->month ))? $esrfdata->lastyrapp->month : [] )
                            ],
                            'yAxis' => [
                                'title' => [
                                    'text' => ' '
                                ]
                            ],
                            'legend' => [
                                'enabled' => false
                            ],
                            'series' => [
                                [ 'name' => ' ', 'color' => '#28a745', 'data' => ((isset( $esrfdata->lastyrapp->total ) && is_array( $esrfdata->lastyrapp->total ))? $esrfdata->lastyrapp->total : [] ) ],
                            ]
                        ]
                    ]); ?>

                </div>

            </div>
            <!-- /.card -->

        </div>
        <div class="col-lg-4">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fa fa-chart-pie" style="margin-right: 10px; font-weight: bold; color: green;"></i> Gender Baesd
                    </h3>
                    <div class="card-tools">
                        <!-- Buttons, labels, and many other things can be placed here! -->
                        <!-- Here is a label for example -->
                        <span class="badge badge-primary">-</span>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-8">

                            <?php

                            echo  \dosamigos\highcharts\HighCharts::widget([
                                'clientOptions' => [
                                    'chart' => [
                                        'plotBackgroundColor' => null,
                                        'plotBorderWidth' => null,
                                        'plotShadow' => false,
                                        'type' => 'pie'
                                    ],
                                    'credits' => [
                                        'enabled' => false
                                    ],
                                    'exporting' => [
                                        'enabled' => false
                                    ],
                                    'title' => [
                                        'text' => ' '
                                    ],
                                    'tooltip' => [
                                        'pointFormat' => '{series.name}: <b>{point.percentage:.1f}%</b>'
                                    ],
                                    'accessibility' => [
                                        'point' => [
                                            'valueSuffix' => '%'
                                        ]
                                    ],
                                    'plotOptions' => [
                                        'pie' => [
                                            'allowPointSelect' => true,
                                            'cursor' => 'pointer',
                                            'dataLabels' => [
                                                'enabled' => false
                                            ],
                                            'showInLegend' => true
                                        ]
                                    ],
                                    'legend' => [
                                        'enabled' => false
                                    ],
                                    'series' => [[
                                        'name' => 'Brands',
                                        'colorByPoint' => true,
                                        'data' => [
                                            [
                                                'color' => '#fd7e14',
                                                'name' => 'Female',
                                                'y' => floatval( ((isset( $esrfdata->pergender->female->usercounter ))? $esrfdata->pergender->female->usercounter : 0.00 ) ),
                                                'sliced' => true,
                                                'selected' => true
                                            ],
                                            [
                                                'color' => '#06b6f9',
                                                'name' => 'Male',
                                                'y' => floatval( ((isset( $esrfdata->pergender->male->usercounter ))? $esrfdata->pergender->male->usercounter : 0.00 ) ),
                                            ]
                                        ]
                                    ]]
                                ]
                            ]);

                            ?>

                        </div>
                        <div class="col-md-4" style="padding-top: 3.6rem;">

                            <h6>Applicant Based on Gender</h6>
                            <div class="row" style="margin-top: 1.0rem; margin-bottom: 10px;">
                                <div class="col-4" style="background-color: #fd7e14!important;">
                                </div>
                                <div class="col-8">
                                    Female
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 1.6rem;">
                                <div class="col-4" style="background-color: #06b6f9!important;">
                                </div>
                                <div class="col-8">
                                    Male
                                </div>
                            </div>
                            <p>Femal : <?= ((isset( $esrfdata->pergender->female->usercounter ))? $esrfdata->pergender->female->usercounter : 0.00 ) ?></p>
                            <p>Male : <?= ((isset( $esrfdata->pergender->male->usercounter ))? $esrfdata->pergender->male->usercounter : 0.00 ) ?></p>

                        </div>
                    </div>

                </div>

            </div>
            <!-- /.card -->

        </div>

    </div>




    <div class="row">

        <div class="col-md-8 col-sm-6 col-12">

            <div class="card">
                <div class="card-header" style="border-bottom: none;">
                    <h3 class="card-title">
                        <span class="far fa-file" style="margin-right: 10px; font-weight: bold; color: green;"></span> Stat
                    </h3>
                    <div class="card-tools">
                        <!-- Buttons, labels, and many other things can be placed here! -->
                        <!-- Here is a label for example -->
                        <span class="badge badge-primary">-</span>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">




                    <table class="table table-sm">
                        <tbody>
                        <tr>
                            <td><i class="fas fa-check-circle"></i></td>
                            <td>Total Approved Application</td>
                            <td><span class="badge badge-success pr-3 pl-3"><?= ((isset( $esrfdata->applicationstatistic->approved ))? $esrfdata->applicationstatistic->approved : 0 ); ?></span></td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-minus-circle"></i></td>
                            <td>Total Pending Application</td>
                            <td><span class="badge badge-warning pr-3 pl-3"><?= ((isset( $esrfdata->applicationstatistic->submitted ))? $esrfdata->applicationstatistic->submitted : 0 ); ?></span></td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-times-circle"></i></td>
                            <td>Total Rejected Application</td>
                            <td><span class="badge bg-danger pr-3 pl-3"><?= ((isset( $esrfdata->applicationstatistic->rejected ))? $esrfdata->applicationstatistic->rejected : 0 ); ?></span></td>
                        </tr>
                        </tbody>
                    </table>





                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                </div>
                <!-- /.card-footer -->
            </div>
            <!-- /.card -->

        </div>
        <div class="col-md-4 col-sm-6 col-12">

            <div class="card">
                <div class="card-header" style="border-bottom: none;">
                    <h3 class="card-title">
                        <span class="far fa-users" style="margin-right: 10px; font-weight: bold; color: green;"></span> Applicant By Region
                    </h3>
                    <div class="card-tools">
                        <!-- Buttons, labels, and many other things can be placed here! -->
                        <!-- Here is a label for example -->
                        <span class="badge badge-primary">-</span>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">



                    <?php
                    if (isset($esrfdata->applicantCounterByRegion) && is_array($esrfdata->applicantCounterByRegion)) {
                        ?>


                        <table class="table table-sm">
                            <thead>
                            <tr>
                                <th>S/No</th>
                                <th>Region</th>
                                <th>User</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $sno = 0;
                            foreach ( $esrfdata->applicantCounterByRegion as $user) {
                                ++ $sno;
                                ?>
                                <tr>
                                    <td><?= $sno; ?></td>
                                    <td><?= ((isset( $user->_name ))? $user->_name : null ); ?></td>
                                    <td><?= ((isset( $user->usercounter ))? $user->usercounter : null ); ?></td>
                                </tr>
                                <?php
                            }
                            ?>

                            </tbody>
                        </table>


                        <?php
                    }
                    ?>


                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                </div>
                <!-- /.card-footer -->
            </div>
            <!-- /.card -->

        </div>

    </div>

</div>




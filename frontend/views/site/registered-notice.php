<?php

/* @var $this yii\web\View */

$this->title = 'Welcome Message';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your ESRF Account now you have to open your email account and click the link to activate your ESRF Account.</p>

        <p>
            <a class="btn btn-success" href="<?= Yii::$app->urlManager->createAbsoluteUrl([ '/site/login' ]); ?>">Login now</a>
            <a class="btn btn-warning" href="<?= Yii::$app->urlManager->createAbsoluteUrl([ '/site/resend-email', 'id' => ((isset( $model->id ))? $model->id : null ) ]); ?>">Resend E-mail</a>
        </p>
    </div>

</div>

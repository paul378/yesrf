<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Password Reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset">
    <h5 class="text-center mt-4 mb-5"><?= Html::encode($this->title) ?></h5>

    <p>Please fill out your email. A link to reset password will be sent there.</p>

    <div class="row">
        <div class="col-lg-12">
            <?php $form = ActiveForm::begin([ 'id' => 'request-password-reset-form' ]); ?>

                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Reset Password', [ 'class' => 'btn btn-primary pr-4 pl-4' ]) ?>
                </div>

                <p class="mb-0" class="text-center" style="text-align: center; padding: 0.5rem;" >
                    I remember password? <?= Html::a( 'Sign In', [ '/site/login' ], [ 'class' => '' ]); ?>
                </p>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>


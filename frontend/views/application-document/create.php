<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplicationDocument */

$this->title = 'Create Application Document';
$this->params['breadcrumbs'][] = ['label' => 'Application Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-document-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'List of Applications';
$this->params['breadcrumbs'][] = 'Application from different calls';
?>

<div class="application-index mr-2 ml-2">

    <div class="row">
        <div class="col-md-12">

            <?php echo $this->render('_staff-search_', [ 'model' => $searchModel, 'forstaff' => 100 ]); ?>

            <div class="table-responsive" style="margin-top: 3rem;">

                <?= GridView::widget([
                    'tableOptions' => [
                        'class' => 'table table-striped',
                    ],
                    'options' => [
                        'class' => 'table-responsive',
                    ],
                    'dataProvider' => $dataProvider,
                    'columns' => [

                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'created_at',
                            'label' => 'Date',
                            'format' => 'html',
                            'value' => function($model) {
                                return ((isset( $model->created_at ))? Yii::$app->formatter->asDate( $model->created_at, 'd-MM-Y' ) : null ) ;
                            }
                        ],
                        [
                            'attribute' => 'title',
                            'label' => 'Proposal Title',
                            'format' => 'html',
                            'value' => function($model) {
                                return Html::a( ((isset( $model->title ))? $model->title : null ), [ '/application/staff-view', 'id' => $model->id ], [ 'class' => '' ]);
                            }
                        ],
                        [
                            'attribute' => 'title',
                            'label' => 'Proposal Call',
                            'format' => 'html',
                            'value' => function($model) {
                                return Html::a( ((isset( $model->proposal->title ))? $model->proposal->title : null ), [ '/proposal-call/view', 'id' => ((isset( $model->proposal->id ))? $model->proposal->id : null ) ], [ 'class' => '' ]);
                            }
                        ],
                        [
                            'attribute' => '_status',
                            'label' => 'Status',
                            'format' => 'html',
                            'value' => function($model) {
                                return ((isset( $model->status->_name ))? '<span class="badge badge-' . $model->status->tag_color . ' mr-2">' . $model->status->_name . '</span>' : null ) ;
                            }
                        ],

                        // [
                        //     'class' => 'yii\grid\ActionColumn',
                        //     'visibleButtons' => [
                        //         'view' => function ($model, $key, $index) {
                        //             if (\Yii::$app->user->can('view-proposal-call')) 
                        //             {
                        //                 return true;
                        //             } else {
                        //                 return false;
                        //             }
                        //          },
                        //         'update' => function ($model, $key, $index) {
                        //             if (\Yii::$app->user->can('update-proposal-call')) 
                        //             {
                        //                 return (( $model->_status == 10 )? true : false );
                        //             } else {
                        //                 return false;
                        //             }
                        //         },
                        //         'delete' => function ($model, $key, $index) {
                        //             if (\Yii::$app->user->can('delete-proposal-call')) 
                        //             {
                        //                 return (( $model->_status == 10 )? true : false );
                        //             } else {
                        //                 return false;
                        //             }
                        //         }
                        //     ]
                        // ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'contentOptions' => [
                                'style' => 'width: 160px'
                            ],
                            'visible' => ((Yii::$app->user->isGuest)? false : true ),
                            'buttons' =>[

                                'view' => function ($url, $model) {
                                    return Html::a('<i class="fas fa-eye"></i>', [ '/application/staff-view', 'id' => $model->id ], [ 'class' => '' ]);
                                },
                                'update' => function ($url, $model) {
                                    return '';
                                },
                                'delete' => function ($url, $model) {
                                    return '';
                                },

                            ],
                        ],

                    ],
                ]); ?>

            </div>

        </div>

    </div>


</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Application */

$this->title = 'Project Information';
$this->params['breadcrumbs'][] = [ 'label' => 'Proposal Call', 'url' => [ 'proposal-call/view', 'id' => $model->proposal_id ] ];
$this->params['breadcrumbs'][] = [ 'label' => 'My Applications', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $model->title;

\yii\web\YiiAsset::register($this);
?>

<div class="application-view mr-2 ml-2">

    <p>

        <?= (\Yii::$app->user->can('submit-application-proposal-call'))? (isset( $model->_status ) && ( $model->_status == 10 ) && ( $model->user_id == ((isset( Yii::$app->user->identity->id ))? Yii::$app->user->identity->id : null ) ))? Html::a('Submit', [ 'publish', 'id' => $model->id ], [ 'class' => 'btn btn-primary' ]) : '' : '' ; ?>
        <?= (\Yii::$app->user->can('submit-application-proposal-call'))? (isset( $model->_status ) && ( $model->_status == 10 ) && ( $model->user_id == ((isset( Yii::$app->user->identity->id ))? Yii::$app->user->identity->id : null ) ))? Html::a('Edit', [ 'update', 'id' => $model->id ], [ 'class' => 'btn btn-primary' ]) : '' : '' ; ?>
        <?= (\Yii::$app->user->can('submit-application-proposal-call'))? (isset( $model->_status ) && ( $model->_status == 10 ) && ( $model->user_id == ((isset( Yii::$app->user->identity->id ))? Yii::$app->user->identity->id : null ) ))? Html::a('Delete', [ 'delete', 'id' => $model->id ], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to reject this application?',
                'method' => 'post',
            ],
        ]) : '' : '' ; ?>

    </p>

    <h5 class="mt-5 font-weight-bold">Call Information</h5>
    <div class="row">
        <div class="col-md-12">

            <div class="callout callout-danger" style="margin-bottom: 1.6rem; padding-bottom: 1.6rem;">
                <h5 style="">
                    <?= ((isset( $model->proposal->title ))? $model->proposal->title : 'call for proposal'); ?>
                </h5>
                <p style="">
                    <?= ((isset( $model->proposal->description ))? $model->proposal->description : '' ); ?>
                </p>
                <p class="badge badge-primary mr-2">
                    Deadline : <?= ((isset( $model->proposal->closed_date ))? $model->proposal->closed_date : '' ); ?>
                </p> <?= ((isset( $model->proposal->docs_path ))? Html::a('<i class="fas fa-cloud-download-alt"></i>', [ $model->proposal->docs_path ], [ 'class' => 'mt-1', 'target' => '_blank' ]) : null ); ?>
            </div>

        </div>
    </div>

    <h5 class="mt-4 font-weight-bold">Applicant Information</h5>
    <div class="row" style="margin-bottom: 3rem;">
        <div class="col-md-6">

            <?= DetailView::widget([
                'options' => [
                    'class' => 'table table-striped',
                ],
                'model' => $model,
                'attributes' => [

                    [
                        'attribute' => 'amount',
                        'label' => 'Fullname',
                        'format' => 'html',
                        'value' => function($model) {
                            return ((isset( $model->user->first_name, $model->user->last_name ))? $model->user->first_name .' '. $model->user->last_name : null ) ;
                        }
                    ],
                    [
                        'attribute' => 'fee',
                        'label' => 'Gender',
                        'value' => function($model) {
                            return ((isset( $model->user->sex ))? (( $model->user->sex == 'M')? 'Male' : 'Female' ) : null ) ;
                        }
                    ],
                    [
                        'attribute' => 'fee',
                        'label' => 'Age',
                        'value' => function($model) {
                            return ((isset( $model->user->dob ))? (date('Y') - date('Y',strtotime( $model->user->dob ))) . ' years' : null ) ;
                        }
                    ],

                    [
                        'attribute' => 'email',
                        'label' => 'E-mail',
                        'value' => function($model) {
                            return ((isset( $model->user->email ))? $model->user->email : null ) ;
                        }
                    ],
                    [
                        'attribute' => 'mobile',
                        'label' => 'Mobile',
                        'format' => 'html',
                        'value' => function($model) {
                            return ((isset( $model->user->mobile ))? $model->user->mobile : null ) ;
                        }
                    ],

                ],
            ]) ?>


        </div>
    </div>

    <h5 class="mt-5 mb-4 font-weight-bold">Application Information</h5>
    <div class="row" style="margin-bottom: 3rem;">
        <div class="col-md-7">


            <div class="callout callout-info" style="margin-bottom: 1.6rem; padding-bottom: 1.6rem;">
                <h5 style="">
                    <?= ((isset( $model->title ))? $model->title : 'call for proposal'); ?>
                </h5>
                <p style="">
                    <?= ((isset( $model->description ))? $model->description : '' ); ?>
                </p>
                <hr />
                <p class="mr-2">
                    Application Status : <?= ((isset( $model->status->_name ))? '<span class="badge badge-' . $model->status->tag_color . ' mr-2">' . $model->status->_name . '</span>' : null ); ?>
                </p>
            </div>

            <div class="card mt-3">

                <div class="card-body p-0">
                    <table class="table table-sm">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>File name</th>
                                <th style="width: 40px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if ((isset( $model->applicationDocuments )) && (is_array( $model->applicationDocuments ))) {

                                    $counter = 0;

                                    foreach ( $model->applicationDocuments as $doc ) {
                                        ++ $counter ;

                                        //  if (isset( $doc->technicalfinacial ) && ( $doc->technicalfinacial == 'ANY')) {

                                        ?>
                                        <tr>
                                            <td><?= Yii::$app->formatter->asDecimal( $counter, 0 ); ?>.</td>
                                            <td><?= ((isset( $doc->filename ))? ((isset( $doc->technicalfinacial ) && ( $doc->technicalfinacial == 'TECH'))? '<strong>Project Proposal</strong>' : $doc->filename ) : 'Supporting Document' ) ?></td>
                                            <td style="">
                                                <?= Html::a( '<span class="badge bg-success"><i class="fas fa-download"></i></span>', [ ((isset( $doc->filepath ))? $doc->filepath : '#' ) ], [ 'class' => '', 'target' => '_blank' ]) ?>
                                            </td>
                                        </tr>
                                        <?php
                                        //  }
                                    }
                                }
                            ?>

                        </tbody>
                    </table>
                </div>

            </div>
            <!-- /.card -->

        </div>

        <div class="col-md-5">


                <div class="row">
                    <div class="col-12">

                        <div class="card card-outline card-warning">
                            <div class="card-header">
                                <h3 class="card-title">
                                    Project's Reports
                                </h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                                <!-- /.card-tools -->
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-8">

                                        <div class="form-group" class="mt-4">
                                            <label for="exampleInputEmail1">Enter file name</label>
                                            <input type="email" class="form-control" id="file-name-input" placeholder="Enter file name" >
                                        </div>
                                        <div style="">
                                            <a id="any-btn-upload" class="btn btn-success pr-3 pl-3" href="Javascript: void(0);">Upload file</a>
                                        </div>

                                    </div>
                                    <div class="col-md-4 pt-2">

                                        <div id="container-any">

                                            <a id="pickfiles-any" class="btn btn-warning btn-block mt-4">
                                                <i class="fas fa-cloud-upload-alt mr-2"></i> Select File <span id="logs_counter-any" class="badge badge-success ml-2"></span>
                                            </a>

                                        </div>
                                        <div id="filelist-any">
                                            Your browser doesn't have Flash, Silverlight or HTML5 support.
                                        </div>

                                    </div>
                                </div>

                                <p class="title mt-4">
                                    Available report for this call for proposal
                                </p>

                                <div class="card-body p-0">

                                    <?php
                                    if (isset( $reportProvider )) {
                                        ?>
                                        <?= \yii\grid\GridView::widget([
                                            'tableOptions' => [
                                                'class' => 'table table-striped table-sm',
                                            ],
                                            'options' => [
                                                'class' => 'table-responsive',
                                            ],
                                            'dataProvider' => $reportProvider,
                                            'summary' => false,
                                            'columns' => [

                                                [
                                                    'attribute' => 'filename',
                                                    'format' => 'html',
                                                    'value' => function($model) {
                                                        return ((isset( $model->filename ))? $model->filename : null );
                                                    }
                                                ],

                                                [
                                                    'class' => 'yii\grid\ActionColumn',
                                                    'contentOptions' => [
                                                        'style' => 'width: 160px'
                                                    ],
                                                    'visible' => ((Yii::$app->user->isGuest)? false : true ),
                                                    'buttons' =>[

                                                        'view' => function ($url, $model) {
                                                            return Html::a( '<span class="badge bg-success"><i class="fas fa-download"></i></span>', [ ((isset( $model->filepath ))? $model->filepath : '#' ) ], [ 'class' => '', 'target' => '_blank' ]);
                                                        },
                                                        'update' => function ($url, $model) {
                                                            return '';
                                                        },
                                                        'delete' => function ($url, $model) {
                                                            return '';
                                                        },

                                                    ],
                                                ],

                                            ],

                                        ]); ?>
                                        <?php
                                    }
                                    ?>


                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                    </div>
                </div>



        </div>
    </div>

</div>
<?php

// here is the url to the file update
$docposturl = Yii::$app->request->baseUrl.'/uploader/file-update' ;

// here is the id of the object or application
$applicationId = (isset($model->id))? $model->id : null ;

// import all js necessary
$this->registerJsFile(Yii::$app->request->baseUrl.'/plupload/js/plupload.full.min.js', [ 'depends' => [ \yii\web\JqueryAsset::className() ] ]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/yvideoJs.js', [ 'depends' => [ \yii\web\JqueryAsset::className() ] ]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/bootstrap-datepicker.min.js', [ 'depends' => [ \yii\web\JqueryAsset::className() ] ]);

// register js code
$this->registerJs( <<< EOT_JS_CODE

/**
	HERE we are going to help searching poing
    document.getElementById('uploading-any-docs').addEventListener('click', function() 
 */
window.addEventListener("load", function ()
{

	let anydoc = PaulUploadButton({
		pickfiles: 'pickfiles-any',
		container: 'container-any',
		url: '/yesrf/uploader/application-document',
		chunk_size: '2mb',
		filters: {
            max_file_size : '2mb',
            mime_types: [
                {title : "Document files", extensions : "pdf,doc,docx,xls,xlsx,zip"},
            ]
        },
		filelist: 'filelist-any',
		progress_counter: 'logs_counter-any',
        multipart_params: {
            "ApplicationDocument[technicalfinacial]": "REPO",
            "ApplicationDocument[application_id]": $applicationId,
        },
		callback: function toa_matokeo(output) {

            // document.getElementById('any-btn-upload').setAttribute( 'href', output.info.href.toString() );

            fetch('$docposturl', {
                method: 'post',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    filename: document.getElementById('file-name-input').value,
                    id: output.info.application.id.toString()
                })
            })
            .then(res => res.json())
            .then(res => {

                // here we can refrash the page after update
                // the document information
                window.location.reload();

            });

        
        },
	});

}, false);


EOT_JS_CODE
);
?>

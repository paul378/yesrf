<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Application */
/* @var $form yii\widgets\ActiveForm */
?>

<?php

if ((isset( $anydoc )) && (is_array($anydoc))) {
    $counter = 0;
    foreach ($anydoc as $doc) {
        ++ $counter ;
        ?>
        <tr>
            <td><?= Yii::$app->formatter->asDecimal( $counter, 0 ); ?>.</td>
            <td><?= ((isset( $doc->filename ))? $doc->filename : 'Supporting Document' ) ?></td>
            <td style="">
                <?= Html::a( '<span class="badge bg-success"><i class="fas fa-download"></i></span>', [ ((isset( $doc->filepath ))? $doc->filepath : '#' ) ], [ 'class' => '', 'target' => '_blank' ]) ?>
                <?= Html::a( '<span class="badge bg-danger"><i class="fas fa-trash-alt"></i></span>', [ '/application/delete-files', 'id' => ((isset( $doc->id ))? $doc->id : null ), 'ap' => ((isset( $model->id ))? $model->id : null ) ], [
                    'class' => '',
                    'data'  => [
                        'confirm' => 'Are you sure you want to delete this '. ((isset( $doc->filename ))? $doc->filename : 'Supporting Document' ) .' file ?',
                        'method' => 'post',
                    ],
                ]) ?>

            </td>
        </tr>
        <?php
    }
}
?>



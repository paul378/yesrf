<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Application */

$this->title = 'Application form';
$this->params['breadcrumbs'][] = [ 'label' => 'Applications', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = ((isset( $model->proposal->title ))? $model->proposal->title : 'call for proposal');
?>
<div class="application-update mr-2 ml-2">

    <?= $this->render('_forms__', [
        'model' => $model,
        'techdoc' => $techdoc,
        'finadoc' => $finadoc,
        'anydoc' => $anydoc,
    ]) ?>

</div>

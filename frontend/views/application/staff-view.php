<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Application */

$this->title = 'Project Information';
$this->params['breadcrumbs'][] = [ 'label' => 'Proposal Call', 'url' => [ 'proposal-call/view', 'id' => $model->proposal_id ] ];
$this->params['breadcrumbs'][] = [ 'label' => 'My Applications', 'url' => [ 'staff-index' ] ];
$this->params['breadcrumbs'][] = $model->title;

\yii\web\YiiAsset::register($this);
?>

<div class="application-view mr-2 ml-2">

    <p>

        <?= (\Yii::$app->user->can('review-application-proposal-call'))? (isset( $model->_status ) && ( $model->_status == 30 ))? Html::a('Review', [ 'review-application', 'id' => $model->id ], [ 'class' => 'btn btn-warning' ]) : '' : '' ; ?>

        <?php
            if (\Yii::$app->user->can('reject-application-proposal-call')) {
                if (isset($model->_status) && ($model->_status == 30) || ($model->_status == 60)) {
                    \yii\bootstrap4\Modal::begin([
                        'title' => 'Proposal Rejection Form',
                        'size' => 'modal-lg',
                        'clientOptions' => [
                            'backdrop' => 'static',
                            'keyboard' => false
                        ],
                        'toggleButton' => [
                            'label' => 'Reject',
                            'class' => 'btn btn-danger'
                        ],
                    ]);

                    $reject = new \common\models\RejectComment();
                    $reject->load([
                        'RejectComment' => [
                            'comment' => null,
                            'proposal_id' => ((isset($model->id)) ? $model->id : null),
                            'title' => ((isset($model->title)) ? $model->title : null),
                        ]
                    ]);

                    echo $this->render('//application/_reject-form', [
                        'model' => $reject,
                    ]);

                    \yii\bootstrap4\Modal::end();
                }
            }
        ?>


        <?= (\Yii::$app->user->can('approve-application-proposal-call'))? (isset( $model->_status ) && ( $model->_status == 30 ) || ( $model->_status == 60 ))? Html::a('Approve', [ 'approve-application', 'id' => $model->id ], [ 'class' => 'btn btn-primary' ]) : '' : '' ; ?>

    </p>

    <h5 class="mt-5 font-weight-bold">Call Information</h5>
    <div class="row">
        <div class="col-md-12">

            <div class="callout callout-danger" style="margin-bottom: 1.6rem; padding-bottom: 1.6rem;">
                <h5 style="">
                    <?= ((isset( $model->proposal->title ))? $model->proposal->title : 'call for proposal'); ?>
                </h5>
                <p style="">
                    <?= ((isset( $model->proposal->description ))? $model->proposal->description : '' ); ?>
                </p>
                <p class="badge badge-primary mr-2">
                    Deadline : <?= ((isset( $model->proposal->closed_date ))? $model->proposal->closed_date : '' ); ?>
                </p> <?= ((isset( $model->proposal->docs_path ))? Html::a('<i class="fas fa-cloud-download-alt"></i>', [ $model->proposal->docs_path ], [ 'class' => 'mt-1', 'target' => '_blank' ]) : null ); ?>
            </div>

        </div>
    </div>

    <h5 class="mt-4 font-weight-bold">Applicant Information</h5>
    <div class="row" style="margin-bottom: 3rem;">
        <div class="col-md-6">

            <?= DetailView::widget([
                'options' => [
                    'class' => 'table table-striped',
                ],
                'model' => $model,
                'attributes' => [

                    [
                        'attribute' => 'amount',
                        'label' => 'Fullname',
                        'format' => 'html',
                        'value' => function($model) {
                            return ((isset( $model->user->first_name, $model->user->last_name ))? $model->user->first_name .' '. $model->user->last_name : null ) ;
                        }
                    ],
                    [
                        'attribute' => 'fee',
                        'label' => 'Gender',
                        'value' => function($model) {
                            return ((isset( $model->user->sex ))? (( $model->user->sex == 'M')? 'Male' : 'Female' ) : null ) ;
                        }
                    ],
                    [
                        'attribute' => 'fee',
                        'label' => 'Age',
                        'value' => function($model) {
                            return ((isset( $model->user->dob ))? (date('Y') - date('Y',strtotime( $model->user->dob ))) . ' years' : null ) ;
                        }
                    ],

                    [
                        'attribute' => 'email',
                        'label' => 'E-mail',
                        'value' => function($model) {
                            return ((isset( $model->user->email ))? $model->user->email : null ) ;
                        }
                    ],
                    [
                        'attribute' => 'mobile',
                        'label' => 'Mobile',
                        'format' => 'html',
                        'value' => function($model) {
                            return ((isset( $model->user->mobile ))? $model->user->mobile : null ) ;
                        }
                    ],

                ],
            ]) ?>


        </div>
    </div>

    <h5 class="mt-5 mb-4 font-weight-bold">Application Information</h5>
    <div class="row" style="margin-bottom: 3rem;">
        <div class="col-md-6">


            <div class="callout callout-info" style="margin-bottom: 1.6rem; padding-bottom: 1.6rem;">
                <h5 style="">
                    <?= ((isset( $model->title ))? $model->title : 'call for proposal'); ?>
                </h5>
                <p style="">
                    <?= ((isset( $model->description ))? $model->description : '' ); ?>
                </p>
                <hr />
                <p class="mr-2">
                    Application Status : <?= ((isset( $model->status->_name ))? '<span class="badge badge-' . $model->status->tag_color . ' mr-2">' . $model->status->_name . '</span>' : null ); ?>
                </p>
            </div>

            <div class="card mt-3">

                <div class="card-body p-0">
                    <table class="table table-sm">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>File name</th>
                                <th style="width: 40px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if ((isset( $model->applicationDocuments )) && (is_array( $model->applicationDocuments ))) {

                                    $counter = 0;

                                    foreach ( $model->applicationDocuments as $doc ) {
                                        ++ $counter ;

                                        ?>
                                        <tr>
                                            <td><?= Yii::$app->formatter->asDecimal( $counter, 0 ); ?>.</td>
                                            <td><?= ((isset( $doc->filename ))? ((isset( $doc->technicalfinacial ) && ( $doc->technicalfinacial == 'TECH'))? '<strong>Project Proposal</strong>' : $doc->filename ) : 'Supporting Document' ) ?></td>
                                            <td style="">
                                                <?= Html::a( '<span class="badge bg-success"><i class="fas fa-download"></i></span>', [ ((isset( $doc->filepath ))? $doc->filepath : '#' ) ], [ 'class' => '', 'target' => '_blank' ]) ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                            ?>

                        </tbody>
                    </table>
                </div>

            </div>
            <!-- /.card -->

        </div>
    </div>

</div>





<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Application */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'action' => [ 'reject-with-comment' ],
    'method' => 'post',
]); ?>

<div class="row">
    <div class="col-md-12">
        <p class="summary">
            You are about to reject application for <?= ((isset( $model->title ))? $model->title : null ); ?>
        </p>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'comment')->textarea([ 'rows' => '6', 'placeholder' => "Any Additional Comment"])->label('Additional Comment') ?>
    </div>
</div>


<div class="row">
    <div class="col-md-12">

        <div class="form-group">

            <?= $form->field($model, 'proposal_id')->hiddenInput([ 'id'=> 'proposal-submit-reject' ])->label(false); ?>
            <?= Html::submitButton('Reject Application', [
                'class' => 'btn btn-danger pr-3 pl-3',
                'id' => 'save-now'
            ]) ?>

        </div>

    </div>
</div>

<?php ActiveForm::end(); ?>
<?php

// register js code
$this->registerJs( <<< EOT_JS_CODE
EOT_JS_CODE
);
?>

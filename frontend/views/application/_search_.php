<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplicationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="application-search">

    <?php $form = ActiveForm::begin([
        'action' => [ ((isset( $forstaff ) && ( $forstaff == 100 ))? 'staff-index' : 'index' ) ],
        'method' => 'get',
    ]); ?>




    <div class="row">

        <div class="col-md-8">

        </div>
        <div class="col-md-4">

            <div class="row">

                <div class="col-md-6">
                    <div class="form-group" class="mt-4">
                        <?= $form->field($model, 'title')->textInput(['placeholder' => "Search By Title"])->label(false) ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" class="mt-4">
                        <?= $form->field($model, 'description')->textInput(['placeholder' => "Search By Description"])->label(false) ?>
                    </div>
                </div>

            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="form-group">
                        <?= Html::submitButton('Search Application', ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Apply for a call', ['/proposal-call/index'], ['class' => 'btn btn-success']) ?>
                    </div>
                </div>

            </div>

        </div>

    </div>






    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Application */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-md-12">

    <div class="callout callout-danger" style="margin-bottom: 1.6rem; padding-bottom: 1.6rem;">
        <h5 style="">
            <?= ((isset( $model->proposal->title ))? $model->proposal->title : 'call for proposal'); ?>
        </h5>
        <p style="">
            <?= ((isset( $model->proposal->description ))? $model->proposal->description : '' ); ?>
        </p>
        <p class="badge badge-primary mr-2">
            Deadline : <?= ((isset( $model->proposal->closed_date ))? $model->proposal->closed_date : '' ); ?>
        </p> <?= ((isset( $model->proposal->docs_path ))? Html::a('<i class="fas fa-cloud-download-alt"></i>', [ $model->proposal->docs_path ], [ 'class' => 'mt-1', 'target' => '_blank' ]) : null ); ?>
    </div>

    </div>
</div>

<?php $form = ActiveForm::begin(); ?>


    <div class="row">
        <div class="col-md-6">

            <div class="row">
                <div class="col-md-6">

                    <p style="">
                        Here you can select and upload technical proposal
                    </p>
                    <div class="form-group">

                        <div id="filelist-technical" class="mt-3">
                            Your browser doesn't have Flash, Silverlight or HTML5 support.
                        </div>
                        <div id="container-technical">

                            <a id="pickfiles-technical" class="btn btn-warning btn-xs">
                                <i class="fas fa-cloud-upload-alt mr-2"></i>Upload Technical Proposal <span id="logs_counter-technical" class="badge badge-success ml-2"></span>
                            </a>
                            <div style="">
                                <?= ((isset( $techdoc->filepath ))? Html::a('View file', [ $techdoc->filepath ], [ 'class' => 'btn btn-success btn-xs pr-3 pl-3', 'id' => 'technical-href', 'target' => '_blank' ]) : null ); ?>
                            </div>

                        </div>
                        
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="form-group">

                        <p style="">
                            Here you can select and upload finacial proposal
                        </p>
                        <div id="filelist-finacial" class="mt-3">
                            Your browser doesn't have Flash, Silverlight or HTML5 support.
                        </div>
                        <div id="container-finacial">

                            <a id="pickfiles-finacial" class="btn btn-warning btn-xs">
                                <i class="fas fa-cloud-upload-alt mr-2"></i>Upload finacial Proposal <span id="logs_counter-finacial" class="badge badge-success ml-2"></span>
                            </a>
                            <div style="">
                                <?= ((isset( $finadoc->filepath ))? Html::a('View file', [ $finadoc->filepath ], [ 'class' => 'btn btn-success btn-xs pr-3 pl-3', 'id' => 'technical-href', 'target' => '_blank' ]) : null ); ?>
                            </div>

                        </div>
                        
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-6">

            <p id="uploading-any-docs" style="">
                Here you can upload any file but you must enter name of the file first before click "Select File" button
            </p>

            <div class="row">
                <div class="col-md-8">

                    <div class="form-group" class="mt-3">
                        <label for="exampleInputEmail1">Enter file name</label>
                        <input type="email" class="form-control" id="file-name-input" placeholder="Enter file name" >
                    </div>
                    <div style="">
                        <a id="any-btn-upload" class="btn btn-success pr-3 pl-3" href="Javascript: void(0);">Save file</a>
                    </div>

                </div>
                <div class="col-md-4 pt-2">

                    <div id="container-any">

                        <a id="pickfiles-any" class="btn btn-warning btn-block mt-4">
                            <i class="fas fa-cloud-upload-alt mr-2"></i> Select File <span id="logs_counter-any" class="badge badge-success ml-2"></span>
                        </a>

                    </div>
                    <div id="filelist-any">
                        Your browser doesn't have Flash, Silverlight or HTML5 support.
                    </div>

                </div>
            </div>




            <div class="card mt-3">

<div class="card-body p-0">
    <table class="table table-sm">
        <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th>File name</th>
                <th style="width: 40px">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if ((isset( $anydoc )) && (is_array($anydoc))) {
                    $counter = 0;
                    foreach ($anydoc as $doc) {
                        ++ $counter ;
                        ?>
                        <tr>
                            <td><?= Yii::$app->formatter->asDecimal( $counter, 0 ); ?>.</td>
                            <td><?= ((isset( $doc->filename ))? $doc->filename : 'Supporting Document' ) ?></td>
                            <td><span class="badge bg-danger">delete</span></td>
                        </tr>
                        <?php
                    }
                }
            ?>

        </tbody>
    </table>
</div>

            </div>
            <!-- /.card -->




        </div>
    </div>


    <div class="form-group" style="margin-top: 5.0rem;">

        <?= $form->field($model, 'proposal_id')->hiddenInput()->label(false); ?>
        <?= $form->field($model, 'user_id')->hiddenInput()->label(false); ?>
        <?= Html::submitButton('Save', [
            'class' => 'btn btn-success pr-5 pl-5', 'id' => 'save-now'
        ]) ?>
        <?= Html::submitButton('Save & Submit', [
            'class' => 'btn btn-info pr-5 pl-5',
            'title' => Yii::t( 'app', 'Apply for call' ),
            'id' => 'button-apply-application'
        ]) ?>

    </div>

<?php ActiveForm::end(); ?>
<?php

// here is the url to the file update
$docposturl = Yii::$app->request->baseUrl.'/uploader/file-update' ;

// here is the id of the object or application
$applicationId = (isset($model->id))? $model->id : null ;

// import all js necessary
$this->registerJsFile(Yii::$app->request->baseUrl.'/plupload/js/plupload.full.min.js', [ 'depends' => [ \yii\web\JqueryAsset::className() ] ]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/yvideoJs.js', [ 'depends' => [ \yii\web\JqueryAsset::className() ] ]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/bootstrap-datepicker.min.js', [ 'depends' => [ \yii\web\JqueryAsset::className() ] ]);

// register js code
$this->registerJs( <<< EOT_JS_CODE

// JS code here

window.addEventListener("load", function ()
{

	let technical = PaulUpload({
		pickfiles: 'pickfiles-technical',
		container: 'container-technical',
		url: '/yesrf/uploader/application-document?id=$applicationId&technicalfinacialstatus=TECH',
		chunk_size: '2mb',
		filters: {
            max_file_size : '2mb',
            mime_types: [
                {title : "Document files", extensions : "pdf,doc,docx,xls,xlsx,zip"},
            ]
        },
		filelist: 'filelist-technical',
		progress_counter: 'logs_counter-technical',
		callback: function toa_matokeo(output) {

            document.getElementById('technical-href').setAttribute( 'href', output.info.href.toString() );
        
        },
	});

});


// JS code here

window.addEventListener("load", function ()
{

	let finacial = PaulUpload({
		pickfiles: 'pickfiles-finacial',
		container: 'container-finacial',
		url: '/yesrf/uploader/application-document?id=$applicationId&technicalfinacialstatus=FINA',
		chunk_size: '2mb',
		filters: {
            max_file_size : '2mb',
            mime_types: [
                {title : "Document files", extensions : "pdf,doc,docx,xls,xlsx,zip"},
            ]
        },
		filelist: 'filelist-finacial',
		progress_counter: 'logs_counter-finacial',
		callback: function toa_matokeo(output) {

            document.getElementById('finacial-href').setAttribute( 'href', output.info.href.toString() );
        
        },
	});

});


/**
	HERE we are going to help searching poing
 */
document.getElementById('button-apply-application').addEventListener('click', function() {

    /*   Here we assume the video has been watched   */
    $("#proposal-submit-status").val("30");
	console.log(this.value);

    /* submit form */
    $("#save-now").trigger('click');

}, false);


/**
	HERE we are going to help searching poing
    document.getElementById('uploading-any-docs').addEventListener('click', function() 
 */
window.addEventListener("load", function ()
{

	let anydoc = PaulUploadButton({
		pickfiles: 'pickfiles-any',
		container: 'container-any',
		url: '/yesrf/uploader/application-document?id=$applicationId',
		chunk_size: '2mb',
		filters: {
            max_file_size : '2mb',
            mime_types: [
                {title : "Document files", extensions : "pdf,doc,docx,xls,xlsx,zip"},
            ]
        },
		filelist: 'filelist-any',
		progress_counter: 'logs_counter-any',
		callback: function toa_matokeo(output) {

            // document.getElementById('any-btn-upload').setAttribute( 'href', output.info.href.toString() );

            fetch('$docposturl', {
                method: 'post',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    filename: document.getElementById('file-name-input').value,
                    id: output.info.application.id.toString()
                })
            })
            .then(res => res.json())
            .then(res => {

                // here we can refrash the page after update
                // the document information
                // window.location.reload();
                console.log(res);

            });

        
        },
	});

}, false);


EOT_JS_CODE
);
?>

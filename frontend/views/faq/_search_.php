<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FaqSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faq-search">

    <?php $form = ActiveForm::begin([
        'action' => [ ((isset( $whois ) && ( $whois == 'applicant' ))? '' : 'index' ) ],
        'method' => 'get',
    ]); ?>




    <div class="row">

        <div class="col-md-8">

        </div>
        <div class="col-md-4">

            <div class="row">

                <div class="col-md-6">
                    <div class="form-group" class="mt-4">
                        <?= $form->field($model, 'question')->textInput(['placeholder' => "Search By Question"])->label(false) ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" class="mt-4">
                        <?= $form->field($model, 'answer')->textInput(['placeholder' => "Search By Answer"])->label(false) ?>
                    </div>
                </div>

            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="form-group">
                        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                        <?= (\Yii::$app->user->can('delete-faq'))? Html::a('Create FAQ', [ '/faq/create' ], [ 'class' => 'btn btn-success' ]) : '' ; ?>
                    </div>
                </div>

            </div>

        </div>

    </div>



    <?php ActiveForm::end(); ?>

</div>

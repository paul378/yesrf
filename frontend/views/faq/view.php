<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Faq */

$this->title = 'FAQ Detail';
$this->params['breadcrumbs'][] = [ 'label' => 'Frequent Asked Question', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $model->question;

\yii\web\YiiAsset::register($this);
?>
<div class="faq-view mr-2 ml-2 mt-5">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
        <div class="col-md-6">

            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h5 class="m-0">FAQ Detail</h5>
                </div>
                <div class="card-body">
                    <h6 class="card-title">
                        <?= ((isset( $model->question ))? $model->question : null ) ?>
                    </h6>

                    <p class="card-text">
                        <?= ((isset( $model->answer ))? $model->answer : null ) ?>
                    </p>
                    <?= Html::a('Return to FAQ', ['/faq/index', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                </div>
            </div>

        </div>
    </div>

</div>

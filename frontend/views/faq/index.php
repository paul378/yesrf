<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use sjaakp\collapse\Collapse;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FaqSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Frequent Asked Question';
$this->params['breadcrumbs'][] = 'List of frequent asked question';
?>

<div class="application-index mr-2 ml-2">

    <div class="row">
        <div class="col-md-12">

            <?php echo $this->render( 
                '//faq/_search_', 
                [ 
                    'model' => $searchModel, 
                    'whois' => ((isset( $whois ) && ( $whois == 'applicant' ))? 'applicant' : null ) 
                ]); 
            ?>

        </div>
    </div>


    <div class="row">

        <?= ListView::widget([

            'options' => [
                'tag' => 'div',
                'class' => 'col-12',
                'id' => 'accordion',
            ],

            'dataProvider' => $dataProvider,
            'itemView' => function ( $model )
            {

                $itemContent = $this->render(
                    '//faq/faqlist',
                    [
                        'model' => $model
                    ]);

                return $itemContent;

            },

            'itemOptions' => [
                'tag' => false
            ],

            'summary' => '',

        ]);
        ?>

    </div>


</div>


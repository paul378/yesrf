<?php

use yii\helpers\Html;

use common\models\Esrf;
use common\models\Common;

?>
<div class="card card-<?= ((isset( $model->id ))? (( ( (int) $model->id % 3 ) == 0 )? 'warning' : (( ( (int) $model->id % 2 ) == 0 )? 'success' : 'primary' ) ) : 'info' ); ?> card-outline">
    <a class="d-block w-100" data-toggle="collapse" href="#collapse<?= ((isset( $model->id ))? $model->id : null ) ?>s">
        <div class="card-header">
            <h4 class="card-title w-100">
                <?= ((isset( $model->question ))? $model->question : "Unknown Basic FAQ's" ) ?>
            </h4>
        </div>
    </a>
    <div id="collapse<?= ((isset( $model->id ))? $model->id : null ) ?>s" class="collapse" data-parent="#accordion">
        <div class="card-body">
            <p class="" style=""><?= ((isset( $model->answer ))? $model->answer : "Details About Basic FAQ's" ) ?></p>
            <?= (\Yii::$app->user->can('update-faq'))? Html::a('<i class="fas fa-pencil-alt"></i>', [ 'update', 'id' => $model->id ], [ 'class' => 'card-link' ]) : '' ; ?>
            <?= (\Yii::$app->user->can('delete-faq'))? Html::a('<i class="fas fa-trash-alt"></i>', [ 'delete', 'id' => $model->id ], [ 'class' => 'card-link' ]) : '' ; ?>
        </div>
    </div>
</div>

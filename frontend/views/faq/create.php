<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Faq */

$this->title = 'Create FAQ';
$this->params['breadcrumbs'][] = ['label' => 'Frequent Asked Question', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'FAQ Creation Form';
?>
<div class="faq-create mr-2 ml-2 mt-4">

    <?= $this->render('_form_', [
        'model' => $model,
    ]) ?>

</div>

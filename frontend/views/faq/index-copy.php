<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use sjaakp\collapse\Collapse;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FaqSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Frequent Asked Question';
$this->params['breadcrumbs'][] = 'List of frequent asked question';
?>

<div class="application-index mr-2 ml-2">

    <div class="row">
        <div class="col-md-12">

            <?php echo $this->render( '_search_', [ 'model' => $searchModel ]); ?>

        </div>
    </div>


    <div class="row">

        <?= ListView::widget([

            'options' => [
                'tag' => 'div',
                'class' => 'col-12',
                'id' => 'accordion',
            ],

            'dataProvider' => $dataProvider,
            'itemView' => function ( $model )
            {

                $itemContent = $this->render(
                    '//faq/faqlist',
                    [
                        'model' => $model
                    ]);

                return $itemContent;

            },

            'itemOptions' => [
                'tag' => false
            ],

            'summary' => '',

        ]);
        ?>

    </div>


    <div class="col-md-12 col-lg-6 col-xl-4">
        <div class="card mb-2 bg-gradient-dark">
            <img class="card-img-top" src="<?= \Yii::$app->request->baseUrl . '/public/images/sample.png' ?>" alt="Dist Photo 1">
            <div class="card-img-overlay d-flex flex-column justify-content-end">
                <h5 class="card-title text-primary text-white">Card Title</h5>
                <p class="card-text text-white pb-2 pt-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor.</p>
                <a href="#" class="text-white">Last update 2 mins ago</a>
            </div>
        </div>
    </div>


</div>


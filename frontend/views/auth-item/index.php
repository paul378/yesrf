<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AuthItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Auth Items';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="auth-item-form mr-2 ml-2">

    <?php echo $this->render('_search_', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'tableOptions' => [
            'class' => 'table table-striped',
        ],
        'options' => [
            'class' => 'table-responsive',
        ],
        'dataProvider' => $dataProvider,
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'name',
                'format' => 'html',
                'value' => function($model) {
                    return ((isset( $model->type ))? (( $model->type == 2 )? Html::a('<i class="fas fa-eye"></i>', [ 'auth-permission', 'id' => ((isset( $model->name ))? $model->name : null ) ], [ 'class' => 'btn btn-primary btn-xs pr-1 pl-1 mr-2 mt-0 mb-0 pt-0 pb-0' ]) . $model->name : (( $model->type == 1 )? $model->name : $model->name ) ) : $model->name );
                }
            ],

            [
                'attribute' => 'type',
                'format' => 'html',
                'value' => function($model) {
                    return ((isset( $model->type ))? (( $model->type == 2 )? 'Role' : (( $model->type == 1 )? 'Permission' : null ) ) : null );
                }
            ],

            'description:ntext',
            // 'rule_name',
            // 'data:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'view' => function ($model, $key, $index) {
                        if (\Yii::$app->user->can('view-auth-item')) 
                        {
                            return true;
                        } else {
                            return false;
                        }
                     },
                    'update' => function ($model, $key, $index) {
                        if (\Yii::$app->user->can('update-auth-item')) 
                        {
                            return true;
                        } else {
                            return false;
                        }
                    },
                    'delete' => function ($model, $key, $index) {
                        if (\Yii::$app->user->can('delete-auth-item')) 
                        {
                            return true;
                        } else {
                            return false;
                        }
                    }
                ]
            ],

            // [
            //     'class' => 'yii\grid\ActionColumn',
            //     'contentOptions' => [
            //         'style' => 'width: 160px'
            //     ],
            //     'visible' => ((Yii::$app->user->isGuest)? false : true ),
            //     'buttons' =>[

            //         'view' => function ($url, $model) {
            //             return Html::a('<i class="fas fa-eye"></i>', [ 'create' ], [ 'class' => 'btn btn-primary btn-xs' ]);
            //         },
            //         'update' => function ($url, $model) {
            //             return Html::a('<i class="fas fa-pencil-alt"></i>', [ 'create' ], [ 'class' => 'btn btn-primary btn-xs' ]);
            //         },
            //         'delete' => function ($url, $model) {
            //             return Html::a('<i class="fas fa-trash-alt"></i>', [ 'create' ], [ 'class' => 'btn btn-primary btn-xs' ]);
            //         },

            //     ],
            // ],


        ],
    ]); ?>

</div>

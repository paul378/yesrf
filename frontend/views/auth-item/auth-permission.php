<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\ProposalCall */

$this->title = ((isset( $model->name ))? $model->name : null ) . "'s Details";
$this->params['breadcrumbs'][] = [ 'label' => 'Auth Items', 'url' => [ 'index'] ];
$this->params['breadcrumbs'][] = ((isset( $model->name ))? $model->name : null );
?>
<div class="proposal-call-update mr-2 ml-2">

    <div class="row">

        <div class="col-md-6">
            <div class="card card-success">
                <div class="card-header">
                    <h3 class="card-title">
                        List of permission for <?= ((isset( $model->name ))? $model->name : null ); ?>
                    </h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="maximize">
                            <i class="fas fa-expand"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">

                    <?php
                        echo GridView::widget([
                            'tableOptions' => [
                                'class' => 'table table-striped',
                            ],
                            'options' => [
                                'class' => 'table-responsive',
                            ],
                            'dataProvider' => $child,
                            'summary' => false,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                [
                                    'attribute' => 'name',
                                    'format' => 'html',
                                    'value'  => function($model) {
                                        return $model->child;
                                    }
                                ],

                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'contentOptions' => [
                                        'style' => 'width: 160px'
                                    ],
                                    'visible' => ((Yii::$app->user->isGuest)? false : true ),
                                    'buttons' =>[

                                        'view' => function ($url, $model) {
                                            return '';
                                        },
                                        'update' => function ($url, $model) {
                                            return '';
                                        },
                                        'delete' => function ($url, $model) {
                                            return '';
                                        },

                                    ],
                                ],


                            ],
                        ]);
                    ?>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->

        <div class="col-md-6">





            <div class="row">

                <div class="col-md-12">
                    <div class="card card-success">
                        <div class="card-header">
                            <h3 class="card-title">
                                Add user to <?= ((isset( $model->name ))? $model->name : null ); ?>
                            </h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="maximize">
                                    <i class="fas fa-expand"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">


                            <div class="auth-assignment-form">

                                <?php if (isset( $auths, $authassign, $model->name )) { $form = ActiveForm::begin(); ?>


                                <div class="row">
                                    <div class="col-md-12">
                                        <?= $form->field($auths, 'user_id')->dropDownList(ArrayHelper::map($authassign, 'id', 'first_name'), [ 'prompt' => 'Select User' ])->label('Select User') ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <?= $form->field($auths, 'item_name')->hiddenInput([ 'maxlength' => true, 'value' => $model->name ])->label(false); ?>
                                        <?= Html::submitButton('Add to '. $model->name .' group', ['class' => 'btn btn-success']) ?>
                                    </div>
                                </div>

                                <?php ActiveForm::end(); } ?>

                            </div>


                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->

            </div>

            <div class="row">

                <div class="col-md-12">
                    <div class="card card-success">
                        <div class="card-header">
                            <h3 class="card-title">
                                List of users belongs to <?= ((isset( $model->name ))? $model->name : null ); ?>
                            </h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="maximize">
                                    <i class="fas fa-expand"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">

                            <?php
                                echo GridView::widget([
                                    'tableOptions' => [
                                        'class' => 'table table-striped',
                                    ],
                                    'options' => [
                                        'class' => 'table-responsive',
                                    ],
                                    'dataProvider' => $users,
                                    'summary' => false,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        [
                                            'attribute' => 'name',
                                            'label' => 'Full name',
                                            'format' => 'html',
                                            'value'  => function($model) {
                                                return ((isset( $model->user->first_name, $model->user->last_name ))? $model->user->first_name .' '. $model->user->last_name : null );
                                            }
                                        ],

                                        [
                                            'attribute' => 'name',
                                            'label' => 'E-mail',
                                            'format' => 'html',
                                            'value'  => function($model) {
                                                return ((isset( $model->user->email ))? $model->user->email : null );
                                            }
                                        ],

                                        [
                                            'attribute' => 'name',
                                            'label' => 'Mobile',
                                            'format' => 'html',
                                            'value'  => function($model) {
                                                return ((isset( $model->user->mobile ))? $model->user->mobile : null );
                                            }
                                        ],

                                        [
                                            'class' => 'yii\grid\ActionColumn',
                                            'contentOptions' => [
                                                'style' => 'width: 160px'
                                            ],
                                            'visible' => ((Yii::$app->user->isGuest)? false : true ),
                                            'buttons' =>[

                                                'view' => function ($url, $model) {
                                                    return Html::a('<i class="fas fa-eye"></i>', [ 'create' ], [ 'class' => '' ]);
                                                },
                                                'update' => function ($url, $model) {
                                                    return '';
                                                },
                                                'delete' => function ($url, $model) {
                                                    return '';
                                                },

                                            ],
                                        ],


                                    ],
                                ]);
                            ?>

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->

            </div>





        </div>
        <!-- /.col -->

    </div>

</div>





<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\AuthItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auth-item-search mr-2 ml-2">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        
        <div class="col-md-3">
            <?= $form->field($model, 'name')->textInput([ 'placeholder' => "Search By name" ])->label(false); ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'type')->dropDownList(ArrayHelper::map([ 
                [ 'id' => 1, 'name' => 'Permission' ],
                [ 'id' => 2, 'name' => 'Role' ] 
            ], 'id', 'name'), [ 'prompt' => 'Select auth item type' ])->label(false); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'description')->textInput([ 'placeholder' => "Search By Description" ])->label(false); ?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('<i class="fas fa-search mr-2"></i>Search', [ 'class' => 'btn btn-success' ]) ?>
                <?= Html::a('<i class="fas fa-plus-circle mr-2"></i>Add New', [ 'create' ], [ 'class' => 'btn btn-primary' ]) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php



?>
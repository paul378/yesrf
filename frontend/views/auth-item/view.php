<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AuthItem */

$this->title = ((isset( $model->type, $model->name ))? $model->name ."'s ". (( $model->type == 2 )? 'Role' : (( $model->type == 1 )? 'Permission' : null ) ) . " Details" : 'Auth Item' );
$this->params['breadcrumbs'][] = ['label' => 'Auth Items', 'url' => ['index']];
$this->params['breadcrumbs'][] =  $model->name;

\yii\web\YiiAsset::register($this);
?>
<div class="auth-item-view mr-2 ml-2 mt-4">

    <p>
        <?= Html::a('Add New', [ 'create' ], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Update', [ 'update', 'id' => $model->name ], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', [ 'delete', 'id' => $model->name ], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'name',
                        'type',
                        'description:ntext',
                        // 'rule_name',
                        // 'data:ntext',
                        // 'created_at',
                        // 'updated_at',
                    ],
                ]) ?>

            </div>
        </div>
    </div>

</div>

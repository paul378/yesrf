<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\AuthItem */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="auth-item-form">

    <?php $form = ActiveForm::begin(); ?>



    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <?= $form->field($model, 'type')->dropDownList(ArrayHelper::map([['id'=>'1','name'=>'Authentication Item'],['id'=>'2','name'=>'Access Role']],'id','name'),[ 'prompt' => 'Please choose type' ]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?= $form->field($model, 'rule_name')->hiddenInput()->label(false) ?>
                <?= $form->field($model, 'data')->hiddenInput()->label(false) ?>
                <?= Html::submitButton($model->isNewRecord ? 'Add Item' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php

use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\grid\GridView;

use common\models\Esrf;
use common\models\Common;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProposalCallSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proposal Calls';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proposal-call-index mr-2 ml-2">

    <div class="row">
        <div class="col-md-12">

            <?php echo $this->render('_search_', ['model' => $searchModel]); ?>

            <div class="table-responsive" style="margin-top: 3rem;">

                <?= GridView::widget([
                    'tableOptions' => [
                        'class' => 'table table-striped',
                    ],
                    'options' => [
                        'class' => 'table-responsive',
                    ],
                    'dataProvider' => $dataProvider,
                    'summary' => false,
                    'columns' => [

                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'closed_date',
                            'label' => 'Date',
                            'format' => 'html',
                            'value' => function($model) {
                                return ((isset( $model->closed_date ))? Yii::$app->formatter->asDate( $model->closed_date, 'd-MM-Y' ) : null );
                            }
                        ],

                        [
                            'attribute' => 'category_id',
                            'label' => 'Category',
                            'format' => 'html',
                            'value' => function($model) {
                                return ((isset( $model->category_id ))? '<span class="badge badge-' . (( $model->category_id == 20 )? 'success' : 'warning' ) .' pr-3 pl-3">'. ((isset( $model->category->_name ))? $model->category->_name : null ) .'</span>' : null );
                            }
                        ],

                        [
                            'attribute' => 'title',
                            'format' => 'html',
                            'value' => function($model) {
                                return Html::a( $model->title, [ 'view', 'id' => $model->id ], [ 'class' => '' ]);
                            }
                        ],

                        [
                            'attribute' => 'types_id',
                            'label' => 'Types',
                            'format' => 'html',
                            'value' => function($model) {
                                return ((isset( $model->types_id ))? '<span class="badge badge-' . (( $model->types_id == 20 )? 'success' : 'warning' ) .' pr-3 pl-3">'. (( $model->types_id == 20 )? 'Invitation' : 'Public' ) .'</span>' : null );
                            }
                        ],
                        [
                            'attribute' => '_status',
                            'label' => 'Status',
                            'format' => 'html',
                            'value' => function($model) {
                                return ((isset( $model->types_id ))? '<span class="badge badge-' . (( $model->_status == 50 )? 'default' : (( $model->_status == 40 )? 'danger' : (( $model->_status == 30 )? 'success' : (( $model->_status == 20 )? 'danger' : (( $model->_status == 10 )? 'primary' : 'info' ) ) ) ) ) .' pr-3 pl-3">'. (( $model->_status == 50 )? 'Reviewing' : (( $model->_status == 40 )? 'Rejected' : (( $model->_status == 30 )? 'Submitted' : (( $model->_status == 20 )? 'Cancelled/Blocked' : (( $model->_status == 10 )? 'Saved' : 'Unknown' ) ) ) ) ) .'</span>' : null );
                            }
                        ],

                        [
                            'attribute' => 'closed_date',
                            'label' => 'Days Remaining',
                            'format' => 'html',
                            'value' => function($model) {

                                $tarehe = Esrf::datedifferent( ((isset( $model->closed_date ))? $model->closed_date : '2000-12-31 00:00:00' ) );
                                $close = ((isset( $tarehe['close'] ))? $tarehe['close'] : 100 );
                                $days  = ((isset( $tarehe['days'] ))?  $tarehe['days']  : 0 );

                                return ((isset( $model->closed_date ))? '<span class="' . ((isset( $days, $close ) && ( $close == 200 ))? 'badge badge-success' : 'badge badge-danger' ) .' pr-3 pl-3">'. ((isset( $days, $close ) && ( $close == 200 ))? $days : 0 ) .' days</span>' : null );

                            }
                        ],

                        // [
                        //     'class' => 'yii\grid\ActionColumn',
                        //     'visibleButtons' => [
                        //         'view' => function ($model, $key, $index) {
                        //             if (\Yii::$app->user->can('view-proposal-call')) 
                        //             {
                        //                 return true;
                        //             } else {
                        //                 return false;
                        //             }
                        //          },
                        //         'update' => function ($model, $key, $index) {
                        //             if (\Yii::$app->user->can('update-proposal-call')) 
                        //             {
                        //                 return (( $model->_status == 10 )? true : false );
                        //             } else {
                        //                 return false;
                        //             }
                        //         },
                        //         'delete' => function ($model, $key, $index) {
                        //             if (\Yii::$app->user->can('delete-proposal-call')) 
                        //             {
                        //                 return (( $model->_status == 10 )? true : false );
                        //             } else {
                        //                 return false;
                        //             }
                        //         }
                        //     ]
                        // ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'contentOptions' => [
                                'style' => 'width: 160px'
                            ],
                            'visible' => ((Yii::$app->user->isGuest)? false : true ),
                            'buttons' =>[

                                'view' => function ($url, $model) {
                                    return Html::a('<i class="fas fa-eye"></i>', [ 'view', 'id' => $model->id ], [ 'class' => '' ]);
                                },
                                'update' => function ($url, $model) {

                                    $tarehe = Esrf::datedifferent( ((isset( $model->closed_date ))? $model->closed_date : '2000-12-31 00:00:00' ) );
                                    $close = ((isset( $tarehe['close'] ))? $tarehe['close'] : 100 );
                                    $days  = ((isset( $tarehe['days'] ))?  $tarehe['days']  : 0 );

                                    return ((( $model->_status == 10 ) && isset( $days, $close ) && ( $close == 200 ))? Html::a('<i class="fas fa-pencil-alt"></i>', [ 'update', 'id' => $model->id ], [ 'class' => '' ]) : '' );
                                },
                                'delete' => function ($url, $model) {
                                    return (( $model->_status == 10 )? Html::a('<i class="fas fa-trash-alt"></i>', [ 'delete', 'id' => $model->id ], [ 'class' => '' ]) : '' );
                                },

                            ],
                        ],

                    ],
                ]); ?>

            </div>

        </div>

    </div>


</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProposalCall */

$this->title = 'Project Information';
$this->params['breadcrumbs'][] = [ 'label' => 'Proposal Call', 'url' => [ 'index'] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->title, 'url' => [ 'view', 'id' => $model->id ] ];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="proposal-call-update">

    <?= $this->render('_form_', [

        'model' => $model,
        'calltype' => ((isset( $calltype ))? $calltype : null ),
        'category' => ((isset( $category ))? $category : null ),

    ]) ?>

</div>

<?php

use yii\helpers\Html;

use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ProposalCall */

$this->title = 'Project Information';
$this->params['breadcrumbs'][] = [ 'label' => 'Proposal Call', 'url' => [ 'index'] ];
$this->params['breadcrumbs'][] = $model->title;

\yii\web\YiiAsset::register($this);
?>
<div class="proposal-call-view mr-2 ml-2 mt-5">

    <div class="row">

        <div class="col-md-<?= ((Yii::$app->user->can('index-invitation') && Yii::$app->user->can('index-calls-report'))? '8' : '12') ?>">

            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title">
                        Call for Proposal Detail
                    </h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">

                    <p class="">
                        <?= (Yii::$app->user->can('update-proposal-call'))? (isset( $model->_status ) && ( $model->_status == 10 ))? Html::a('Publish', ['publish', 'id' => $model->id], ['class' => 'btn btn-info btn-sm']) : '' : '' ?>
                        <?= (Yii::$app->user->can('update-proposal-call'))? (isset( $model->_status ) && ( $model->_status == 10 ))? Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-warning btn-sm']) : '' : '' ?>
                        <?= (Yii::$app->user->can('delete-proposal-call'))? (isset( $model->_status ) && ( $model->_status == 10 ))? Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger btn-sm',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this call?',
                                'method' => 'post',
                            ],
                        ]) : '' : '' ?>
                    </p>

                    <div class="">

                        <h5 style="margin-bottom: 1.6rem; font-size: 16px;">
                            Title : <?= ((isset( $model->title ))? $model->title : '' ); ?>
                        </h5>
                        <h5 style="margin-bottom: 1.6rem; font-size: 16px;">
                            Categories : <?= ((isset( $model->category->_name ))? $model->category->_name : '' ); ?>
                        </h5>

                        <h5 style="">
                            Description
                        </h5>
                        <div class="callout callout-danger" style="margin-bottom: 1.6rem; padding-bottom: 2.6rem;">
                            <?= ((isset( $model->description ))? $model->description : '' ); ?>
                        </div>

                        <h5 style="margin-bottom: 1.6rem; font-size: 16px;">
                            Project Duration : <?= ((isset( $model->project_duration ))? $model->project_duration : '' ); ?> Months
                        </h5>
                        <h5 style="margin-bottom: 1.6rem; font-size: 16px;">
                            Submission Deadline : <?= ((isset( $model->closed_date ))? $model->closed_date : '' ); ?> - <span class="<?= ((isset( $days, $close ) && ( $close == 200 ))? 'badge badge-success' : 'badge badge-danger' ); ?> pr-3 pl-3"><?= ((isset( $days, $close ) && ( $close == 200 ))? $days : 0 ); ?> days remaining</span>
                        </h5>

                        <p style="margin-bottom: 5px;">
                            Please download Proposal Template and continue with the application
                        </p>
                        <?= ((isset( $model->docs_path ))? Html::a('<i class="fas fa-cloud-download-alt"></i><span style="margin-left: .3rem;">Download Template</span>', [ $model->docs_path ], [ 'class' => 'btn btn-success', 'target' => '_blank' ]) : null ); ?>

                        <p style="margin-bottom: 5px; margin-top: 1.0rem;">
                            <?= (\Yii::$app->user->can('submit-application-proposal-call'))? (isset( $model->_status, $close ) && ( $model->_status == 30 ) && ( $close == 200 ))? Html::a(' Apply ', [ 'proposal-call/submit-application', 'id' => $model->id ], [ 'class' => 'btn btn-warning pr-3 pl-3' ]) : '' : '' ; ?>
                        </p>

                    </div>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </div>

        <?php if (Yii::$app->user->can('index-invitation') && Yii::$app->user->can('index-calls-report')) { ?>
        <div class="col-md-4">

            <?php if (isset( $model->types_id ) && ( $model->types_id == 20 ) && Yii::$app->user->can('index-invitation')) { ?>
            <div class="row">
                <div class="col-12">

                    <div class="card card-outline card-primary">
                        <div class="card-header">
                            <h3 class="card-title">
                                Call type : <span class="badge badge-<?= ((isset( $model->types_id ) && ( $model->types_id == 20 ))? 'success' : 'warning' ); ?> pr-3 pl-3"><?= ((isset( $model->types->_name ))? $model->types->_name : null ); ?></span>
                            </h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <?php if (isset( $usersearch, $model->_status ) && ( $model->_status == 10 ) && Yii::$app->user->can('create-invitation')) { ?>

                            <?php $form = ActiveForm::begin([
                                'action' => [
                                    '/proposal-call/view' ,
                                    'id' => ((isset( $model->id ))? $model->id : null )
                                ],
                                'method' => 'post',
                            ]); ?>

                                <p>List email of all applicant you want to invite. If there are more than one separate them with comma(,)</p>
                                <div class="row">
                                    <div class="col-12">
                                        <?= $form->field($usersearch, 'email', [])->textInput([ 'placeholder' => 'Enter e-mail list' ])->label(false); ?>
                                    </div>
                                </div>

                                <?php if (isset( $output, $errorFound ) && is_array( $output )) { ?>
                                <div class="row">
                                    <div class="col-12" id="id-output-result">

                                        <div class="alert alert-danger alert-dismissible">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                                            Applicant with the following email doesn't exist

                                            <ol>
                                                <?php foreach( $output as $out ) { ?>
                                                <?php if (isset( $out['error'], $out['user'] )) { ?>
                                                <li><?= $out['user']; ?></li>
                                                <?php } ?>
                                                <?php } ?>

                                            </ol>

                                        </div>

                                    </div>
                                </div>
                                <?php } ?>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <?= $form->field($usersearch, 'proposal_calls_id', [])->hiddenInput([ 'id'=> 'proposal-call-id', 'value' => ((isset( $model->id ))? $model->id : null ) ])->label(false);; ?>
                                            <?= Html::submitButton('Send Invitation', [ 'class' => 'btn btn-success pr-3 pl-3' ]) ?>
                                        </div>
                                    </div>
                                </div>

                            <?php ActiveForm::end(); ?>

                            <?php } ?>

                            <?php
                                if (isset( $invitedUser )) {
                            ?>
                            <h6 class="mt-4">List of invited applicant</h6>
                            <?= \yii\grid\GridView::widget([
                                'tableOptions' => [
                                    'class' => 'table table-striped table-sm',
                                ],
                                'options' => [
                                    'class' => 'table-responsive',
                                ],
                                'dataProvider' => $invitedUser,
                                'summary' => false,
                                'columns' => [

                                    [
                                        'attribute' => 'user_id',
                                        'label' => 'Applicant',
                                        'format' => 'html',
                                        'value' => function($model) {
                                            return ((isset( $model->user->first_name, $model->user->last_name ))? $model->user->first_name .' '. $model->user->last_name : null );
                                        }
                                    ],
                                    [
                                        'attribute' => 'user_id',
                                        'label' => 'E-mail',
                                        'format' => 'html',
                                        'value' => function($model) {
                                            return ((isset( $model->user->email ))? $model->user->email : null );
                                        }
                                    ],

                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'contentOptions' => [
                                            'style' => 'width: 160px'
                                        ],
                                        'visible' => ((Yii::$app->user->isGuest)? false : true ),
                                        'buttons' =>[

                                            'view' => function ($url, $model) {
                                                return Html::a('<i class="fas fa-eye"></i>', [ '/user/view', 'id' => $model->user_id ], [ 'class' => '' ]);
                                            },
                                            'update' => function ($url, $model) {
                                                return '';
                                            },
                                            'delete' => function ($url, $model) {
                                                return ((isset( $model->proposalCalls->_status ) && ( $model->proposalCalls->_status == 10 ) && Yii::$app->user->can('delete-invitation') )? Html::a('<i class="fas fa-trash-alt"></i>', [ 'delete-invitation', 'id' => $model->id ], [ 'class' => '' ]) : '' );
                                            },

                                        ],
                                    ],

                                ],

                            ]); ?>
                            <?php
                                }
                            ?>

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                </div>
            </div>
            <?php } ?>
            <!-- /.card -->


            <?php if (Yii::$app->user->can('index-calls-report')) { ?>
            <div class="row mt-4">
                <div class="col-12">

                    <div class="card card-outline card-warning">
                        <div class="card-header">
                            <h3 class="card-title">
                                Call Reports
                            </h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">

                            <?php if (isset( $model->_status ) && ( $model->_status == 30 ) && Yii::$app->user->can('create-calls-report')) { ?>
                            <div class="row">
                                <div class="col-md-8">

                                    <div class="form-group" class="mt-4">
                                        <label for="exampleInputEmail1">Enter file name</label>
                                        <input type="email" class="form-control" id="file-name-input" placeholder="Enter file name" >
                                    </div>
                                    <div style="">
                                        <a id="any-btn-upload" class="btn btn-success pr-3 pl-3" href="Javascript: void(0);">Upload Report</a>
                                    </div>

                                </div>
                                <div class="col-md-4 pt-2">

                                    <div id="container-any">

                                        <a id="pickfiles-any" class="btn btn-warning btn-block mt-4">
                                            <i class="fas fa-cloud-upload-alt mr-2"></i> Select File <span id="logs_counter-any" class="badge badge-success ml-2"></span>
                                        </a>

                                    </div>
                                    <div id="filelist-any">
                                        Your browser doesn't have Flash, Silverlight or HTML5 support.
                                    </div>

                                </div>
                            </div>
                            <?php } ?>
                            <p class="title mt-4">
                                Available report for this call for proposal
                            </p>

                            <div class="card-body p-0">

                                <?php
                                if (isset( $reportProvider )) {
                                    ?>
                                    <?= \yii\grid\GridView::widget([
                                        'tableOptions' => [
                                            'class' => 'table table-striped table-sm',
                                        ],
                                        'options' => [
                                            'class' => 'table-responsive',
                                        ],
                                        'dataProvider' => $reportProvider,
                                        'summary' => false,
                                        'columns' => [

                                            [
                                                'attribute' => 'filename',
                                                'format' => 'html',
                                                'value' => function($model) {
                                                    return ((isset( $model->filename ))? $model->filename : null );
                                                }
                                            ],

                                            [
                                                'class' => 'yii\grid\ActionColumn',
                                                'contentOptions' => [
                                                    'style' => 'width: 160px'
                                                ],
                                                'visible' => ((Yii::$app->user->isGuest)? false : true ),
                                                'buttons' =>[

                                                    'view' => function ($url, $model) {
                                                        return Html::a('<i class="fas fa-download"></i>', [ ((isset( $model->filepath ))? $model->filepath : '#' ) ], [ 'class' => '', 'target' => '_blank' ]);
                                                    },
                                                    'update' => function ($url, $model) {
                                                        return '';
                                                    },
                                                    'delete' => function ($url, $model) {
                                                        return ((isset( $model->proposalCalls->_status ) && ( $model->proposalCalls->_status == 10 ) && Yii::$app->user->can('delete-calls-report'))? Html::a('<i class="fas fa-trash-alt"></i>', [ 'delete-report', 'id' => $model->id ], [ 'class' => '' ]) : '' );
                                                    },

                                                ],
                                            ],

                                        ],

                                    ]); ?>
                                    <?php
                                }
                                ?>


                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                </div>
            </div>
            <?php } ?>


        </div>
        <?php } ?>

    </div>


    <div class="row mt-3 mb-5">

        <?php if (Yii::$app->user->can('create-proposal-call')) { ?>
        <div class="col-md-<?= ((Yii::$app->user->can('index-invitation') && Yii::$app->user->can('index-calls-report'))? '8' : '12') ?>">
            <div class="card card-outline card-warning">
                <div class="card-header">
                    <h3 class="card-title">
                        List of Application
                    </h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">


                    <?= ((isset( $applyProvider ))? \yii\grid\GridView::widget([
                        'tableOptions' => [
                            'class' => 'table table-striped',
                        ],
                        'options' => [
                            'class' => 'table-responsive',
                        ],
                        'dataProvider' => $applyProvider,
                        'columns' => [

                            ['class' => 'yii\grid\SerialColumn'],

                            [
                                'attribute' => 'created_at',
                                'label' => 'Date',
                                'format' => 'html',
                                'value' => function($model) {
                                    return ((isset( $model->created_at ))? Yii::$app->formatter->asDate( $model->created_at, 'd-MM-Y' ) : null ) ;
                                }
                            ],
                            [
                                'attribute' => 'title',
                                'label' => 'Proposal Title',
                                'format' => 'html',
                                'value' => function($model) {
                                    return Html::a( ((isset( $model->title ))? $model->title : null ), [ '/application/staff-view', 'id' => $model->id ], [ 'class' => '' ]);
                                }
                            ],

                            [
                                'attribute' => '_status',
                                'label' => 'Status',
                                'format' => 'html',
                                'value' => function($model) {
                                    return ((isset( $model->status->_name ))? '<span class="badge badge-' . $model->status->tag_color . ' mr-2">' . $model->status->_name . '</span>' : null ) ;
                                }
                            ],

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'contentOptions' => [
                                    'style' => 'width: 160px'
                                ],
                                'visible' => ((Yii::$app->user->isGuest)? false : true ),
                                'buttons' =>[

                                    'view' => function ($url, $model) {
                                        return Html::a('<i class="fas fa-eye"></i>', [ '/application/staff-view', 'id' => $model->id ], [ 'class' => '' ]);
                                    },
                                    'update' => function ($url, $model) {
                                        return '';
                                    },
                                    'delete' => function ($url, $model) {
                                        return '';
                                    },

                                ],
                            ],

                        ],
                    ]) : '' ); ?>



                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <?php } ?>
        <!-- /.col -->


    </div>



</div>
<?php

// here is the url to the file update
$endPointUrl = Yii::$app->request->baseUrl.'/uploader/call-report' ;

// here is the url to the file update
$docposturl = Yii::$app->request->baseUrl.'/uploader/call-report-update' ;

// get proposal call
$proposalId = ((isset( $model->id ))? $model->id : null );

// import all js necessary
$this->registerJsFile(Yii::$app->request->baseUrl .'/plupload/js/plupload.full.min.js', [ 'depends' => [ \yii\web\JqueryAsset::className() ] ]);
$this->registerJsFile(Yii::$app->request->baseUrl .'/js/yvideoJs.js', [ 'depends' => [ \yii\web\JqueryAsset::className() ] ]);

// register js code
if (isset($model->_status) && ($model->_status == 30) && Yii::$app->user->can('create-calls-report')) {
$this->registerJs(<<< EOT_JS_CODE

/**
	HERE we are going upload report
 */
window.addEventListener("load", function ()
{

	let anydoc = PaulUploadButton({
		pickfiles: 'pickfiles-any',
		container: 'container-any',
		url: '$endPointUrl',
		chunk_size: '2mb',
		filters: {
            max_file_size : '2mb',
            mime_types: [
                {title : "Document files", extensions : "pdf,doc,docx,xls,xlsx"},
            ]
        },
		filelist: 'filelist-any',
		progress_counter: 'logs_counter-any',
        multipart_params: {
            "ProposalCallsReport[proposal_calls_id]": $proposalId,
            "ProposalCallsReport[category]": "CALL",
        },
		callback: function toa_matokeo(output) {

            fetch('$docposturl', {
                method: 'post',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    filename: document.getElementById('file-name-input').value,
                    id: output.info.proposal.id.toString()
                })
            })
            .then(res => res.json())
            .then(res => {

                // here we can refrash the page after update
                // the document information
                window.location.reload();

            });

        },
	});

}, false);

EOT_JS_CODE
);
}

// here is the url to the file update
$userfetch = Yii::$app->request->baseUrl.'/api/default/user-search' ;
$this->registerJs(<<< EOT_JS_CODE

/**
	HERE we are going upload report
 */
document.getElementById('inviteduser-email').addEventListener("keyup", function ()
{

    fetch('$userfetch', {
        method: 'post',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email: this.value,
            id: $proposalId
        })
    })
    .then(res => res.json())
    .then(data => {

        console.log(data);

    });

}, false);

EOT_JS_CODE
);

?>

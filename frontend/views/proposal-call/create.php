<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProposalCall */

$this->title = 'Project Information';
$this->params['breadcrumbs'][] = ['label' => 'Proposal Call', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Add New Proposal Call';
?>
<div class="proposal-call-create">

    <?= $this->render('_form_', [

        'model' => $model,
        'calltype' => ((isset( $calltype ))? $calltype : null ),
        'category' => ((isset( $category ))? $category : null ),

    ]) ?>

</div>

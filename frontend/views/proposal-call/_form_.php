<?php

use yii\helpers\Html;

use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\ProposalCall */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proposal-call-form mr-2 ml-2">

<?= $this->registerCssFile("@web/css/bootstrap-datepicker3.min.css"); ?>

<?php $form = ActiveForm::begin(['id' => 'form-call']); ?>

<div class="card" style="margin-top: 1.5rem; padding-top: 1.0rem;">
    <div class="card-body login-card-body">

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?= $form->field($model, 'title')->textInput(['placeholder' => "Proposal Title"]) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?= $form->field($model, 'description')->textarea(['rows' => 6, 'placeholder' => "Proposal Introduction"]) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map($category, 'id', '_name'), [ 'prompt' => 'Please Category' ])->label('Call Category') ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= $form->field($model, 'closed_date')->textInput([ 'placeholder' => "yyyy-mm-dd", 'class' => 'form-control datepicker', 'readonly' => 'readonly' ]) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?= $form->field($model, 'project_duration')->textInput(['placeholder' => "Project Duration (In Days)"])->label('Project Duration (In Month)') ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= ((isset( $calltype ))? $form->field($model, 'types_id')->dropDownList(ArrayHelper::map($calltype, 'id', '_name'), [ 'prompt' => 'Please Call Type' ])->label('Call Type') : '' ); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group mt-2">

                    <div id="attachment-7gsh8765" style="min-height: 26px;">
                    </div>

                    <div id="container" style="margin-left: 10px;">

                        <a id="pickfiles" class="btn btn-info btn-lg" href="Javascript: void(0);" style="position: relative; z-index: 1;">
                            <i class="fas fa-cloud-upload-alt"></i><span style="margin-left: .3rem;"><?= ((isset( $model->docs_path ))? 'Remove & Upload Proposal' : 'Upload Proposal' ) ?></span>
                        </a>
                        <div style="">
                            <?= ((isset( $model->docs_path ))? Html::a('View File', [ $model->docs_path ], [ 'class' => 'btn btn-success btn-xs', 'target' => '_blank', 'style' => 'padding: 2px 20px;' ]) : null ); ?>
                        </div>

                    </div>

                    <div id="filelist" class="mt-3" style="margin-left: 10px;">
                        Your browser doesn't have Flash, Silverlight or HTML5 support.
                    </div>
                    <pre id="logs_counter" style="margin-left: 10px;">
                    </pre>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="form-group">

                    <?= Html::submitButton(
                        'Save', [ 
                            'class' => 'btn btn-warning btn-lg', 
                            'style' => 'padding: .5rem 2.6rem;',
                            'id' => 'save-now'
                        ]
                    ); ?>
                    <?= Html::button("Save & Publish",
                        [
                            'class' => 'btn btn-info btn-lg',
                            'style' => 'padding: .5rem 2.6rem; visibility: hidden;',
                            'title' => Yii::t( 'app', 'Publish new call for proposal' ),
                            'id' => 'button-save-publish'
                        ]
                    ); ?>
                    <?= $form->field($model, 'docs_path')->hiddenInput([ 'id'=> 'proposal-file-path' ])->label(false); ?>
                    <?= $form->field($model, '_status')->hiddenInput([ 'id'=> 'proposal-submit-status' ])->label(false); ?>

                </div>
            </div>
        </div>

<?php
$this->registerJsFile(Yii::$app->request->baseUrl.'/plupload/js/plupload.full.min.js', [ 'depends' => [ \yii\web\JqueryAsset::className() ] ]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/yvideoJs.js', [ 'depends' => [ \yii\web\JqueryAsset::className() ] ]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/bootstrap-datepicker.min.js', [ 'depends' => [ \yii\web\JqueryAsset::className() ] ]);
$this->registerJs( <<< EOT_JS_CODE

// JS code here

window.addEventListener("load", function ()
{

	var forAvatar = PaulUpload({
		pickfiles: 'pickfiles',
		container: 'container',
		url: '/yesrf/uploader/upload-document',
		chunk_size: '2mb',
		filters: {
            max_file_size : '2mb',
            mime_types: [
                {title : "Document files", extensions : "pdf,doc,docx,xls,xlsx,zip"},
            ]
        },
		filelist: 'filelist',
		progress_counter: 'logs_counter',
		callback: function toa_matokeo(output) { $('#proposal-file-path').val( output.info.src.toString() ); },
	});

});

$(".datepicker").datepicker({
	format: "yyyy-mm-dd", autoclose: true, startDate: '+1d'
});


/**
	HERE we are going to help searching poing
 */
document.getElementById('button-save-publish').addEventListener('click', function() {

    /*   Here we assume the video has been watched   */
    $("#proposal-submit-status").val("30");
	console.log(this.value);

    /* submit form */
    $("#save-now").trigger('click');

}, false);


EOT_JS_CODE
);
?>

    </div>
</div>

<?php ActiveForm::end(); ?>

</div>

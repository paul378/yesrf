<?php

use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yii\grid\GridView;

use common\models\Esrf;
use common\models\Common;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProposalCallSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proposal Calls';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proposal-call-index mr-2 ml-2">

    <div class="row">
        <div class="col-md-12">

            <?php echo $this->render('_search_', ['model' => $searchModel]); ?>

            <div class="table-responsive" style="margin-top: 3rem;">

                <?= GridView::widget([
                    'tableOptions' => [
                        'class' => 'table table-striped',
                    ],
                    'options' => [
                        'class' => 'table-responsive',
                    ],
                    'dataProvider' => $dataProvider,
                    'summary' => false,
                    'columns' => [

                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'closed_date',
                            'label' => 'Date',
                            'format' => 'html',
                            'value' => function($model) {
                                return ((isset( $model->closed_date ))? Yii::$app->formatter->asDate( $model->closed_date, 'd-MM-Y' ) : null );
                            }
                        ],
                        [
                             'attribute' => 'category_id',
                             'label' => 'Category',
                             'format' => 'html',
                             'value' => function($model) {
                                 return ((isset( $model->category_id ))? '<span class="badge badge-' . (( $model->category_id == 20 )? 'success' : 'warning' ) .' pr-3 pl-3">'. ((isset( $model->category->_name ))? $model->category->_name : null ) .'</span>' : null );
                             }
                        ],

                        [
                            'attribute' => 'title',
                            'format' => 'html',
                            'value' => function($model) {
                                return Html::a( $model->title, [ 'view', 'id' => $model->id ], [ 'class' => '' ]);
                            }
                        ],

                        [
                            'attribute' => 'types_id',
                            'label' => 'Types',
                            'format' => 'html',
                            'value' => function($model) {
                                return ((isset( $model->types_id ))? '<span class="badge badge-' . (( $model->types_id == 20 )? 'success' : 'warning' ) .' pr-3 pl-3">'. (( $model->types_id == 20 )? 'Invitation' : 'Public' ) .'</span>' : null );
                            }
                        ],
                        [
                            'attribute' => '_status',
                            'label' => 'Status',
                            'format' => 'html',
                            'value' => function($model) {
                                return ((isset( $model->types_id ))? '<span class="badge badge-' . (( $model->_status == 50 )? 'default' : (( $model->_status == 40 )? 'danger' : (( $model->_status == 30 )? 'success' : (( $model->_status == 20 )? 'danger' : (( $model->_status == 10 )? 'primary' : 'info' ) ) ) ) ) .' pr-3 pl-3">'. (( $model->_status == 50 )? 'Reviewing' : (( $model->_status == 40 )? 'Rejected' : (( $model->_status == 30 )? 'Submitted' : (( $model->_status == 20 )? 'Cancelled/Blocked' : (( $model->_status == 10 )? 'Saved' : 'Unknown' ) ) ) ) ) .'</span>' : null );
                            }
                        ],


                        [
                            'attribute' => 'closed_date',
                            'label' => 'Days Remaining',
                            'format' => 'html',
                            'value' => function($model) {

                                $tarehe = Esrf::datedifferent( ((isset( $model->closed_date ))? $model->closed_date : '2000-12-31 00:00:00' ) );
                                $close = ((isset( $tarehe['close'] ))? $tarehe['close'] : 100 );
                                $days  = ((isset( $tarehe['days'] ))?  $tarehe['days']  : 0 );

                                return ((isset( $model->closed_date ))? '<span class="' . ((isset( $days, $close ) && ( $close == 200 ))? 'badge badge-success' : 'badge badge-danger' ) .' pr-3 pl-3">'. ((isset( $days, $close ) && ( $close == 200 ))? $days : 0 ) .' days</span>' : null );

                            }
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'visibleButtons' => [
                                'view' => function ($model, $key, $index) {
                                    return true;
                                 },
                                'update' => function ($model, $key, $index) {
                                    return false;
                                },
                                'delete' => function ($model, $key, $index) {
                                    return false;
                                }
                            ]
                        ],

                    ],
                ]); ?>

            </div>

        </div>

    </div>


</div>

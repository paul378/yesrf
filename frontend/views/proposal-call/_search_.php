<?php

use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\ApplicationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="application-search">

    <?php $form = ActiveForm::begin([
        'action' => [ 'index' ],
        'method' => 'get',
    ]); ?>


    <div class="row">

        <div class="col-md-8">

            <div class="col-md-3" style="visibility: hidden;">
                <div class="form-group">
                    <?= Html::submitButton('Search', [ 'class' => 'btn btn-default btn-block', 'id' => 'search-now' ]) ?>
                </div>
            </div>

        </div>
        <div class="col-md-4">

            <div class="row">

                <div class="col-md-4">
                    <div class="form-group">
                        <?php
                            if (\Yii::$app->user->can('create-proposal-call')) {
                                echo Html::a('Add New Call', [ 'create' ], [ 'class' => 'btn btn-success btn-block' ]);
                            }
                        ?>
                    </div>
                </div>
                <div class="col-md-8">
                    <?= $form->field(
                        $model, 
                        'title', [
                            'addon' => [
                                'prepend' => [
                                    'content' => '<i class="fas fa-search" id="proposal-calls-search" style="cursor: pointer;"></i>'
                                ]
                            ]
                        ])
                        ->textInput([
                            'placeholder' => "Search By Title"
                        ])
                        ->label(false);
                    ?>
                </div>

            </div>

        </div>

    </div>





    <?php ActiveForm::end(); ?>
    <?php

// register js code
$this->registerJs( <<< EOT_JS_CODE

/**
	HERE we are going to help searching poing
 */
document.getElementById('proposal-calls-search').addEventListener('click', function() {

    /* submit form */
    $("#search-now").trigger('click');

}, false);


EOT_JS_CODE
);
?>


</div>

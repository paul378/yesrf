<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProposalCall */
/* @var $roles app\models\AuthItem */
/* @var $regions app\models\Region */

$this->title = 'Users';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Add New User';
?>
<div class="proposal-call-create mr-2 ml-2">

    <?= $this->render('_register_', [

        'model' => $model,
        'roles' => $roles,
        'regions' => $regions

    ]) ?>

</div>

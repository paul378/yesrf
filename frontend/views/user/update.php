<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProposalCall */
/* @var $roles app\models\AuthItem */
/* @var $regions app\models\Region */

$this->title = $model->first_name . "'s Profile";

if (\Yii::$app->user->can('delete-user')) {
    $this->params['breadcrumbs'][] = [ 'label' => 'Users', 'url' => [ 'index'] ];
}

$this->params['breadcrumbs'][] = [ 'label' => $model->first_name, 'url' => [ 'view', 'id' => $model->id ] ];
$this->params['breadcrumbs'][] = "Edit " . $this->title;

?>
<div class="proposal-call-update mr-2 ml-2">

    <?= $this->render('_form_', [

        'model' => $model,
        'roles' => $roles,
        'regions' => $regions

    ]) ?>

</div>

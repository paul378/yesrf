<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $roles app\models\AuthAssignment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php if (isset( $regions )) { ?>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sex')->dropDownList(ArrayHelper::map([ 
        [ 'id' => 'F', 'name' => 'Female' ],
        [ 'id' => 'M', 'name' => 'Male' ] 
    ], 'id', 'name'), [ 'prompt' => 'Select Gender' ])->label("Select Gender"); ?>

    <?= $form->field($model, 'dob')->widget(\yii\widgets\MaskedInput::class, [
            'mask' => '9999-99-99',
        ])->textInput([ 'placeholder' => '1980-09-26' ])->label('Date of Birth') ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mobile')->widget(\yii\widgets\MaskedInput::class, [
            'mask' => '+255 999 999 999',
        ])->textInput(['maxlength' => true]) ?>

    <?= ((\Yii::$app->user->can('update-user'))? $form->field($model, 'role')->dropDownList(ArrayHelper::map($roles, 'name', 'name'), [ 'prompt' => 'Select Role' ])->label("Select Role") : '' ); ?>

    <?= $form->field($model, '_status')->dropDownList(ArrayHelper::map([
        [ 'id' => 10, 'name' => 'Activate' ],
        [ 'id' => 20, 'name' => 'Deactivate' ]
    ], 'id', 'name'), [ 'prompt' => 'Select Status' ])
    ->label("User Status"); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php } ?>

</div>

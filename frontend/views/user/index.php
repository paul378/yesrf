<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = 'List of users';
?>
<div class="user-index mr-2 ml-2">

    <div class="row">
        <div class="col-md-12">

            <?php echo $this->render(
                '_search_', 
                [
                    'model' => $searchModel,
                    'regions' => $regions
                ]); ?>

            <div class="row">

                <div class="col-md-2 mt-4">
                    <h5>List of user's available</h5>
                </div>

            </div>
            <div class="table-responsive">

                <?= GridView::widget([

                    'tableOptions' => [
                        'class' => 'table table-striped',
                    ],
                    'options' => [
                        'class' => 'table-responsive',
                    ],
                    'dataProvider' => $dataProvider,

                    'columns' => [

                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'first_name',
                            'label' => 'Name',
                            'format' => 'html',
                            'value' => function($model)
                            {
                                return Html::a( ((isset( $model->company->full_name ))? $model->company->full_name : $model->first_name .' '. $model->last_name ), [ 'view', 'id' => $model->id ], [ 'class' => '' ]);
                            }
                        ],
                        [
                            'attribute' => 'last_name',
                            'label' => ((isset( $model->company->full_name ))? 'Institution Category' : 'Last name' ),
                            'format' => 'html',
                            'value' => function($model)
                            {
                                return Html::a( ((isset( $model->company->full_name ))? ((isset( $model->company->category->_name ))? $model->company->category->_name : '' ) : '' ), [ 'view', 'id' => $model->id ], [ 'class' => '' ]);
                            }
                        ],

                        'email',
                        'mobile',

                        [
                            'attribute' => 'user_activated',
                            'label' => 'Activated',
                            'format' => 'html',
                            'value' => function($model)
                            {

                                switch ($model->user_activated)
                                {
                                    case 100:
                                        return "YES";
                                        break;
                                    case 200:
                                        return "NO";
                                        break;
                                    default:
                                        return "NA";
                                        break;
                                }

                            }
                        ],

                        [
                            'attribute' => '_status',
                            'label' => 'Status',
                            'format' => 'html',
                            'value' => function($model)
                            {

                                switch ($model->_status)
                                {
                                    case 10:
                                        return "Active";
                                        break;
                                    case 20:
                                        return "Blocked";
                                        break;
                                    default:
                                        return "NA";
                                        break;
                                }

                            }
                        ],

                        [
                            'attribute' => 'role',
                            'label' => 'User Role',
                            'format' => 'html',
                            'value' => function($model)
                            {

                                return $model->role;

                            }
                        ],

                        //  ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>

            </div>

        </div>

    </div>

</div>


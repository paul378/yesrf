<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProposalCall */
/* @var $roles app\models\AuthItem */
/* @var $regions app\models\Region */
/* @var $category app\models\CompanyCategory */
/* @var $company app\models\Company */

$this->title = ((isset( $model->company->full_name ))? $model->company->full_name : $model->first_name ) . "'s Profile";
((\Yii::$app->user->can('delete-user'))? $this->params['breadcrumbs'][] = [ 'label' => 'Users', 'url' => [ 'index'] ] : '' );
$this->params['breadcrumbs'][] = [ 'label' => ((isset( $model->company->full_name ))? $model->company->full_name : $model->first_name ), 'url' => [ 'view', 'id' => $model->id ] ];
$this->params['breadcrumbs'][] = "Edit " . $this->title;
?>
<div class="proposal-call-update mr-2 ml-2">

    <?= $this->render('_form_institution_', [

        'model' => $model,
        'roles' => $roles,
        'regions' => $regions,
        'category' => $category,
        //  'company'  => $company,
    ]) ?>

</div>

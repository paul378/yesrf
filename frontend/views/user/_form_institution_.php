<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $roles app\models\AuthAssignment */
/* @var $category app\models\CompanyCategory */
/* @var $company app\models\Company */
/* @var $regions app\models\Region */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-12">

                <div class="row">
                    <div class="col-md-6">

                        <h5 class="mb-4">Contact Personal</h5>

                        <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'sex')->dropDownList(ArrayHelper::map([
                            [ 'id' => 'F', 'name' => 'Female' ],
                            [ 'id' => 'M', 'name' => 'Male' ]
                        ], 'id', 'name'), [ 'prompt' => 'Select status' ])->label("Select Gender"); ?>

                        <?= $form->field($model, 'email')->textInput([ 'maxlength' => true, 'readonly' => 'readonly' ]) ?>

                        <?= $form->field($model, 'mobile')->widget(\yii\widgets\MaskedInput::class, [
                            'mask' => '+255 999 999 999',
                        ])->textInput(['maxlength' => true]) ?>

                        <?= ((\Yii::$app->user->can('update-user'))? $form->field($model, 'region_id')->dropDownList(ArrayHelper::map($regions, 'id', '_name'), [ 'prompt' => 'Select Region' ])->label("Select Region") : '' ); ?>

                        <?= ((\Yii::$app->user->can('update-user'))? $form->field($model, 'role')->dropDownList(ArrayHelper::map($roles, 'name', 'name'), [ 'prompt' => 'Select Role' ])->label("Select Role") : '' ); ?>

                        <div class="form-group mt-5">
                            <?= Html::submitButton('Save', ['class' => 'btn btn-success pt-3 pb-3 pl-5 pr-5']) ?>
                        </div>

                    </div>
                    <div class="col-md-6">

                        <h5 class="mb-4">Institution Information</h5>

                        <?= $form->field($model->company, 'full_name')->textInput(['maxlength' => true])->label("Institution Name") ?>

                        <?= $form->field($model->company, 'category_id')
                            ->dropDownList(ArrayHelper::map($category, 'id', '_name'), [ 'prompt' => 'Select Category' ])
                            ->label("Institution Category") ?>

                        <?= $form->field($model->company, 'email')->textInput([ 'maxlength' => true, 'readonly' => 'readonly' ])->label("E-mail") ?>

                        <?= $form->field($model->company, 'mobile')->widget(\yii\widgets\MaskedInput::class, [
                            'mask' => '+255 999 999 999',
                        ])->textInput([ 'maxlength' => true ])->label("Phone Number") ?>

                    </div>
                </div>

            </div>
        </div>

    <?php ActiveForm::end(); ?>

</div>

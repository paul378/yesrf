<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin([
        'action' => [ 'user/change-password', 'id' => ((isset( $model->id ))? $model->id : null ) ],
        'method' => 'post',
    ]); ?>

    <div class="col-md-12" style="margin-top: 3rem;">
        <div class="form-group">
            <h5>Change Password</h5>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: .5rem;">

        <div class="row">

            <div class="col-md-6">
                <div class="form-group">
                    <?= $form->field($common_user, 'old_password')->passwordInput([ 'maxlength' => true, 'placeholder' => "Old Password" ])->label(false); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= $form->field($common_user, 'new_password')->passwordInput([ 'maxlength' => true, 'placeholder' => "Enter new password" ])->label(false); ?>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <?= Html::submitButton('Change Password', ['class' => 'btn btn-success btn-sm']) ?>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = ((isset( $model->company->full_name ))? $model->company->full_name : $model->first_name ) . "'s Profile Detail";
((\Yii::$app->user->can('delete-user'))? $this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']] : '' );
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>
<div class="user-view mr-2 ml-2 mt-4">



    <p>
        <?= ((\Yii::$app->user->can('update-user'))? Html::a('<i class="fas fa-pen mr-2"></i>Edit', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ); ?>
        <?= ((\Yii::$app->user->can('delete-user'))? Html::a('<i class="fas fa-user-times mr-2"></i>Delete Account', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) : '' ); ?>

    </p>

    <div class="row" style="margin-bottom: 3rem;">

        <div class="col-md-6">
            <div class="form-group">

                <?= DetailView::widget([

                    'options' => [
                        'class' => 'table table-striped',
                    ],
                    'model' => $model,

                    'attributes' => [

                        [
                            'attribute' => 'created_at',
                            'label' => 'Registered Date',
                            'format' => 'html',
                            'value' => function($model)
                            {
                                return ((isset( $model->created_at ))? Yii::$app->formatter->asDate( $model->created_at, "php: d F, Y" ) : null );
                            }
                        ],

                        [
                            'attribute' => 'first_name',
                            'label' => ((isset( $model->company->full_name ))? 'Institution Name' : 'First name' ),
                            'format' => 'html',
                            'value' => function($model)
                            {
                                return ((isset( $model->company->full_name ))? $model->company->full_name : $model->first_name );
                            }
                        ],
                        [
                            'attribute' => 'last_name',
                            'label' => ((isset( $model->company->full_name ))? 'Institution Category' : 'Last name' ),
                            'format' => 'html',
                            'value' => function($model)
                            {
                                return ((isset( $model->company->full_name ))? ((isset( $model->company->category->_name ))? $model->company->category->_name : $model->last_name ) : $model->last_name );
                            }
                        ],

                        [
                            'attribute' => 'country_id',
                            'label' => 'Region',
                            'format' => 'html',
                            'value' => function($model)
                            {
                                return ((isset( $model->region->_name ))? $model->region->_name : "Not Set" );
                            }
                        ],

                        'email',
                        'mobile',

                    ],
                ]) ?>

            </div>
        </div>
        <div class="col-md-6">
            <div class="row">

                <div class="col-md-12">
                    <div class="form-group">

                        <?= DetailView::widget([

                            'options' => [
                                'class' => 'table table-striped',
                            ],
                            'model' => $model,

                            'attributes' => [

                                [
                                    'attribute' => 'user_activated',
                                    'label' => 'Activated',
                                    'format' => 'html',
                                    'value' => function($model)
                                    {

                                        switch ($model->user_activated)
                                        {
                                            case 100:
                                                return "YES";
                                                break;
                                            case 200:
                                                return "NO";
                                                break;
                                            default:
                                                return "NO";
                                                break;
                                        }

                                    }
                                ],

                                //    [
                                //        'attribute' => 'login_log',
                                //        'label' => 'Last login',
                                //        'format' => 'html',
                                //        'value' => function($model)
                                //        {
                                //
                                //            return $model->login_log;
                                //
                                //        }
                                //    ],

                            ],
                        ]) ?>

                    </div>
                </div>

            </div>
            <div class="row">

                <?= Yii::$app->controller->renderPartial("//user/_change_password", [
                    'common_user' => new \common\models\UserUpdate(),
                    'model' => $model
                ]) ?>

            </div>
        </div>

    </div>

    <div class="row">

        <!-- ?= Yii::$app->controller->renderPartial("//user/_member_annual_fee", [
            'model' => $model
        ]) ? -->

    </div>

</div>


<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $regions app\models\Region */
/* @var $roles app\models\AuthAssignment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php if (isset( $regions )) { $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'sex')->dropDownList(ArrayHelper::map([
            [ 'id' => 'F', 'name' => 'Female' ],
            [ 'id' => 'M', 'name' => 'Male' ]
        ], 'id', 'name'), [ 'prompt' => 'Select status' ])->label("Select Gender"); ?>

        <?= $form->field($model, 'dob')->textInput() ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'mobile')->widget(\yii\widgets\MaskedInput::class, [
                'mask' => '+255 999 999 999',
            ])->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'region_id')->dropDownList(ArrayHelper::map($regions, 'id', '_name'), [ 'prompt' => 'Select Region' ])->label("Select Region"); ?>

        <?= $form->field($model, 'role')->dropDownList(ArrayHelper::map($roles, 'name', 'name'), [ 'prompt' => 'Select Role' ])->label("Select Role"); ?>

        <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); } ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->first_name . "'s Profile Detail";
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>
<div class="user-view mt-5 mr-2 ml-2">




    <p>
        <?= Html::a('Edit', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <div class="row" style="margin-bottom: 3rem;">

        <div class="col-md-6">
            <div class="form-group">

                <?= DetailView::widget([

                    'options' => [
                        'class' => 'table table-striped',
                    ],
                    'model' => $model,

                    'attributes' => [

                        'created_at',
                        'first_name',
                        'last_name',

                        [
                            'attribute' => 'sex',
                            'format' => 'html',
                            'value' => function($model)
                            {
                                return ((isset( $model->sex ))? (( $model->sex == 'F' )? 'Female' : 'Male' ) : null );
                            }
                        ],
                        'dob',

                        [
                            'attribute' => 'country_id',
                            'label' => 'Country',
                            'format' => 'html',
                            'value' => function($model)
                            {
                                return ((isset( $model->region->_name ))? $model->region->_name : "Not Set" );
                            }
                        ],

                        'email',
                        'mobile',

                    ],
                ]) ?>

            </div>
        </div>
        <div class="col-md-6">
            <div class="row">

                <div class="col-md-12">
                    <div class="form-group">

                        <?= DetailView::widget([

                            'options' => [
                                'class' => 'table table-striped',
                            ],
                            'model' => $model,

                            'attributes' => [

                                [
                                    'attribute' => 'user_activated',
                                    'label' => 'Activated',
                                    'format' => 'html',
                                    'value' => function($model)
                                    {

                                        switch ($model->user_activated)
                                        {
                                            case 100:
                                                return "YES";
                                                break;
                                            case 200:
                                                return "NO";
                                                break;
                                            default:
                                                return "NO";
                                                break;
                                        }

                                    }
                                ],

                                //    [
                                //        'attribute' => 'login_log',
                                //        'label' => 'Last login',
                                //        'format' => 'html',
                                //        'value' => function($model)
                                //        {
                                //
                                //            return $model->login_log;
                                //
                                //        }
                                //    ],

                            ],
                        ]) ?>

                    </div>
                </div>

            </div>
            <div class="row">

                <?= Yii::$app->controller->renderPartial("//user/_profile_change_password", [
                    'common_user' => new \common\models\UserUpdate(),
                    'model' => $model
                ]) ?>

            </div>
        </div>

    </div>

    <div class="row">
    </div>

</div>


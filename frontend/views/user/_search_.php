<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>


    <div class="row">

        <div class="col-md-2 mt-4">
            <h5>Advanced User's Search Form</h5>
        </div>

    </div>
    <div class="row">

        <div class="col-md-2">
            <?= $form->field($model, 'first_name')->textInput([ 'placeholder' => "First name" ])->label(false); ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'last_name')->textInput([ 'placeholder' => "Last name" ])->label(false); ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'sex')->dropDownList(ArrayHelper::map([ 
                [ 'id' => 'F', 'name' => 'Female' ],
                [ 'id' => 'M', 'name' => 'Male' ] 
            ], 'id', 'name'), [ 'prompt' => 'Select status' ])->label(false); ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'email')->textInput([ 'placeholder' => "Enter E-mail" ])->label(false); ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'mobile')->textInput([ 'placeholder' => "Mobile" ])->label(false); ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'region_id')->dropDownList(ArrayHelper::map($regions, 'id', '_name'), [ 'prompt' => 'Select Region' ])->label(false); ?>
        </div>

    </div>
    <div class="row">

        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('<i class="fas fa-search mr-2"></i>Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::a('<i class="fas fa-user-plus mr-2"></i>Register', [ 'create', 'id' => $model->id ], [ 'class' => 'btn btn-outline-secondary' ]) ?>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MenuPanel */

$this->title  = 'Create Menu';
$this->params = [
    [
        'label' => 'Menu',
        'url' => [
            'index',
        ],
        'template' => "{link}", // template for this link only
        'class' => 'breadcrumb-item',
    ],
    [
        'label' => ''. $this->title,
    ],
];


?>

<div class="card">

    <div class="card-header border bottom">
        <h4 class="card-title">
            <?= Html::encode(' Create Menu or Submenu') ?>
        </h4>
    </div>
    <div class="card-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;
/* @var $this yii\web\View */
/* @var $model app\models\MenuPanel */

$this->title  = 'View Menu';
$this->params['breadcrumbs'][] = [ 'label' => 'Menu Panel', 'url' => [ 'index'] ];
$this->params['breadcrumbs'][] = $model->name;

?>

<div class="card mr-2 ml-2">

    <div class="card-header border bottom">
        <h4 class="card-title">
            <?= Html::encode('Menu Detail') ?>
        </h4>
    </div>
    <div class="card-body">
        <p>
            <?= (Yii::$app->user->can('create-menupanel'))? Html::a( 'Create', [ 'create' ], [ 'class' => 'btn btn-primary btn-sm' ]) : '' ?>
            <?= (Yii::$app->user->can('update-menupanel'))? Html::a( 'Update', [ 'update', 'id' => $model->id], [ 'class' => 'btn btn-success btn-sm' ]) : '' ?>
            <?= (Yii::$app->user->can('delete-menupanel'))? Html::a( 'Delete', [ 'delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-sm',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) : '' ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'type',
                'parent_id',
                'name',
                'redirect',
//            'user',
                [
                    'attribute'=>'user',
                    'format'=>'raw',
                    'value'=>function($model){
                        return User::find()->where('id=:id',[':id'=>$model->user])->one()->email;
                    }
                ],
//            'status',
                [
                    'attribute'=>'status',
                    'format'=>'raw',
                    'value'=>function($model){
                        return ($model->status==10)?'Active':'In Active';
                    }
                ],
                'order_index',
                [
                    'attribute'=>'icon',
                    'format'=>'raw',
                    'value'=>function($model){
                        return '<i class="'.$model->icon.'"></i>';
                    }
                ]
            ],
        ]) ?>
    </div>

</div>


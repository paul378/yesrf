<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MenuPanelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title  = 'Menu Management';
$this->params = [
    [
        'label' => 'Menu',
        'url' => [
            'index',
        ],
        'template' => "{link}", // template for this link only
        'class' => 'breadcrumb-item',
    ],
    [
        'label' => ''. $this->title,
    ],
];

?>

<div class="card">

    <div class="card-header border bottom">
        <h4 class="card-title">
            <?= Html::encode(' List of menu') ?>
        </h4>
    </div>
    <div class="card-body">
        <p>
            <?= Html::a('Add Menu', ['create'], ['class' => 'btn btn-primary']) ?>
        </p>
        <?php

            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'summary' => false,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    // 'id',
                    'type',
                    // 'parent_id',
                    'name',
                    'redirect',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header'=>'Actions',
                        'headerOptions' => ['style' => 'color:#337ab7'],
                        'visibleButtons' => [
                            'view' => function ($model, $key, $index) {
                                if (\Yii::$app->user->can('view-proposal-call')) 
                                {
                                    return true;
                                } else {
                                    return false;
                                }
                             },
                            'update' => function ($model, $key, $index) {
                                if (\Yii::$app->user->can('update-proposal-call')) 
                                {
                                    return (( $model->status == 10 )? true : false );
                                } else {
                                    return false;
                                }
                            },
                            'delete' => function ($model, $key, $index) {
                                if (\Yii::$app->user->can('delete-proposal-call')) 
                                {
                                    return (( $model->status == 10 )? true : false );
                                } else {
                                    return false;
                                }
                            }
                        ]
                    ],

                ],
                'tableOptions' =>['class' => 'table table-sm  table-hover'],
            ]);

        ?>
    </div>

</div>


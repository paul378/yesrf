<?php

use yii\base\BaseObject;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\grid\GridView;
use \yii\widgets\Pjax;

use \app\models\ApplicationReceiver;
/* @var $this yii\web\View */
/* @var $model app\models\ApplicationReceiver */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="application-receiver-form">

    <div class="row">
        <div class="col-md-7">


            <div class="col-md-12">
                <div class="card card-outline card-warning">
                    <div class="card-header">
                        <h3 class="card-title">
                            Search for user
                        </h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        Here you can search for user whom you want make them receive email everytime applicant submit a proposal or apply for a call



                        <?php
                            if(isset( $userProvider, $userModel )) {

                                Pjax::begin();

                                echo GridView::widget([

                                    'tableOptions' => [
                                        'class' => 'table table-striped',
                                    ],
                                    'options' => [
                                        'class' => 'table-responsive',
                                    ],
                                    'dataProvider' => $userProvider,
                                    'filterModel' => $userModel,
                                    'summary' => false,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        [
                                            'attribute' => 'first_name',
                                            'format' => 'html',
                                            'value' => function($model) {
                                                return ((isset( $model->first_name, $model->first_name ))? $model->first_name .' '. $model->last_name : null );
                                            }
                                        ],
                                        [
                                            'attribute' => 'email',
                                            'format' => 'text',
                                            'value' => function($model) {
                                                return ((isset( $model->email ))? $model->email : null );
                                            }
                                        ],
                                        [
                                            'attribute' => 'mobile',
                                            'format' => 'html',
                                            'value' => function($model) {
                                                return ((isset( $model->mobile ))? $model->mobile : null );
                                            }
                                        ],

                                        [
                                            'class' => 'yii\grid\ActionColumn',
                                            'contentOptions' => [
                                                'style' => 'width: 160px'
                                            ],
                                            'visible' => ((Yii::$app->user->isGuest)? false : true ),
                                            'buttons' =>[

                                                'view' => function ($url, $model) {
                                                    return Html::a('<i class="fas fa-eye"></i>', [ '/user/view', 'id' => $model->id ], [ 'class' => '' ]);
                                                },
                                                'update' => function ($url, $model) {
                                                    return Html::a('<i class="fas fa-user-plus"></i>', [ '/application-receiver/adduser', 'id' => $model->id ], [ 'class' => 'mr-1 ml-1' ]);
                                                },
                                                'delete' => function ($url, $model) {
                                                    return '';
                                                },

                                            ],
                                        ],


                                    ],
                                ]);

                                Pjax::end();

                            }

                            ?>




                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->


        </div>

        <div class="col-md-5">


            <div class="row">
                <div class="col-md-12">


                    <div class="card card-outline card-warning">
                        <div class="card-header">
                            <h3 class="card-title">
                                E-mail Receiver
                            </h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            List of staff who receive email for any calls applied



                            <?= ((isset( $receiverProvider, $receiverModel ))? GridView::widget([
                                'tableOptions' => [
                                    'class' => 'table table-striped',
                                ],
                                'options' => [
                                    'class' => 'table-responsive',
                                ],
                                'dataProvider' => $receiverProvider,
                                'summary' => false,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],

                                    'created_at',

                                    [
                                        'attribute' => 'receiver_id',
                                        'format' => 'html',
                                        'value' => function($model) {
                                            return ((isset( $model->receiver->first_name, $model->receiver->first_name ))? $model->receiver->first_name .' '. $model->receiver->last_name : null );
                                        }
                                    ],

                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'contentOptions' => [
                                            'style' => 'width: 160px'
                                        ],
                                        'visible' => ((Yii::$app->user->isGuest)? false : true ),
                                        'buttons' =>[

                                            'view' => function ($url, $model) {
                                                return '';
                                            },
                                            'update' => function ($url, $model) {
                                                return '';
                                            },
                                            'delete' => function ($url, $model) {
                                                return Html::a('<i class="fas fa-trash-alt"></i>', [ 'delete', 'id' => $model->id ], [
                                                    'class' => 'mr-1 ml-1',
                                                    'data' => [
                                                        'confirm' => 'Are you sure you want to delete this '. ((isset( $model->receiver->first_name, $model->receiver->first_name ))? $model->receiver->first_name .' '. $model->receiver->last_name : null ) .' in mailing receiver list?',
                                                        'method' => 'post',
                                                    ],
                                                ]);
                                            },

                                        ],
                                    ],

                                ],
                            ]) : '' ); ?>



                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->


                </div>
            </div>


        </div>
    </div>

</div>


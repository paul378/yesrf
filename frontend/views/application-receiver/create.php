<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplicationReceiver */

$this->title = 'Application Receiver';
$this->params['breadcrumbs'][] = ['label' => 'Application Receivers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-receiver-create mr-2 ml-2 mt-5">

    <?= $this->render('_form__', [
        'model' => $model,
        'userProvider' => ((isset( $userProvider ))? $userProvider : null ),
        'userModel' => ((isset( $userModel ))? $userModel : null ),

        'receiverProvider' => ((isset( $receiverProvider ))? $receiverProvider : null ),
        'receiverModel' => ((isset( $receiverModel ))? $receiverModel : null ),
    ]) ?>

</div>

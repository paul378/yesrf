<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProposalCall */

$this->title = 'Permission';
$this->params['breadcrumbs'][] = 'Access Denied';
?>
<div style="margin: 10.0rem auto; width: 360px;">

	<h1>
		You don't have access to this pages
	</h1>
	<p>return to <?= Html::a('Home page', [ '/site/index' ], [ 'class' => 'btn btn-primary' ]) ?> or contact system adminstrator</p>

</div>

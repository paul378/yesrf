<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Notification */

$this->title = 'Notification';
$this->params['breadcrumbs'][] = $this->title . ' Detail';

\yii\web\YiiAsset::register($this);
?>
<div class="notification-view mr-2 ml-2 mt-5">

    <div class="card">
        <div class="card-header">
            <h5 class="m-0">Notification</h5>
        </div>
        <div class="card-body">

            <p class="card-text">
                <?= ((isset( $model->notification->message ))? $model->notification->message : null ) ?>
            </p>
        </div>
    </div>

</div>

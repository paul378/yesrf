<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProposalCallsInvitation */

$this->title = 'Create Proposal Calls Invitation';
$this->params['breadcrumbs'][] = ['label' => 'Proposal Calls Invitations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proposal-calls-invitation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

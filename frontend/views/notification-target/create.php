<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NotificationTarget */

$this->title = 'Create Notification Target';
$this->params['breadcrumbs'][] = ['label' => 'Notification Targets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notification-target-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

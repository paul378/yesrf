<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AuthAssignmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Auth Assignments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-assignment-index mr-2 ml-2 mt-4">

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'tableOptions' => [
            'class' => 'table table-striped',
        ],
        'options' => [
            'class' => 'table-responsive',
        ],
        'dataProvider' => $dataProvider,
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'created_at',

            'item_name',

            [
                'attribute' => 'item_name',
                'label' => 'Full name',
                'format' => 'html',
                'value'  => function($model) {
                    return ((isset( $model->user->first_name, $model->user->last_name ))? $model->user->first_name .' '. $model->user->last_name : null );
                }
            ],

            [
                'attribute' => 'item_name',
                'label' => 'Mobile',
                'format' => 'html',
                'value'  => function($model) {
                    return ((isset( $model->user->mobile ))? $model->user->mobile : null );
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AuthAssignmentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auth-assignment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        
        <div class="col-md-3">
            <?= $form->field($model, 'item_name')->textInput([ 'placeholder' => "Search By role" ])->label(false); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'user_id')->textInput([ 'placeholder' => "Search By user id" ])->label(false); ?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('<i class="fas fa-search mr-2"></i>Search', [ 'class' => 'btn btn-success' ]) ?>
                <?= Html::a('<i class="fas fa-plus-circle mr-2"></i>Add New', [ 'create' ], [ 'class' => 'btn btn-primary' ]) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

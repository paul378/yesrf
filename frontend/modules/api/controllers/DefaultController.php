<?php

namespace app\modules\api\controllers;

use common\models\Common;
use common\models\Esrf;
use yii\web\Controller;

/**
 * Default controller for the `api` module
 */
class DefaultController extends Controller
{

    /**
     * Renders the index view for the module
     * @return string
     */
    public $enableCsrfValidation = false;

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [];
    }
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionUserSearch()
    {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $inputs = (object) json_decode( \Yii::$app->request->getRawBody() );

        return Esrf::userSearch( $inputs->email );

    }
}
